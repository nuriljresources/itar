using JResources.Common;
using JResources.Data;
using JResources.Data.Model;
using JResources.Data.Model.Accpac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DOAuthority.BusinessLogic
{
    public class AccpacLogic
    {
        public const string URI_SAVE_SHIPMENT = "Shipment/SaveShipment";
        public const string URI_POST_SHIPMENT = "Shipment/PostShipment";

        public const string URI_SAVE_TRANSFERS = "Transfers/SaveTransfers";
        public const string URI_POST_TRANSFERS = "Transfers/PostTransfers";

        public static ResponseModel MSRSaveShipment(MSRIssuedModel MSRIssuedModel)
        {
            Company company = Company.GetById(MSRIssuedModel.CompanyId);

            ShipmentModel model = new ShipmentModel();
            var dataSource = AccpacDataContext.GetDatabaseName(company.CompanyCode);
            
            model.DBUserName = SiteSettings.ACCPAC_USERNAME;
            model.DBPassword = SiteSettings.ACCPAC_PASSWORD;
            model.DBName = dataSource;
            model.docNum = MSRIssuedModel.MSRIssuedNo;
            model.description = (string.IsNullOrEmpty(MSRIssuedModel.Remarks) ? string.Empty : MSRIssuedModel.Remarks);
            model.reference = string.Format("{0};{1};{2}", MSRIssuedModel.MSRIssuedNo, MSRIssuedModel.MSR.DocumentNo, MSRIssuedModel.MSR.WorkOrderNo);
            model.transDate = MSRIssuedModel.MSR.DateCreated.ToString("yyyy,MM,dd");
            model.dateBus = DateTime.Now.ToString("yyyy,MM,dd");
            model.custNo = string.Empty;
            model.contact = string.Empty;
            model.status = "1";
            model.currency = company.Currency;
            model.optField = "MSRNO";
            model.swset = "1";
            model.valifText = MSRIssuedModel.MSR.DocumentNo;

            foreach (var item in MSRIssuedModel.MSRIssuedDetails)
            {
                ShipmentDetailModel detail = new ShipmentDetailModel();
                detail.ITEMNO = item.ItemNo;

                ////Added by Erda on 20150106
                detail.CATEGORY = item.CostCode;

                detail.LOCATION = item.LocationFrom;
                detail.QUANTITY = item.QtyProcess.ToString();
                detail.UNIT = item.UOM;

                detail.OPTFIELD1 = "CCCODE";
                detail.OPTFIELD2 = "ECCODE";
                detail.OPTFIELD3 = "MTCODE";

                detail.SWSET1 = "1";
                detail.SWSET2 = "1";
                detail.SWSET3 = "1";

                detail.VALIFTEXT1 = MSRIssuedModel.MSR.CostCenter;
                detail.VALIFTEXT2 = "00";
                detail.VALIFTEXT3 = "00";

                detail.COMMENTS = item.Description;
                detail.MANITEMNO = "12345";
                detail.FUNCTION = "100";
                detail.LINENO = item.LineNo.ToString();

                model.ShipmentDetails.Add(detail);
            }

            return SaveShipment(model);
        }

        public static ResponseModel MSRSaveTransfers(MSRIssuedModel MSRIssuedModel)
        {
            TransferModel model = new TransferModel();

            Company company = Company.GetById(MSRIssuedModel.CompanyId);
            var dataSource = AccpacDataContext.GetDatabaseName(company.CompanyCode);

            model.DBUserName = SiteSettings.ACCPAC_USERNAME;
            model.DBPassword = SiteSettings.ACCPAC_PASSWORD;
            model.DBName = dataSource;
            model.docNum = MSRIssuedModel.MSRIssuedNo;
            model.description = (string.IsNullOrEmpty(MSRIssuedModel.Remarks) ? string.Empty : MSRIssuedModel.Remarks);
            model.reference = string.Format("{0};{1};{2}", MSRIssuedModel.MSRIssuedNo, MSRIssuedModel.MSR.DocumentNo, MSRIssuedModel.MSR.WorkOrderNo);
            model.transDate = MSRIssuedModel.MSR.DateCreated.ToString("yyyy,MM,dd");
            model.dateBus = DateTime.Now.ToString("yyyy,MM,dd");
            model.exparDate = MSRIssuedModel.MSR.ExpectedDate.ToString("yyyy,MM,dd");
            model.addCost = MSRIssuedModel.MSRIssuedDetails.Sum(x => x.QtyProcess * x.Cost).ToString();
            model.prorMethod = "3";
            model.status = "1";

            foreach (var item in MSRIssuedModel.MSRIssuedDetails)
            {
                TransferDetailModel detail = new TransferDetailModel();
                detail.ITEMNO = item.ItemNo;
                detail.FROMLOC = item.LocationFrom;
                detail.TOLOC = item.Location;
                detail.QTYREQ = item.QtyRequest.ToString();
                detail.QUANTITY = item.QtyProcess.ToString();
                detail.UNITREQ = item.UOM;
                detail.UNIT = item.UOM;
                detail.COMMENTS = (string.IsNullOrEmpty(MSRIssuedModel.Remarks) ? string.Empty : MSRIssuedModel.Remarks);
                detail.MANITEMNO = "M101";
                detail.EXTWEIGHT = "200.0000";
                detail.FUNCTION = "100";
                model.TransferDetails.Add(detail);
            }

            return SaveTransfers(model);
        }

        public static ResponseModel FuelSaveShipment(FRFModel frfModel)
        {
            ShipmentModel model = new ShipmentModel();
            Company company = Company.GetById(frfModel.CompanyId);
            var dataSource = AccpacDataContext.GetDatabaseName(company.CompanyCode);

            model.DBUserName = SiteSettings.ACCPAC_USERNAME;
            model.DBPassword = SiteSettings.ACCPAC_PASSWORD;
            model.DBName = dataSource;
            model.docNum = frfModel.FRFNo;
            model.description = string.Format("{0} FRF Import to Accpac automatically", frfModel.FRFNo);
            model.reference = "";
            model.transDate = DateTime.Now.ToString("yyyy,MM,dd");
            model.dateBus = DateTime.Now.ToString("yyyy,MM,dd");
            model.custNo = string.Empty;
            model.contact = string.Empty;
            model.status = "1";
            model.currency = company.Currency;
            model.optField = "MSRNO";
            model.swset = "1";
            model.valifText = frfModel.FRFNo;

            foreach (var item in frfModel.FRFDetails)
            {
                ShipmentDetailModel detail = new ShipmentDetailModel();
                detail.ITEMNO = item.ItemNo;
                detail.CATEGORY = item.Category;
                detail.UNIT = item.Unit;

                detail.LOCATION = item.LocationId;
                detail.QUANTITY = (item.Qty.HasValue ? item.Qty.Value.ToString() : "0");
                

                detail.OPTFIELD1 = "CCCODE";
                detail.OPTFIELD2 = "ECCODE";
                detail.OPTFIELD3 = "MTCODE";

                detail.SWSET1 = "1";
                detail.SWSET2 = "1";
                detail.SWSET3 = "1";

                detail.VALIFTEXT1 = item.CostCode;
                detail.VALIFTEXT2 = item.EquipmentCode;
                detail.VALIFTEXT3 = item.MaterialCode;

                detail.COMMENTS = "12345";
                detail.MANITEMNO = "12345";
                detail.FUNCTION = "100";

                model.ShipmentDetails.Add(detail);
            }

            return SaveShipment(model);
        }


        public static ResponseModel SaveShipment(ShipmentModel model)
        {
            API api = new API();
            api.BASE_URL = SiteSettings.ACCPAC_URL;
            var response = new ResponseModel();
            if (SiteSettings.IS_ACCPAC_CONNECT)
            {
                response = api.Post(URI_SAVE_SHIPMENT, model);

                if (response.IsSuccess)
                {
                    model = new ShipmentModel();
                    model.DBUserName = SiteSettings.ACCPAC_USERNAME;
                    model.DBPassword = SiteSettings.ACCPAC_PASSWORD;
                    model.DBName = model.DBName;
                    model.docNum = model.docNum;

                    response = api.Post(URI_POST_SHIPMENT, model);
                }
            }

            return response;
        }

        public static ResponseModel SaveTransfers(TransferModel model)
        {
            API api = new API();
            api.BASE_URL = SiteSettings.ACCPAC_URL;
            var response = new ResponseModel();
            if (SiteSettings.IS_ACCPAC_CONNECT)
            {
                response = api.Post(URI_SAVE_TRANSFERS, model);

                if (response.IsSuccess)
                {
                    model = new TransferModel();
                    model.DBUserName = SiteSettings.ACCPAC_USERNAME;
                    model.DBPassword = SiteSettings.ACCPAC_PASSWORD;
                    model.DBName = model.DBName;
                    model.docNum = model.docNum;

                    response = api.Post(URI_POST_TRANSFERS, model);
                }
            }

            return response;
        }
    }
}
