﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Omu.ValueInjecter;

using ESS.Data;
using ESS.Data.Model;
using ESS.Common;

namespace ESS.BusinessLogic
{
    public static class ChangeRequestLogic
    {
        public static ChangeRequestModel GetById(Guid ChangeRequestId)
        {
            var Model = new ChangeRequestModel();
            Model.ChangeRequestNo = string.Empty;

            var _ChangeRequest = ChangeRequest.GetByID(ChangeRequestId);

            if (_ChangeRequest != null)
            {
                Model.InjectFrom(_ChangeRequest);
                Model.ID = _ChangeRequest.ID;
                Model.ChangeRequestNo = _ChangeRequest.ChangeRequestNo;
                Model.RequestUser = UserLogic.GetByNIK(_ChangeRequest.NIKSite);
                Model.CreatedBy = UserLogic.GetByNIK(_ChangeRequest.CreatedBy);

                var appItem = CurrentDataContext.CurrentContext.ApplicationLists.FirstOrDefault(x => x.ModuleCode == _ChangeRequest.ModuleCode);
                if (appItem != null)
                {
                    Model.ModuleName = appItem.ModuleName;
                    Model.ApplicationCode = appItem.ApplicationCode;
                    Model.ApplicationName = appItem.ApplicationName;
                }


                Model.DateIssued = _ChangeRequest.DateIssued;
                Model.DateIssuedStr = _ChangeRequest.DateIssued.ToString(Constant.FORMAT_DATE_JRESOURCES);

                if (_ChangeRequest.DueDate.HasValue)
                {
                    Model.DueDateStr = _ChangeRequest.DueDate.Value.ToString(Constant.FORMAT_DATE_JRESOURCES);
                }

                Model.CreatedDateStr = _ChangeRequest.CreatedDate.ToString(Constant.FORMAT_DATE_JRESOURCES);
                Model.StatusWF = _ChangeRequest.StatusWF;

                if (!string.IsNullOrEmpty(Model.CreatedBy.NIKSite))
                {
                    Model.CreatedBy = UserLogic.GetByNIK(Model.CreatedBy.NIKSite);
                }


                Model.UserApproval = UserLogic.GetUserApprovalChangeRequest(_ChangeRequest.ChangeRequestNo);
                Model.UserApproval.ManagerCheck = _ChangeRequest.fMgr_nik;
                Model.UserApproval.GeneralManagerCheck = _ChangeRequest.fGMSite_nik;
                Model.UserApproval.GMHOCheck = _ChangeRequest.fGMHO_nik;
                Model.UserApproval.VPHOCheck = _ChangeRequest.fVPHO_nik;
                Model.UserApproval.ManagerITCheck = _ChangeRequest.fMgrIT_nik;
                Model.UserApproval.VPITCheck = _ChangeRequest.fVPIt_nik;
                Model.UserApproval.OwnerAppCheck = (_ChangeRequest.fOwnerApp.HasValue ? _ChangeRequest.fOwnerApp.Value : 0);

            }

            return Model;
        }

        public static void SetHistory(ChangeRequestModel model)
        {
            List<LogHistoryModel> logs = new List<LogHistoryModel>();

            var viewlogs = CurrentDataContext.CurrentContext.usp_log_history(model.ChangeRequestNo);
            if (viewlogs != null)
            {
                foreach (var item in viewlogs.ToList())
                {
                    LogHistoryModel newLog = new LogHistoryModel();
                    newLog.InjectFrom(item);

                    newLog.Nama = item.Nama;
                    newLog.NIKSite = item.NIKSITE;
                    newLog.Jabatan = item.nmJabat;

                    newLog.IsApprove = (item.IsApprove.HasValue ? (item.IsApprove.Value == 1 ? true : false) : false);
                    logs.Add(newLog);
                }
            }

            model.LogHistories = logs;
        }

    }
}
