﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using ESS.Common;
using ESS.Data;
using ESS.Data.Model;
using Omu.ValueInjecter;

namespace ESS.BusinessLogic
{
    public class DelegationLogic
    {        
        public ResponseModel Save(DelegationModel model)
        {
            var response = new ResponseModel();
            Delegation delegation = new Delegation();
            try
            {
                response = Validation(model);

                if (response.IsSuccess)
                {
                    delegation = Mapping(model);
                    delegation.Save<Delegation>();
                    response.SetSuccess("Delegation " + delegation.DelegationNo + " Success Created");
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null && ex.InnerException.Message != null)
                {
                    response.AddMessageList(ex.InnerException.Message);
                }
                else
                {
                    if (ex.Message != null)
                    {
                        response.AddMessageList(ex.Message);
                    }
                }

                response.MessageToString();
                response.SetError();
            }

            if(response.IsSuccess)
            {
                ADOHelper ExternalConnection;
                string queryAccpac = string.Empty;

                StringBuilder sbErrorApps = new StringBuilder();

                foreach (var item in delegation.DelegationDetails)
                {
                    switch (item.ApplicationName)
                    {
                        case "MSR":
                            try
                            {
                                ExternalConnection = new ADOHelper(ConfigurationManager.ConnectionStrings[CONNECTION_STRING.MSR_CONNECTION].ConnectionString);

                                ExternalConnection.ExecNonQueryProc("InsertMSRDelegation",
                                    "@NIKSiteFrom", delegation.CreatedBy,
                                    "@NIKSiteTo", item.NIKSite,
                                    "@DateFrom", item.DateFrom,
                                    "@DateTo", item.DateTo
                                    );
                            }
                            catch (Exception ex)
                            {
                                sbErrorApps.AppendLine("MSR Error When Insert");
                            }

                            break;

                        case "ACCPAC":
                            try
                            {
                                string ConCode = string.Format("{0}_{1}", CONNECTION_STRING.ACCPAC_CONNECTION, UserLogic.GetCurrentSite);
                                ExternalConnection = new ADOHelper(ConfigurationManager.ConnectionStrings[ConCode].ConnectionString);
                                queryAccpac = "INSERT INTO PTSUB (USERID, BEGINDATE, AUDTDATE, AUDTTIME, AUDTUSER, AUDTORG, ENDDATE, REFUSERID) " +
                                    "VALUES(@USERID, @BEGINDATE, @AUDTDATE, @AUDTTIME, @AUDTUSER, @AUDTORG, @ENDDATE, @REFUSERID)";

                                UserModel userFrom = UserLogic.GetCurrentUser();
                                UserModel userTarget = UserLogic.GetByNIK(item.NIKSite);

                                ExternalConnection.ExecNonQuery(queryAccpac,
                                    "@USERID", userFrom.UserIdAccpac,
                                    "@BEGINDATE", string.Format("{0}{1}{2}", item.DateFrom.Year, item.DateFrom.Month.ToString("00"), item.DateFrom.Day.ToString("00")),
                                    "@AUDTDATE", string.Format("{0}{1}{2}", item.DateFrom.Year, item.DateFrom.Month.ToString("00"), item.DateFrom.Day.ToString("00")),
                                    "@AUDTTIME", item.DateTo.TimeOfDay.TotalMinutes,
                                    "@AUDTUSER", userFrom.UserIdAccpac,
                                    "@AUDTORG", ExternalConnection.DatabaseName,
                                    "@ENDDATE", string.Format("{0}{1}{2}", item.DateTo.Year, item.DateTo.Month.ToString("00"), item.DateTo.Day.ToString("00")),
                                    "@REFUSERID", userTarget.UserIdAccpac
                                    );
                            }
                            catch (Exception ex)
                            {
                                sbErrorApps.AppendLine("ACCPAC Error When Insert");
                            }

                            break;

                        case "OVERTIME":

                            try
                            {
                                ExternalConnection = new ADOHelper(ConfigurationManager.ConnectionStrings[CONNECTION_STRING.ESS_CONNECTION].ConnectionString);
                                queryAccpac = "INSERT INTO H_H11002 (delegation_id, nikreq, nikdel, date_from, date_to) " +
                                    "VALUES(@delegation_id, @nikreq, @nikdel, @date_from, @date_to)";

                                UserModel userFrom = UserLogic.GetCurrentUser();
                                UserModel userTarget = UserLogic.GetByNIK(item.NIKSite);

                                ExternalConnection.ExecNonQuery(queryAccpac,
                                    "@delegation_id", Guid.NewGuid(),
                                    "@nikreq", delegation.CreatedBy,
                                    "@nikdel", item.NIKSite,
                                    "@date_from", string.Format("{0}{1}{2}", item.DateFrom.Year, item.DateFrom.Month.ToString("00"), item.DateFrom.Day.ToString("00")),
                                    "@date_to", string.Format("{0}{1}{2}", item.DateFrom.Year, item.DateFrom.Month.ToString("00"), item.DateFrom.Day.ToString("00"))
                                    );
                            }
                            catch (Exception ex)
                            {
                                sbErrorApps.AppendLine("OVERTIME Error When Insert");
                            }


                            break;

                    }
                }

                //if(sbErrorApps.Length > 0)
                //{
                //    sbErrorApps.AppendLine("Info To IT Business Support For Error");
                //    response.Message = response.Message + Environment.NewLine + sbErrorApps.ToString();
                //}

                
                SendEmail(delegation.DelegationNo);


            }

            
            return response;
        }

        private Delegation Mapping(DelegationModel model)
        {
            UserModel CurrentUser = UserLogic.GetCurrentUser();            

            Delegation _delegation = new Delegation();
            _delegation.ID = Guid.NewGuid();
            _delegation.DelegationNo = GetDocumentNumber(CurrentUser.CompanyCode);
            _delegation.DelegationTo = string.Join("|", model.ListDelegationTo.Select(x => x.NIKSite).ToArray());
            if (model.ListDelegationCC.Count > 0)
            {
                _delegation.DelegationCC = string.Join("|", model.ListDelegationCC.Select(x => x.NIKSite).ToArray());
            }
            else
            {
                _delegation.DelegationCC = string.Empty;
            }

            _delegation.CompanyCode = CurrentUser.CompanyCode;
            _delegation.CompanyName = CurrentUser.CompanyName;
            _delegation.DepartmentCode = CurrentUser.DepartmentCode;
            _delegation.DepartmentName = CurrentUser.DepartmentName;
            _delegation.DateFrom = model.DateFrom;
            _delegation.DateTo = model.DateTo;
            _delegation.CreatedBy = CurrentUser.NIKSite;
            _delegation.CreatedDate = DateTime.Now;
            _delegation.Reason = model.Reason;

            foreach (var item in model.DelegationDetails)
            {
                UserModel userDelegation = UserLogic.GetByNIK(item.NIKSite);

                DelegationDetail newDetail = new DelegationDetail();
                newDetail.ID = Guid.NewGuid();
                newDetail.DelegationId = _delegation.ID;
                newDetail.ApplicationName = item.ApplicationName;
                newDetail.NIKSite = item.NIKSite;
                newDetail.Fullname = userDelegation.FullName;
                newDetail.CompanyCode = userDelegation.CompanyCode;
                newDetail.CompanyName = userDelegation.CompanyName;
                newDetail.DepartmentCode = userDelegation.DepartmentCode;
                newDetail.DepartmentName = userDelegation.DepartmentName;
                newDetail.Position = userDelegation.Jabatan;
                newDetail.DateFrom = item.DateFrom;
                newDetail.DateTo = item.DateTo;
                newDetail.Task = item.Task;


                _delegation.DelegationDetails.Add(newDetail);
            }

            return _delegation;
        }

        private ResponseModel Validation(DelegationModel model)
        {
            var response = new ResponseModel();
            UserModel CurrentUser = UserLogic.GetCurrentUser();

            StringBuilder sbError = new StringBuilder();

            if (!(model.ListDelegationTo != null && model.ListDelegationTo.Count > 0))
            {
                sbError.AppendLine("List To cannot be empty");
            }

            if (string.IsNullOrEmpty(model.DateFromStr) && string.IsNullOrEmpty(model.DateToStr))
            {
                sbError.AppendLine("Date from and to cannot be blank");
            }
            else
            {
                var dateConvert = Common.CommonFunction.DateTimeESS(model.DateFromStr);
                if (dateConvert != DateTime.MinValue)
                {
                    model.DateFrom = Common.DateHelper.DateFromMinimun(dateConvert);
                }

                dateConvert = Common.CommonFunction.DateTimeESS(model.DateToStr);
                if (dateConvert != DateTime.MinValue)
                {
                    model.DateTo = Common.DateHelper.DateToMax(dateConvert);
                }

                if (model.DateFrom >= model.DateTo)
                {
                    sbError.AppendLine("Date to cannot be higher from date first");
                }
            }

            if (string.IsNullOrEmpty(model.Reason))
            {
                sbError.AppendLine("Alasan Cuti tidak boleh kosong");
            }

            if (!(model.DelegationDetails != null && model.DelegationDetails.Count > 0))
            {
                sbError.AppendLine("List Task Delegation Cannot Empty");
            }
            else
            {
                int rowTask = 1;
                foreach (var item in model.DelegationDetails)
                {
                    if (string.IsNullOrEmpty(item.NIKSite))
                    {
                        sbError.AppendLine("Task " + rowTask + " : User must be select");
                    }
                    else
                    {
                        ADOHelper ConnectionCheck;
                        string queryCheckUser = string.Empty;
                        object userResult;

                        switch (item.ApplicationName)
                        {
                            case "ACCPAC":
                                string ConCode = string.Format("{0}_{1}", CONNECTION_STRING.ACCPAC_CONNECTION, UserLogic.GetCurrentSite);
                                ConnectionCheck = new ADOHelper(ConfigurationManager.ConnectionStrings[ConCode].ConnectionString);

                                queryCheckUser = "SELECT USERID FROM PTUSER WHERE INACTIVE = 0 AND POSITION = @NIKSiteTo";
                                userResult = ConnectionCheck.ExecScalar(queryCheckUser, "@NIKSiteTo", item.NIKSite);
                                if (!(userResult != null && !string.IsNullOrEmpty(userResult.ToString())))
                                {
                                    sbError.AppendLine(string.Format("Task " + rowTask + " : User {0} not found in ACCPAC", item.Fullname));
                                }

                                queryCheckUser = "SELECT USERID FROM PTSUB WHERE USERID = @USERID AND REFUSERID = @REFUSERID AND " +
                                                 "(BEGINDATE >= @BEGINDATE OR ENDDATE <= @ENDDATE)";

                                userResult = ConnectionCheck.ExecScalar(queryCheckUser,
                                    "@USERID", CurrentUser.NIKSite,
                                    "@BEGINDATE", string.Format("{0}{1}{2}", item.DateFrom.Year, item.DateFrom.Month.ToString("00"), item.DateFrom.Day.ToString("00")),
                                    "@ENDDATE", string.Format("{0}{1}{2}", item.DateTo.Year, item.DateTo.Month.ToString("00"), item.DateTo.Day.ToString("00")),
                                    "@REFUSERID", item.NIKSite
                                    );

                                if (userResult != null && !string.IsNullOrEmpty(userResult.ToString()))
                                {
                                    sbError.AppendLine(string.Format("Task " + rowTask + " : User {0} has been delegate in ACCPAC", item.Fullname));
                                }


                                break;

                            case "MSR":
                                ConnectionCheck = new ADOHelper(ConfigurationManager.ConnectionStrings[CONNECTION_STRING.MSR_CONNECTION].ConnectionString);
                                queryCheckUser = "SELECT FullName FROM UserLogin WHERE NIKSite = @NIKSiteTo";
                                userResult = ConnectionCheck.ExecScalar(queryCheckUser, "@NIKSiteTo", item.NIKSite);
                                if (!(userResult != null && !string.IsNullOrEmpty(userResult.ToString())))
                                {
                                    sbError.AppendLine(string.Format("Task " + rowTask + " : User {0} not found in MSR", item.Fullname));
                                }

                                break;
                        }
                    }

                    if (string.IsNullOrEmpty(item.ApplicationName))
                    {
                        sbError.AppendLine("Task " + rowTask + " : Application must be select");
                    }

                    if (string.IsNullOrEmpty(item.Task))
                    {
                        sbError.AppendLine("Task " + rowTask + " : task deskripsi harus di isi");
                    }

                    if (string.IsNullOrEmpty(item.DateFromStr) && string.IsNullOrEmpty(item.DateToStr))
                    {
                        sbError.AppendLine("Task " + rowTask + " : date from and to cannot be blank");
                    }
                    else
                    {
                        var dateConvert = Common.CommonFunction.DateTimeESS(item.DateFromStr);
                        if (dateConvert != DateTime.MinValue)
                        {
                            item.DateFrom = Common.DateHelper.DateFromMinimun(dateConvert);
                        }

                        dateConvert = Common.CommonFunction.DateTimeESS(item.DateToStr);
                        if (dateConvert != DateTime.MinValue)
                        {
                            item.DateTo = Common.DateHelper.DateToMax(dateConvert);
                        }

                        if (item.DateFrom >= item.DateTo)
                        {
                            sbError.AppendLine("Task " + rowTask + " : date to cannot be higher from date first");
                        }

                        if(!(item.DateFrom >= model.DateFrom && item.DateFrom <= model.DateTo && item.DateTo >= model.DateFrom && item.DateTo <= model.DateTo))
                        {
                            sbError.AppendLine("Task " + rowTask + " : date to must in range from absence date");
                        }
                    }
                }
            }



            //if(sbError.Length < 1)
            //{
            //    var groupDelegationDetails = model.DelegationDetails
            //        .GroupBy(u => u.ApplicationName)
            //        .Select(grp => grp.ToList())
            //        .ToList();

            //    foreach (var groupDelegations in groupDelegationDetails)
            //    {
            //        List<DelegationDetailModel> listItemCheck = new List<DelegationDetailModel>();

            //        if(groupDelegations.Count > 1)
            //        {
            //            foreach (var itemDelegation in groupDelegations)
            //            {
            //                var checkDelegation = listItemCheck.Where(x => 
            //                x.DateTo > itemDelegation.DateFrom
            //                ).ToList();

            //                if(checkDelegation != null && checkDelegation.Count > 0)
            //                {
            //                    sbError.AppendLine("Delegation " + itemDelegation.ApplicationName +  " was conflict");
            //                } else
            //                {
            //                    listItemCheck.Add(itemDelegation);
            //                }
            //            }
            //        }
            //    }

            //}


            if (sbError.Length > 0)
            {
                response.SetError(sbError.ToString());
            }
            else
            {
                response.SetSuccess();
            }

            return response;
        }

        private string GetDocumentNumber(string KdSite)
        {
            int lastNumber = 0;

            string DocumentNumber = string.Format("DEL-{0}/{1}/{2}", KdSite.ToUpper(), DateTime.Now.Year, (lastNumber + 1).ToString("D4"));
            return DocumentNumber;
        }

        private void SendEmail(string _delegationNo)
        {
            var model = DelegationLogic.GetByDelegationNo(_delegationNo);

            try
            {
                var fromAddress = new MailAddress(SiteSettings.EMAIL_USERNAME, "noreply@jresources.com");

                var smtp = new SmtpClient
                {
                    Host = SiteSettings.EMAIL_SMTP,
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(SiteSettings.EMAIL_USERNAME, SiteSettings.EMAIL_PASSWORD)
                };

                var message = new MailMessage();
                message.IsBodyHtml = true;
                message.From = fromAddress;
                message.To.Add(new MailAddress("nuril.umam@jresources.com", "Nuril Umam"));

                foreach (var item in model.ListDelegationTo)
                {
                    if(!string.IsNullOrEmpty(item.Email) || item.Email == "prisilia.amansyah@jresources.com")
                    {
                        message.To.Add(new MailAddress(item.Email, item.FullName));
                    }                    
                }

                foreach (var item in model.ListDelegationCC)
                {
                    if (!string.IsNullOrEmpty(item.Email) || item.Email == "prisilia.amansyah@jresources.com")
                    {
                        //message.CC.Add(new MailAddress(item.Email, item.FullName));
                    }
                }


                message.Subject = string.Format("Delegation No : {0}", model.DelegationNo);

                StringBuilder sbBodyMail = new StringBuilder();
                sbBodyMail.AppendLine("<table>");
                sbBodyMail.AppendLine("<tbody>");

                sbBodyMail.AppendLine("<tr>");
                sbBodyMail.AppendLine("<td>DELEGATION NO</td>");
                sbBodyMail.AppendLine("<td>:</td>");
                sbBodyMail.AppendLine(string.Format("<td>{0}</td>", model.DelegationNo));
                sbBodyMail.AppendLine("</tr>");

                sbBodyMail.AppendLine("<tr>");
                sbBodyMail.AppendLine("<td>From</td>");
                sbBodyMail.AppendLine("<td>:</td>");
                sbBodyMail.AppendLine(string.Format("<td>{0} ({1})</td>", model.UserCreated.FullName, model.UserCreated.Jabatan));
                sbBodyMail.AppendLine("</tr>");

                sbBodyMail.AppendLine("<tr>");
                sbBodyMail.AppendLine("<td>Absence Date</td>");
                sbBodyMail.AppendLine("<td>:</td>");
                sbBodyMail.AppendLine(string.Format("<td>{0} - {1}</td>", model.DateFromStr, model.DateToStr));
                sbBodyMail.AppendLine("</tr>");

                

                sbBodyMail.AppendLine("<tr>");
                sbBodyMail.AppendLine("<td>Reason</td>");
                sbBodyMail.AppendLine("<td>:</td>");
                sbBodyMail.AppendLine(string.Format("<td>{0}</td>", model.Reason));
                sbBodyMail.AppendLine("</tr>");

                sbBodyMail.AppendLine("<tr>");
                sbBodyMail.AppendLine("<td style='vertical-align: top;'>Task</td>");
                sbBodyMail.AppendLine("<td>:</td>");
                sbBodyMail.AppendLine(string.Format("<td>{0}</td>", model.Task.Replace("\r\n", "<br />")));
                sbBodyMail.AppendLine("</tr>");

                sbBodyMail.AppendLine("</tbody>");
                sbBodyMail.AppendLine("</table>");
                message.Body = sbBodyMail.ToString();

                //smtp.Send(message);
            }
            catch (Exception ex)
            {
                
            }

        }

        public static DelegationModel GetByDelegationId(Guid DelegationId)
        {
            if(DelegationId != Guid.Empty)
            {
                Delegation _delegation = Delegation.GetByDelegationId(DelegationId);
                if(_delegation != null)
                {
                    return GetByDelegationNo(_delegation.DelegationNo);
                } else
                {
                    return null;
                }                
            } else
            {
                return null;
            }            
        }

        public static DelegationModel GetByDelegationNo(string _delegationNo)
        {
            DelegationModel model = new DelegationModel();
            Delegation _delegation = Delegation.GetByDelegationNo(_delegationNo);
            model.InjectFrom(_delegation);
            model.Reason = _delegation.Reason;
            model.DateFromStr = _delegation.DateFrom.ToString(Constant.FORMAT_DATE_JRESOURCES);
            model.DateToStr = _delegation.DateTo.ToString(Constant.FORMAT_DATE_JRESOURCES);
            model.CreatedDateStr = _delegation.CreatedDate.ToString(Constant.FORMAT_DATE_JRESOURCES);
            model.UserCreated = UserLogic.GetByNIK(_delegation.CreatedBy);

            string[] userIds = _delegation.DelegationTo.Split('|');
            StringBuilder sbName = new StringBuilder();
            foreach (var userId in userIds)
            {
                var _user = UserLogic.GetByNIK(userId);
                sbName.AppendLine(string.Format("{0} ({1})", _user.FullName, _user.Jabatan));

                model.ListDelegationTo.Add(_user);
            }
            model.DelegationTo = sbName.ToString();

            model.DelegationCC = string.Empty;
            if(!string.IsNullOrEmpty(_delegation.DelegationCC))
            {
                userIds = _delegation.DelegationCC.Split('|');
                sbName = new StringBuilder();
                foreach (var userId in userIds)
                {
                    var _user = UserLogic.GetByNIK(userId);
                    sbName.AppendLine(string.Format("{0} ({1})", _user.FullName, _user.Jabatan));
                    model.ListDelegationCC.Add(_user);
                }
                model.DelegationCC = sbName.ToString();
            }

            StringBuilder sbTask = new StringBuilder();
            foreach (var item in _delegation.DelegationDetails)
            {
                DelegationDetailModel _detail = new DelegationDetailModel();
                _detail.InjectFrom(item);
                _detail.Task = item.Task;
                _detail.UserDelegation = UserLogic.GetByNIK(item.NIKSite);
                _detail.DateFromStr = item.DateFrom.ToString(Constant.FORMAT_DATE_JRESOURCES);
                _detail.DateToStr = item.DateTo.ToString(Constant.FORMAT_DATE_JRESOURCES);


                model.ListDelegationTo.Add(_detail.UserDelegation);
                model.DelegationDetails.Add(_detail);

                sbTask.AppendLine(string.Format("{0} - {1}", _detail.ApplicationName, _detail.UserDelegation.FullName));
                sbTask.AppendLine(item.Task);
                sbTask.AppendLine(string.Empty);
            }

            model.Task = sbTask.ToString();



            return model;
        }
    }
}
