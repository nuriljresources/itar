﻿using ESS.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ESS.BusinessLogic
{
    public class DocumentNumberLogic
    {
        public static String GetDocumentNo(string _documentType)
        {            
            DocumentNumber docNumer = DocumentNumber.GetByDocumentType(_documentType, UserLogic.GetCurrentCompanyCode);
            int runningNumber = 1;
            if(!(docNumer != null))
            {
                docNumer = new DocumentNumber();
                docNumer.ID = Guid.NewGuid();
                docNumer.CompanyCode = UserLogic.GetCurrentCompanyCode;
                docNumer.DocumentType = _documentType;
                docNumer.Year = DateTime.Now.Year;
                docNumer.RunningNumber = runningNumber;                
            } else
            {
                runningNumber = docNumer.RunningNumber + 1;
                docNumer.RunningNumber = runningNumber;
            }

            docNumer.Save<DocumentNumber>();
            string _documentNumber = String.Format("{0}-{1}/{2}/{3}", _documentType, docNumer.CompanyCode, docNumer.Year, runningNumber.ToString("D4"));
            return _documentNumber;
        }
    }
}
