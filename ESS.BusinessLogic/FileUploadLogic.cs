﻿using ESS.Data;
using ESS.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Omu.ValueInjecter;
using ESS.Common;
using System.IO;

namespace ESS.BusinessLogic
{
    public class FileUploadLogic
    {
        public static List<FileUploadModel> GetListFile(String documentNo)
        {
            List<FileUploadModel> list = new List<FileUploadModel>();
            var files = FileUpload.GetByDocumentNo(documentNo);
            foreach (var item in files)
            {
                FileUploadModel fileItem = new FileUploadModel();
                fileItem.InjectFrom(item);
                fileItem.LinkDownloadFile = Path.Combine(SiteSettings.BASE_URL, fileItem.FullPathFile);

                list.Add(fileItem);
            }

            return list;
        }

        public static ResponseModel DeleteFile(Guid fileId)
        {
            ResponseModel result = new ResponseModel();
            FileUpload fileUpload = FileUpload.GetById(fileId);
            if(fileUpload != null && fileUpload.ID != Guid.Empty)
            {



                result.SetSuccess();
            } else
            {
                result.SetError("File Not Found");
            }

            return result;
        }
    }
}
