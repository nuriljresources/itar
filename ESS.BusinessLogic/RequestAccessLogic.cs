﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Omu.ValueInjecter;

using ESS.Data;
using ESS.Data.Model;
using ESS.Common;

namespace ESS.BusinessLogic
{
    public static class RequestAccessLogic
    {
        public static RequestAccessModel GetById(Guid RequestAccessId)
        {
            var Model = new RequestAccessModel();
            Model.RequestAccessNo = string.Empty;

            var _requestAccess = RequestAccess.GetByID(RequestAccessId);

            if(_requestAccess != null)
            {
                Model.InjectFrom(_requestAccess);
                Model.ID = _requestAccess.ID;
                Model.RequestAccessNo = _requestAccess.RequestAccessNo;
                Model.RequestUser = UserLogic.GetByNIK(_requestAccess.NIKSite);
                Model.Notes = _requestAccess.RequestDesc;
                Model.IsEmailAddresAccess = _requestAccess.IsEmail;
                Model.IsUSBPortAccess = _requestAccess.IsUSBAccess;
                Model.IsAccpacAccess = _requestAccess.IsAccpacAccess;
                Model.IsVPNAccess = _requestAccess.IsVPNAccess;
                Model.IsEmailAddresToExtAccess = _requestAccess.IsEmailExtended;
                Model.IsInternetAccess = _requestAccess.IsInternetAccess;
                Model.IsSharepointAccess = _requestAccess.IsSharepointAccess;
                Model.IsEmailDomainRegister = _requestAccess.IsEmailDomain;
                Model.IsMSR = _requestAccess.IsMSRAccess;

                if (Model.IsDocumentCenter)
                {
                    if (Model.AppointmentDate.HasValue)
                    {
                        Model.AppointmentDateStr = DateTime.Now.ToString(Constant.FORMAT_DATE_JRESOURCES);
                    }

                    Model.AppointmentUser = UserLogic.GetByNIK(_requestAccess.AppointmentNIKSite);
                }

                Model.RequestStatus = CommonFunction.StatusWorkFlowText(_requestAccess.StatusWorkflow);
                Model.StatusWorkFlow = _requestAccess.StatusWorkflow;

                if (!string.IsNullOrEmpty(Model.RequestUser.NIKSite))
                {
                    Model.RequestUser = UserLogic.GetByNIK(Model.RequestUser.NIKSite);
                    if (Model.IsDocumentCenter)
                    {
                        if (Model.RequestUser != null && !String.IsNullOrEmpty(Model.RequestUser.FullName))
                        {
                            var userApproval = UserLogic.GetUserApprovalAccessDocCenter(Model.RequestUser.NIKSite);
                            Model.Manager = userApproval.Manager;
                            Model.VP = userApproval.VP;
                            Model.ManagerIT = userApproval.ManagerIT;
                            Model.VPIT = userApproval.VPIT;
                            Model.ITAdmin = userApproval.ITAdmin;
                            Model.ITSPV = userApproval.ITSPV;
                        }
                    }
                    else
                    {
                        if (Model.RequestUser != null && !String.IsNullOrEmpty(Model.RequestUser.FullName))
                        {
                            var userApproval = UserLogic.GetUserApproval(Model.RequestUser.NIKSite);
                            Model.Manager = userApproval.Manager;
                            Model.VP = userApproval.VP;
                            Model.ManagerIT = userApproval.ManagerIT;
                            Model.VPIT = userApproval.VPIT;
                            Model.ITAdmin = userApproval.ITAdmin;
                            Model.ITSPV = userApproval.ITSPV;
                        }
                    }
                }

            }

            return Model;
        }

        public static void SetHistory(RequestAccessModel model)
        {
            List<LogHistoryModel> logs = new List<LogHistoryModel>();

            var viewlogs = CurrentDataContext.CurrentContext.usp_log_history(model.RequestAccessNo);
            if(viewlogs != null)
            {
                foreach (var item in viewlogs.ToList())
                {
                    LogHistoryModel newLog = new LogHistoryModel();
                    newLog.InjectFrom(item);
                    
                    newLog.Nama = item.Nama;
                    newLog.NIKSite = item.NIKSITE;
                    newLog.Jabatan = item.nmJabat;
                    
                    newLog.IsApprove = (item.IsApprove.HasValue ? (item.IsApprove.Value == 1 ? true : false) : false);
                    logs.Add(newLog);
                }
            }

            model.LogHistories = logs;
        }

    }
}
