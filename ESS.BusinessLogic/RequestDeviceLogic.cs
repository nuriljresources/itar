﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Omu.ValueInjecter;

using ESS.Data;
using ESS.Data.Model;
using ESS.Common;

namespace ESS.BusinessLogic
{
    public static class RequestDeviceLogic
    {
        public static RequestDeviceModel GetById(Guid RequestAccessId)
        {
            var Model = new RequestDeviceModel();
            Model.RequestDeviceNo = string.Empty;

            var _requestDevice = RequestDevice.GetByID(RequestAccessId);

            if (_requestDevice != null)
            {
                Model.InjectFrom(_requestDevice);
                Model.ID = _requestDevice.ID;
                Model.RequestDeviceNo = _requestDevice.RequestDeviceNo;
                Model.RequestUser = UserLogic.GetByNIK(_requestDevice.NIKSite);
                Model.Notes = _requestDevice.RequestDesc;
                

                Model.RequestStatus = CommonFunction.StatusWorkFlowText(_requestDevice.StatusWorkflow);
                Model.StatusWorkFlow = _requestDevice.StatusWorkflow;

                if (!string.IsNullOrEmpty(Model.RequestUser.NIKSite))
                {
                    Model.RequestUser = UserLogic.GetByNIK(Model.RequestUser.NIKSite);
                    if (Model.RequestUser != null && !String.IsNullOrEmpty(Model.RequestUser.FullName))
                    {
                        var userApproval = UserLogic.GetUserApproval(Model.RequestUser.NIKSite);
                        Model.Manager = userApproval.Manager;
                        Model.VP = userApproval.VP;
                        Model.ManagerIT = userApproval.ManagerIT;
                        Model.VPIT = userApproval.VPIT;
                        Model.ITAdmin = userApproval.ITAdmin;
                        Model.ITSPV = userApproval.ITSPV;
                    }
                }

                foreach (var item in _requestDevice.RequestDeviceItems)
                {
                    RequestDeviceItemModel itemDevice = new RequestDeviceItemModel();
                    itemDevice.InjectFrom(item);

                    Model.RequestDeviceItems.Add(itemDevice);
                }   


            }

            return Model;
        }

        public static void SetHistory(RequestDeviceModel model)
        {
            List<LogHistoryModel> logs = new List<LogHistoryModel>();
            var viewlogs = CurrentDataContext.CurrentContext.usp_log_history(model.RequestDeviceNo);
            if (viewlogs != null)
            {
                foreach (var item in viewlogs.ToList())
                {
                    LogHistoryModel newLog = new LogHistoryModel();
                    newLog.InjectFrom(item);
                    newLog.Nama = item.Nama;
                    newLog.NIKSite = item.NIKSITE;
                    newLog.Jabatan = item.nmJabat;
                    newLog.IsApprove = (item.IsApprove.HasValue ? (item.IsApprove.Value == 1 ? true : false) : false);
                    logs.Add(newLog);
                }
            }

            model.LogHistories = logs;
        }

    }
}
