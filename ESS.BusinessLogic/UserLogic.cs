﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using ESS.Common;
using ESS.Data;
using ESS.Data.Model;
using System.DirectoryServices;
using System.Data.SqlClient;

namespace ESS.BusinessLogic
{
    public class UserLogic
    {
        public static string GetCurrentNIKSite
        {
            get
            {
                return CookieManager.Get(COOKIES_NAME.USER_NIK);
            }
        }

        public static string GetCurrentSite
        {
            get
            {
                return CookieManager.Get(COOKIES_NAME.USER_SITE_CODE).ToUpper();
            }
        }

        public static string GetCurrentCompanyCode
        {
            get
            {
                return CookieManager.Get(COOKIES_NAME.USER_COMPANY_CODE).ToUpper();
            }
        }

        public static string GetCurrentName
        {
            get
            {
                return CookieManager.Get(COOKIES_NAME.FULL_NAME);
            }
        }

        public static UserModel GetCurrentUser()
        {
            var nik = CookieManager.Get(COOKIES_NAME.USER_NIKSITE);
            return GetByNIK(nik);
        }

        public static string GetCurrentAccess
        {
            get
            {
                return GetCurrentUser().RoleAccess;
            }
        }

        public static UserModel GetByNIK(string NIK)
        {
            UserModel model = new UserModel();
            V_ITAS_EMP user = V_ITAS_EMP.GetByNIKSite(NIK);

            if (user != null && !string.IsNullOrEmpty(user.KdSite))
            {
                model.FullName = user.Nama;
                model.NIK = user.nik;
                model.NIKSite = user.NIKSITE;
                model.PhoneNumber = "";
                model.Email = (!string.IsNullOrEmpty(user.email) ? user.email:"");
                model.Jabatan = user.nmJabat;
                model.SiteCode = user.KdSite;
                model.CompanyCode = user.kdCompany;
                model.CompanyName = user.nmSite;
                model.DepartmentCode = user.KdDepar;
                model.DepartmentName = user.nmDepar;
                model.RoleAccess = "";
                model.kdFunct = user.kdFunct;
                model.kdDivisi = user.kdDivisi;
                model.kdLevel = user.KdLevel;

                UserAccess ua = UserAccess.GetByNIKSite(user.NIKSITE);
                if (ua != null)
                {
                    model.RoleAccess = ua.RoleAccess;
                }
                else
                {
                    model.RoleAccess = ROLE.USERS;
                }

                //if (!string.IsNullOrEmpty(UserLogic.GetCurrentSite))
                //{
                //    string ConCode = string.Format("{0}_{1}", CONNECTION_STRING.ACCPAC_CONNECTION, UserLogic.GetCurrentSite);
                //    var ConnectionCheck = new ADOHelper(ConfigurationManager.ConnectionStrings[ConCode].ConnectionString);

                //    string queryCheckUser = "SELECT USERID FROM PTUSER WHERE INACTIVE = 0 AND POSITION = @NIKSiteTo";
                //    var userResult = ConnectionCheck.ExecScalar(queryCheckUser, "@NIKSiteTo", NIK);
                //    if (userResult != null && !string.IsNullOrEmpty(userResult.ToString()))
                //    {
                //        model.UserIdAccpac = userResult.ToString();
                //    }

                //    ConnectionCheck = new ADOHelper(ConfigurationManager.ConnectionStrings[CONNECTION_STRING.MSR_CONNECTION].ConnectionString);

                //    queryCheckUser = "SELECT A.Username FROM UserLogin A " +
                //        "INNER JOIN RoleAccessUser B ON A.ID = B.UserId " +
                //        "INNER JOIN RoleAccess C ON B.RoleAccessId = C.ID " +
                //        "WHERE C.RoleAccessCode IN('MSR_APPROVAL', 'MSR_APPROVAL_GM') AND NIKSite = @NIKSiteTo";
                //    userResult = ConnectionCheck.ExecScalar(queryCheckUser, "@NIKSiteTo", NIK);
                //    if (userResult != null && !string.IsNullOrEmpty(userResult.ToString()))
                //    {
                //        model.UserIdMSR = userResult.ToString();
                //    }
                //}
            }

            return model;
        }

        public static UserApprovalModel GetUserApproval(string NIKSite)
        {
            UserApprovalModel result = new UserApprovalModel();

            if (!String.IsNullOrEmpty(NIKSite))
            {
                var user = UserLogic.GetByNIK(NIKSite);
                if (user != null && !String.IsNullOrEmpty(user.FullName))
                {
                    var employee = V_ITAS_EMP.GetByNIKSite(NIKSite);
                    if (employee != null && !String.IsNullOrEmpty(employee.Nama))
                    {
                        if (!String.IsNullOrEmpty(employee.Manager))
                        {
                            result.Manager = UserLogic.GetByNIK(employee.Manager);
                        }

                        var VP = V_ITADR_APPROVAL_VP.GetByNIK(user.NIK);
                        if (VP != null && !String.IsNullOrEmpty(VP.nik))
                        {
                            result.VP = UserLogic.GetByNIK(VP.VPNiksite);
                        }

                        result.ManagerIT = UserLogic.GetByNIK(SiteSettings.MANAGER_IT);
                        result.VPIT = UserLogic.GetByNIK(SiteSettings.VP_IT);

                        result.ITAdmin = UserLogic.GetByNIK(SiteSettings.IT_ADMIN);
                        result.ITSPV = UserLogic.GetByNIK(SiteSettings.IT_SPV);
                    }
                }

            }

            return result;
        }

        public static UserApprovalModel GetUserApprovalAccessDocCenter(string NIKSite)
        {
            UserApprovalModel result = new UserApprovalModel();

            if (!String.IsNullOrEmpty(NIKSite))
            {
                var user = UserLogic.GetByNIK(NIKSite);
                
                if (user != null && !String.IsNullOrEmpty(user.FullName))
                {
                    var employee = V_ITAS_EMP.GetByNIKSite(NIKSite);
                    var employeeManagerFunct = V_ITAS_EMP.GetByNiksiteFunctDiv(user.kdFunct, user.kdDivisi, "3");
                    var employeeGMFunct = V_ITAS_EMP.GetByNiksiteFunctDiv(user.kdFunct, user.kdDivisi, "4");
                    var employeeVPFunct = V_ITAS_EMP.GetByNiksiteFunctDiv(user.kdFunct, "00", "5");
                    if (employee != null && !String.IsNullOrEmpty(employee.Nama))
                    {
                        if (!String.IsNullOrEmpty(employee.GMFunc))
                        {
                            if (user.kdFunct == "34")
                            {
                                if (employeeManagerFunct != null)
                                {
                                    result.Manager = UserLogic.GetByNIK(employeeManagerFunct.NIKSITE);
                                }
                                else
                                {
                                    result.Manager = UserLogic.GetByNIK(employee.GMFunc);
                                }
                                
                            }
                            else if (user.kdFunct == "25" || user.kdFunct == "26" || user.kdFunct == "32" || user.kdFunct == "40")
                            {
                                if (employeeGMFunct != null)
                                {
                                    result.Manager = UserLogic.GetByNIK(employeeGMFunct.NIKSITE);
                                }
                                else
                                {
                                    result.Manager = UserLogic.GetByNIK(employee.GMFunc);
                                }
                                
                            }
                            else if (user.kdFunct == "33")
                            {
                                result.Manager = UserLogic.GetByNIK(employee.GMFunc);
                                result.Manager.Jabatan = "General Manager - Sustainibility";
                            }
                            else
                            {
                                result.Manager = UserLogic.GetByNIK(employee.GMFunc);
                            }
                        }
                        else
                        {
                            if (employeeGMFunct != null)
                            {
                                result.Manager = UserLogic.GetByNIK(employeeGMFunct.NIKSITE);
                            }
                            
                        }

                        var VP = V_ITAS_EMP.GetByNIKSite("JRN0152");
                        if (VP != null && !String.IsNullOrEmpty(VP.NIKSITE))
                        {
                            result.VP = UserLogic.GetByNIK(VP.NIKSITE);
                        }

                        result.ManagerIT = UserLogic.GetByNIK(SiteSettings.MANAGER_IT);
                        result.VPIT = UserLogic.GetByNIK(SiteSettings.VP_IT);

                        //var itadmin = V_ITAS_EMP.GetByNIKSite(employee.GMFunc);
                        //var itadmin = V_ITADR_APPROVAL_VP.GetByNIK(user.NIK);
                        var itadmin = employeeVPFunct;
                        if (itadmin == null)
                        {
                            //result.ITAdmin = UserLogic.GetByNIK(itadmin.Manager);
                            //result.ITAdmin = UserLogic.GetByNIK(result.Manager.NIK);
                            //result.ITAdmin = UserLogic.GetByNIK(itadmin.NIKSITE);
                            //if (String.IsNullOrEmpty(result.ITAdmin.NIK))
                            //{
                                //result.ITAdmin = UserLogic.GetByNIK("JRN0115");
                                if (!String.IsNullOrEmpty(result.Manager.NIKSite))
                                {
                                    result.ITAdmin = result.Manager;
                                }
                                else
                                {
                                    result.ITAdmin = UserLogic.GetByNIK(employeeManagerFunct.NIKSITE);
                                }
                            //}
                        }
                        else
                        {
                            result.ITAdmin = UserLogic.GetByNIK(itadmin.NIKSITE);
                        }
                        
                        result.ITSPV = UserLogic.GetByNIK(SiteSettings.IT_SPV);
                    }
                }

            }

            return result;
        }

        public static UserApprovalChangeRequestModel GetUserApprovalChangeRequest(string DocumentNo)
        {
            UserApprovalChangeRequestModel model = new UserApprovalChangeRequestModel();
            ChangeRequest cr = ChangeRequest.GetByRequestNo(DocumentNo);

            if (cr != null)
            {
                if (string.IsNullOrEmpty(cr.MGR))
                {
                    var CR_APPROVAL = CurrentDataContext.CurrentContext.SP_GET_APPROVAL_CHANGEREQUEST(cr.NIKSite, cr.ModuleCode).FirstOrDefault();

                    model.Manager = UserLogic.GetByNIK(CR_APPROVAL.Manager);
                    model.GeneralManager = UserLogic.GetByNIK(CR_APPROVAL.GMSITE);
                    model.OwnerApp = UserLogic.GetByNIK(CR_APPROVAL.OWNAPP);
                    model.GMHO = UserLogic.GetByNIK(CR_APPROVAL.GMHO);
                    model.VPHO = UserLogic.GetByNIK((cr.IsChangeMasterData ? CR_APPROVAL.VPHO : string.Empty));
                    model.ITApp = UserLogic.GetByNIK(CR_APPROVAL.ITAPP);
                    model.ManagerIT = UserLogic.GetByNIK(CR_APPROVAL.MGRIT);
                    model.VPIT = UserLogic.GetByNIK(CR_APPROVAL.VPIT);
                }
                else
                {
                    model.Manager = UserLogic.GetByNIK(cr.MGR);
                    model.GeneralManager = UserLogic.GetByNIK(cr.GMSITE);
                    model.OwnerApp = UserLogic.GetByNIK(cr.OWNAPP);
                    model.GMHO = UserLogic.GetByNIK(cr.GMHO);
                    model.VPHO = UserLogic.GetByNIK((cr.IsChangeMasterData ? cr.VPHO : string.Empty));
                    model.ITApp = UserLogic.GetByNIK(cr.ITAPP);
                    model.ManagerIT = UserLogic.GetByNIK(cr.MGRIT);
                    model.VPIT = UserLogic.GetByNIK(cr.VPIT);
                }
            }

            return model;
        }


        public static ResponseModel LoginActiveDirectory(Data.Model.UserLoginModel model, bool IsSearch = false)
        {
            ResponseModel response = new ResponseModel(false);
            var username = model.Username.Split('\\');

            var user = new Data.Model.UserModel();
            DirectoryEntry _entry;

            if (IsSearch)
            {
                _entry = new DirectoryEntry(string.Format("LDAP://{0}", username[0]));
            }
            else
            {
                _entry = new DirectoryEntry(string.Format("LDAP://{0}", username[0]), username[1], model.Password);
            }


            DirectorySearcher _searcher = new DirectorySearcher(_entry);
            _searcher.Filter = string.Format("(samAccountName={0})", username[1]);

            try
            {
                SearchResult _sr = _searcher.FindOne();
                if (_sr != null)
                {
                    //string jsonUser = Newtonsoft.Json.JsonConvert.SerializeObject(_sr);
                    user.EmployeeId = _sr.Properties["employeeid"][0].ToString();
                    user.NIKSite = _sr.Properties["employeeid"][0].ToString();

                    var propertyCompany = _sr.Properties["company"];
                    if (propertyCompany != null && propertyCompany.Count > 0)
                    {
                        if (propertyCompany[0] != null && !string.IsNullOrEmpty(propertyCompany[0].ToString()))
                        {
                            user.CompanyName = propertyCompany[0].ToString();
                        }
                    }

                    user.FullName = _sr.Properties["name"][0].ToString();
                    user.Username = _sr.Properties["samaccountname"][0].ToString();
                    user.Email = _sr.Properties["mail"][0].ToString();
                    user.CompanyCode = username[0];

                    response.SetSuccess();
                    response.ResponseObject = user;
                }
            }
            catch (Exception ex)
            {
                response.SetError(ex.Message);
            }

            return response;
        }

    }
}
