using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace ESS.Common
{
    /// <summary>
    /// Common All Function
    /// </summary>
    public class CommonFunction
    {
        public static DateTime DateTimeESS(string datestring)
        {
            DateTime dtConvert = DateTime.MinValue;

            if (!string.IsNullOrEmpty(datestring))
            {
                DateTime.TryParseExact(datestring, Constant.FORMAT_DATE_JRESOURCES, null, System.Globalization.DateTimeStyles.None, out dtConvert);
            }

            return dtConvert;
        }

        public static string AddSpacesToSentence(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
                return "";
            StringBuilder newText = new StringBuilder(text.Length * 2);
            newText.Append(text[0]);
            for (int i = 1; i < text.Length; i++)
            {
                if (char.IsUpper(text[i]) && text[i - 1] != ' ')
                    newText.Append(' ');
                newText.Append(text[i]);
            }
            return newText.ToString();
        }

        public static string StatusWorkFlowText(string status)
        {
            string statusLabel = String.Empty;

            switch (status)
            {
                case REQUEST_STATUS.NEW:
                    statusLabel = "NEW";
                    break;

                case REQUEST_STATUS.INITIAL:
                    statusLabel = "INITIAL";
                    break;

                case REQUEST_STATUS.IN_PROGRESS:
                    statusLabel = "IN PROGRESS";
                    break;

                case REQUEST_STATUS.COMPLETE:
                    statusLabel = "COMPLETE";
                    break;

                case REQUEST_STATUS.REVISION:
                    statusLabel = "REVISION";
                    break;
            }

            return statusLabel;
        }

        public static string StatusWorkFlowLabel(string status)
        {
            string statusLabel = StatusWorkFlowText(status);
            string statusBadges = String.Empty;

            switch (status)
            {
                case REQUEST_STATUS.NEW:
                    statusBadges = "badge-light";
                    break;

                case REQUEST_STATUS.INITIAL:
                    statusBadges = "badge-primary-inverted";
                    break;

                case REQUEST_STATUS.IN_PROGRESS:
                    statusBadges = "badge-warning-inverted";
                    break;

                case REQUEST_STATUS.COMPLETE:
                    statusBadges = "badge-success-inverted";
                    break;

                case REQUEST_STATUS.REVISION:
                    statusBadges = "badge-warning";
                    break;
            }

            return String.Format("<span class=\"badge {0}\">{1}</span>", statusBadges, statusLabel);
        }

    }
}
