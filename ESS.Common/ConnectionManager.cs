using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ESS.Common
{
    public static class DatabaseGroupName
    {
        public static string MAIN_DATABASE = "ESSEntities";
        public static string MSR_DATABASE = "MSR";
        public static string ACCPAC_DATABASE = "ACCPAC";
    }

    public class ConnectionModel
    {
        public string DatabaseGroupName { get; set; }
        public string ConnectionName { get; set; }
        public string CompanyCode { get; set; }
        public string NameSite { get; set; }
        public string ConnectionString { get; set; }
    }

    public class ConnectionManager
    {
        public static List<ConnectionModel> GetListConnectionString()
        {
            List<ConnectionModel> list = new List<ConnectionModel>();
            foreach (ConnectionStringSettings c in System.Configuration.ConfigurationManager.ConnectionStrings)
            {
                if (!string.IsNullOrEmpty(c.Name) 
                    && c.Name != "LocalSqlServer" 
                    && !c.Name.ToLower().Contains("local"))
                {
                    var item = new ConnectionModel();
                    item.ConnectionString = c.ConnectionString;
                    item.ConnectionName = c.Name;
                    item.NameSite = c.Name;
                    item.CompanyCode = c.Name;

                    if (c.Name.Contains("-"))
                    {
                        var splitString = c.Name.Split('-');
                        if (splitString[0] != null)
                        {
                            item.DatabaseGroupName = splitString[0];
                        }

                        if (splitString[1] != null)
                        {
                            item.CompanyCode = splitString[1];
                        }
                    }
                    else
                    {
                        item.DatabaseGroupName = c.Name;
                    }
                    
                    list.Add(item);
                }
            }


            return list;
        }

        public static string MainConnection
        {
            get
            {
                var listConnection = GetListConnectionString();
                var mainConnection = listConnection.FirstOrDefault(x => x.DatabaseGroupName == DatabaseGroupName.MAIN_DATABASE);

                return mainConnection.ConnectionString;
            }
        }

        public static void SetConnection(string databaseGroupName, string companyCode)
        {
            var listConnection = GetListConnectionString();
            string ConnectionName = listConnection.FirstOrDefault(x => x.CompanyCode == companyCode).ConnectionName;
            CookieManager.Set(string.Format("{0}{1}", Constant.SITE_CONNECTION, databaseGroupName), ConnectionName, true);
        }

        public static string GetConnection(string databaseGroupName)
        {
            var connectionName = CookieManager.Get(string.Format("{0}{1}", Constant.SITE_CONNECTION, databaseGroupName));
            var stringConnection = string.Empty;
            
            if (System.Configuration.ConfigurationManager.ConnectionStrings[connectionName] != null)
            {
                stringConnection = System.Configuration.ConfigurationManager.ConnectionStrings[connectionName].ConnectionString;
            }

            return stringConnection;
        }

        public static string GetConnectionStringByCompanyCode(string databaseGroupName, string companyCode)
        {
            var listConnection = GetListConnectionString();
            string ConnectionName = listConnection.FirstOrDefault().ConnectionName;
            var connectionCode = listConnection.FirstOrDefault(x => x.DatabaseGroupName == databaseGroupName &&
                x.CompanyCode == companyCode);

            if (connectionCode != null)
            {
                ConnectionName = connectionCode.ConnectionName;
                var stringConnection = string.Empty;
                if (System.Configuration.ConfigurationManager.ConnectionStrings[ConnectionName] != null)
                {
                    var connectString = System.Configuration.ConfigurationManager.ConnectionStrings[ConnectionName].ConnectionString;

                    return connectString;
                }
            }

            return null;
        }
    }
}

