using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace ESS.Common
{
    public class CONNECTION_STRING
    {
        public static string MSR_CONNECTION = "MSRConnection";
        public static string ACCPAC_CONNECTION = "ACCPACConnection";
        public static string ESS_CONNECTION = "ESSConnection";
    }

    public class Constant
    {
        public static string VERSION_APP = "Version.2.0.1.1";
        public static string FORMAT_DATE_JRESOURCES = "dd/MM/yyyy";
        public static string FORMAT_DATETIME_JRESOURCES = "dd/MM/yyyy hh:mm";
        public static string FORMAT_TIME_JRESOURCES = "hh:mm";
        public static string FORMAT_DATE_JRESOURCES_PICKER = "dd/mm/yyyy";
        public static string FORMAT_DATE_JRESOURCES_TEXT = "d MMMM yyyy";
        public static string FORMAT_DATE_TRANSDATE_ACCPAC = "yyyyMMdd";

        public static string SITE_CONNECTION = "SITE_CONNECTION_";

        public static string ROLE_DEFAULT = "USERS";
        public static string CREATED_SYSTEM = "SYSTEM";
        public static int PAGE_SIZE_DEFAULT = 20;
    }

    public class COOKIES_NAME
    {
        public const string USER_MODEL = "USER";
        public const string USER_NIK = "NIK";
        public const string USER_NIK_ENC = "NIK_ENC";
        public const string USER_NIKSITE = "NIKSITE";
        public const string FULL_NAME = "FULLNAME";
        public const string USER_NAME = "USERNAME";
        public const string USER_COMPANY_CODE = "COMPANY_CODE";
        public const string USER_SITE_CODE = "USER_SITE_CODE";
        public const string USER_COMPANY_NAME = "COMPANY_NAME";
        public const string USER_DEPARTMENT_CODE = "USER_DEPARTMENT_CODE";
        public const string ROLE = "ROLE";
        public const string URL_CODE = "URL_CODE";
        public const string URL_DOCUMENTNO = "URL_DOCUMENTNO";
        public const string USER_INFO = "USER_INFO";
    }

    public enum MenuCode
    {
        None = 0,
        GenerateDocument = 1,
        Report = 2,
        Setting = 3,
        UserList = 4,
        RoleSetting = 5,
    }

    public class PSTType
    {
        public const string SCHEDULE = "SCHEDULE";
        public const string TRANSACTED = "TRANSACTED";
        public const string RANDOM = "RANDOM";

        public List<string> GetList()
        {
            var list = new List<String>();
            list.Add(SCHEDULE);
            list.Add(TRANSACTED);
            list.Add(RANDOM);

            return list;
        }

        public static List<SelectListItem> GetSelectList()
        {
            var list = new List<SelectListItem>();
            list.Add(new SelectListItem() { Text = "", Value = "" });
            list.Add(new SelectListItem() { Text = SCHEDULE, Value = SCHEDULE });
            list.Add(new SelectListItem() { Text = TRANSACTED, Value = TRANSACTED });
            list.Add(new SelectListItem() { Text = RANDOM, Value = RANDOM });

            return list;
        }
    }

    public enum ReportType
    {
        None = 0,
        Report1 = 1,
    }

    public static class ConsoleMenuCode
    {
        public static string BuilderFirst = "00";
        public static string WorkFlowCode = "01";
    }

    public static class URI_ACCPAC
    {
        public static string SHIPMENT_SAVE = "Shipment/Save";
        public static string SHIPMENT_POST = "Shipment/PostShipment";

        public static string TRANSFER_SAVE = "Transfer/Save";
        public static string TRANSFER_POST = "Transfer/PostTransfers";

        public static string VMDOCUMENT_VIEW = "VMDocument/View";
        public static string VMDOCUMENT_SAVE = "VMDocument/Save";
        public static string VMDOCUMENT_POST = "VMDocument/Post";

        public static string PURCHASING_SAVE_REQUISITION = "Purchasing/SaveRequisition";
        public static string PURCHASING_SAVE_WORKFLOW = "Purchasing/TriggerWorkFlowRequisition";
        public static string PURCHASING_UPDATE_REQUISITION = "Purchasing/UpdateRequisition";

        public static string PROCESS_SAVE = "Process/Save";
    }
}

