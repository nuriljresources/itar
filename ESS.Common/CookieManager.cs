using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace ESS.Common
{
    public class CookieManager
    {
        public const string COOKIE_PREFIX = "ESS_";

        /// <summary>
        /// Sets the value
        /// </summary>
        /// <param name="key">the key</param>
        /// <param name="value"></param>
        /// <param name="nonPersistent"></param>
        public static void Set(string key, string value, bool Persistent)
        {
            value = DataEncription.Encrypt(value);

            HttpCookie Cookie = new HttpCookie(COOKIE_PREFIX + key, value);

            if (Persistent)
            {
                var userAgent = !string.IsNullOrEmpty(HttpContext.Current.Request.UserAgent) ? HttpContext.Current.Request.UserAgent.ToLower() : string.Empty;
                Cookie.Expires = DateTime.Now.AddDays(1);
            }

            var check = HttpContext.Current.Response.Cookies[COOKIE_PREFIX + key];
            if (check == null)
            {
                HttpContext.Current.Response.Cookies.Add(Cookie);
            }
            else
            {
                HttpContext.Current.Response.Cookies.Set(Cookie);
            }
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public static string Get(string key)
        {
            if (Contains(key))
            {
                string value = "";
                int total = HttpContext.Current.Request.Cookies.Count;
                for (int i = 0; i < total; i++)
                {
                    var cookiees = HttpContext.Current.Request.Cookies[i];
                    if (cookiees.Name == COOKIE_PREFIX + key)
                    {
                        value = cookiees.Value;
                    }

                }

                if (!string.IsNullOrEmpty(value))
                {
                    return DataEncription.Decrypt(value);
                }

            }


            return string.Empty;

        }

        /// <summary>
        /// Determines whether [contains] [the specified key].
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>
        /// 	<c>true</c> if [contains] [the specified key]; otherwise, <c>false</c>.
        /// </returns>
        public static bool Contains(string key)
        {
            return HttpContext.Current.Request.Cookies[COOKIE_PREFIX + key] != null ? true : false;
        }

        /// <summary>
        /// Deletes the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        public static void Delete(string key)
        {
            if (HttpContext.Current.Request.Cookies[COOKIE_PREFIX + key] != null)
            {
                var cookie = new HttpCookie(COOKIE_PREFIX + key) { Expires = DateTime.Now.AddDays(-1d) };
                HttpContext.Current.Response.Cookies.Set(cookie);
            }
        }

        public static void DeleteAll()
        {
            var keys = HttpContext.Current.Request.Cookies.AllKeys;
            var keys2 = HttpContext.Current.Response.Cookies.AllKeys;

            foreach (var key in keys)
            {
                if (HttpContext.Current.Request.Cookies[key.ToString()] != null)
                {
                    var cookie = new HttpCookie(key.ToString()) { Expires = DateTime.Now.AddDays(-1d) };
                    HttpContext.Current.Response.Cookies.Set(cookie);
                }
            }
        }
    }
}
