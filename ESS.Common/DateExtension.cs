using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ESS.Common
{
    /// <summary>
    /// DateExtension For Helper
    /// </summary>
    public static class DateExtension
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static string ESSDate(this DateTime? date)
        {
            if (date.HasValue)
                return date.Value.ToString(Constant.FORMAT_DATE_JRESOURCES_PICKER);

            return string.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static string ESSDate(this DateTime date)
        {
            return date.ToString(Constant.FORMAT_DATE_JRESOURCES);
        }

        public static string ESSDateTime(this DateTime? date)
        {
            if (date.HasValue)
                return date.Value.ToString(Constant.FORMAT_DATETIME_JRESOURCES);

            return string.Empty;
        }

        public static string ESSTime(this DateTime? date)
        {
            if (date.HasValue)
                return date.Value.ToString(Constant.FORMAT_TIME_JRESOURCES);

            return string.Empty;
        }

        public static string ESSDateTime(this DateTime date)
        {
            return date.ToString(Constant.FORMAT_DATETIME_JRESOURCES);
        }

        public static string ESSTime(this DateTime date)
        {
            return date.ToString(Constant.FORMAT_TIME_JRESOURCES);
        }
    }
}
