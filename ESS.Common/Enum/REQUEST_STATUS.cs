﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ESS.Common
{
    public class REQUEST_STATUS
    {
        public const string NEW = "";
        public const string INITIAL = "INT";
        public const string IN_PROGRESS = "INP";
        public const string COMPLETE = "COMPLT";
        public const string REVISION = "RVS";
    }
}
