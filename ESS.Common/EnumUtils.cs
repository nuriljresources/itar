using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Web.Mvc;

namespace ESS.Common
{
    /// <span class="code-SummaryComment"><summary></span>
    /// enum utilities. 
    /// - converts from a [Description(&quot;&quot;)] to an enum value
    /// - grabs the [Description(&quot;&quot;)] from an enum value
    /// 
    /// <span class="code-SummaryComment"></summary></span>
    public class EnumUtils<T>
    {
        public static string GetDescription(T enumValue, string defDesc)
        {
            FieldInfo fi = enumValue.GetType().GetField(enumValue.ToString());

            if (null != fi)
            {
                object[] attrs = fi.GetCustomAttributes
                        (typeof(DescriptionAttribute), true);
                if (attrs != null && attrs.Length > 0)
                    return ((DescriptionAttribute)attrs[0]).Description;
            }

            return defDesc;
        }

        public static string GetDescription(T enumValue)
        {
            return GetDescription(enumValue, string.Empty);
        }

        public static T FromDescription(string description)
        {
            Type t = typeof(T);
            foreach (FieldInfo fi in t.GetFields())
            {
                object[] attrs = fi.GetCustomAttributes
                        (typeof(DescriptionAttribute), true);
                if (attrs != null && attrs.Length > 0)
                {
                    foreach (DescriptionAttribute attr in attrs)
                    {
                        if (attr.Description.Equals(description))
                            return (T)fi.GetValue(null);
                    }
                }
            }
            return default(T);
        }

        public static List<SelectListItem> GetDropDownList(string valueSelected = "")
        {
            var list = new List<SelectListItem>();

            int enumIndex = 0;
            Type t = typeof(T);
            foreach (FieldInfo fi in t.GetFields())
            {
                object[] attrs = fi.GetCustomAttributes
                        (typeof(DescriptionAttribute), true);
                if (attrs != null && attrs.Length > 0)
                {
                    foreach (DescriptionAttribute attr in attrs)
                    {
                        var item = new SelectListItem();
                        item.Text = attr.Description;
                        item.Value = ((int)System.Enum.GetValues(t).GetValue(enumIndex)).ToString();

                        if (!string.IsNullOrEmpty(valueSelected))
                        {
                            if (item.Value == valueSelected)
                            {
                                item.Selected = true;
                            }
                        }

                        list.Add(item);
                    }

                    enumIndex++;
                }
            }


            return list;
        }
    }

}
