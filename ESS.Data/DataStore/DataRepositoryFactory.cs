using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Objects;
using System.Linq;
using ESS.Data;
//using ESS.Data.GenericRepository;
using System.Data.EntityClient;
using ESS.Common;
using System.Data.SqlClient;

namespace ESS.Data
{
    public static class CurrentDataContext
    {
        public static DBEntities CurrentContext
        {
            get
            {
                var repository = DataRepositoryStore.CurrentDataStore[DataRepositoryStore.KEY_DATACONTEXT] as DBEntities;//IRepository;
                if (repository == null)
                {
                    repository = new DBEntities();
                    DataRepositoryStore.CurrentDataStore[DataRepositoryStore.KEY_DATACONTEXT] = repository;
                }

                return repository;

            }
        }

        public static void CloseCurrentRepository()
        {
            var repository = DataRepositoryStore.CurrentDataStore[DataRepositoryStore.KEY_DATACONTEXT] as DBEntities;//IRepository;
            if (repository != null)
            {
                repository.Dispose();
                DataRepositoryStore.CurrentDataStore[DataRepositoryStore.KEY_DATACONTEXT] = null;
            }
        }
    }

    //public static class ITSDataContext
    //{
    //    public static ITSEntities CurrentContext
    //    {
    //        get
    //        {
    //            var repository = DataRepositoryITS.CurrentDataStore[DataRepositoryITS.KEY_DATACONTEXT] as ITSEntities;//IRepository;
    //            if (repository == null)
    //            {
    //                repository = new ITSEntities();
    //                DataRepositoryITS.CurrentDataStore[DataRepositoryITS.KEY_DATACONTEXT] = repository;
    //            }

    //            return repository;
    //        }
    //    }

    //    public static void CloseCurrentRepository()
    //    {
    //        var repository = DataRepositoryITS.CurrentDataStore[DataRepositoryITS.KEY_DATACONTEXT] as ITSEntities;//IRepository;
    //        if (repository != null)
    //        {
    //            repository.Dispose();
    //            DataRepositoryITS.CurrentDataStore[DataRepositoryITS.KEY_DATACONTEXT] = null;
    //        }
    //    }
    //}
}