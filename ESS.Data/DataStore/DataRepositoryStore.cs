using System;


namespace ESS.Data
{
    public class DataRepositoryStore
    {
        public static readonly string KEY_DATACONTEXT = "DataRepository";
        public static IDataRepositoryStore CurrentDataStore;
    }

    public class DataRepositoryITS
    {
        public static readonly string KEY_DATACONTEXT = "DataRepositoryITS";
        public static IDataRepositoryITS CurrentDataStore;
    }
}
