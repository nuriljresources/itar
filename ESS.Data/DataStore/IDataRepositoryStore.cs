using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ESS.Data
{
    public interface IDataRepositoryStore
    {
        object this[string key] { get; set; }
    }

    public interface IDataRepositoryITS
    {
        object this[string key] { get; set; }
    }
}
