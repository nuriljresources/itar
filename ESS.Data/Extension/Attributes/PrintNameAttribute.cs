using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ESS.Extension.Attributes
{
    [System.AttributeUsage(System.AttributeTargets.Method | System.AttributeTargets.Property)]
    public class PrintNameAttribute : System.Attribute
    {
        public string Name { get; set; }

        public PrintNameAttribute(string name)
        {
            this.Name = name;
        }
    }
}
