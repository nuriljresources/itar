using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace ESS.Data.Extension
{
    public static class ObjectExtensions
    {
        /// <summary>
        /// Convert object to Byte array.
        /// </summary>
        /// <param name="objectClass">The object Class.</param>
        /// <returns></returns>
        public static byte[] GetByteArrayFromObject(this object objectClass)
        {
            var memoryStream = new MemoryStream();
            var binaryformatter = new BinaryFormatter();
            binaryformatter.Serialize(memoryStream, objectClass);
            return memoryStream.ToArray();
        }

        /// <summary>
        /// Gets the byte array and converts back to an object.
        /// </summary>
        /// <param name="objectClass">The object class.</param>
        /// <param name="objectBytes">The object bytes.</param>
        /// <returns></returns>
        public static TSource DeserializeByteArrayToObject<TSource>(this object objectClass, byte[] objectBytes)
        {
            var memoryStream = new MemoryStream(objectBytes);
            var binaryformatter = new BinaryFormatter();
            return (TSource)binaryformatter.Deserialize(memoryStream);
        }
    }
}
