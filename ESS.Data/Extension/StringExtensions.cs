using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ESS.Data.Extension
{
    public static class StringExtensions
    {
        /// <summary>
        /// Equalses the ingore case.
        /// </summary>
        /// <param name="str">The STR.</param>
        /// <param name="strValue">The STR value.</param>
        /// <returns></returns>
        public static bool EqualsIgnoreCase(this string str, string strValue)
        {
            if (!string.IsNullOrWhiteSpace(strValue))
            {
                return str.Equals(strValue, StringComparison.OrdinalIgnoreCase);
            }
            return false;
        }
    }
}
