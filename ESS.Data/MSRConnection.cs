﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

using ESS.Common;

namespace ESS.Data
{
    public class MSRConnection
    {
        public MSRConnection(string _nIKSiteFrom, string _nIKSiteTo, DateTime _dateFrom, DateTime _dateTo)
        {
            NIKSiteFrom = _nIKSiteFrom;
            NIKSiteTo = _nIKSiteTo;
            DateFrom = _dateFrom;
            DateTo = _dateTo;

            //adoHelper = new AdoHelper();
            //AdoHelper.ConnectionString = ConfigurationManager.ConnectionStrings[CONNECTION_STRING.MSR_CONNECTION].ConnectionString;
        }

        public string NIKSiteFrom { get; set; }
        public string NIKSiteTo { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public ADOHelper adoHelper { get; set; }

        public void InsertDelegation()
        {
            string qry = "";
            adoHelper.ExecNonQueryProc(qry, NIKSiteFrom, NIKSiteTo, DateFrom, DateTo);
        }

        public void Commit()
        {
            adoHelper.Commit();
        }


    }
}
