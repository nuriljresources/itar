﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ESS.Data
{
    public partial class ApplicationList
    {
        public static IQueryable<ApplicationList> GetAll()
        {
            return CurrentDataContext.CurrentContext.ApplicationLists.OrderByDescending(x => x.ApplicationCode);
        }

        public static List<ApplicationList> GetAllApplication()
        {
            var apps = CurrentDataContext.CurrentContext.ApplicationLists.OrderByDescending(x => x.ApplicationCode).ToList();

            apps = apps
                .GroupBy(p => p.ApplicationCode)
                .Select(g => g.First())
                .ToList();

            return apps;
        }

        public static IQueryable<ApplicationList> GetModuleByApplicationCode(string _applicationCode)
        {
            return CurrentDataContext.CurrentContext.ApplicationLists.Where(x => x.ApplicationCode == _applicationCode)
                .OrderByDescending(x => x.ModuleCode);
        }

        public static IQueryable<ApplicationList> GetListModuleByModuleCode(string _moduleCode)
        {
            return CurrentDataContext.CurrentContext.ApplicationLists.Where(x => x.ModuleCode == _moduleCode)
                .OrderByDescending(x => x.ModuleCode);
        }

        public static ApplicationList GetModuleByModuleCode(string _moduleCode)
        {
            return CurrentDataContext.CurrentContext.ApplicationLists.FirstOrDefault(x => x.ModuleCode == _moduleCode);
        }

    }
}
