﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ESS.Data
{
    public partial class ChangeRequest
    {
        public static IQueryable<ChangeRequest> GetAll()
        {
            return CurrentDataContext.CurrentContext.ChangeRequests.Where(x => !x.IsDeleted).OrderByDescending(x => x.CreatedDate);
        }

        public static ChangeRequest GetByID(Guid Id)
        {
            return CurrentDataContext.CurrentContext.ChangeRequests.FirstOrDefault(x => !x.IsDeleted && x.ID == Id);
        }

        public static ChangeRequest GetByRequestNo(string _requestNo)
        {
            return CurrentDataContext.CurrentContext.ChangeRequests.FirstOrDefault(x => !x.IsDeleted && x.ChangeRequestNo == _requestNo);
        }
    }
}
