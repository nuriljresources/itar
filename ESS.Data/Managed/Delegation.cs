﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ESS.Data
{
    public partial class Delegation
    {
        public static IQueryable<Delegation> GetAll()
        {
            return CurrentDataContext.CurrentContext.Delegations.OrderByDescending(x => x.CreatedDate);
        }

        public static Delegation GetByDelegationNo(string DelegationNo)
        {
            return CurrentDataContext.CurrentContext.Delegations.FirstOrDefault(x => x.DelegationNo == DelegationNo);
        }

        public static Delegation GetByDelegationId(Guid DelegationId)
        {
            return CurrentDataContext.CurrentContext.Delegations.FirstOrDefault(x => x.ID == DelegationId);
        }

    }
}
