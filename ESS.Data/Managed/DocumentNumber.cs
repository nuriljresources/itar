﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ESS.Data
{
    public partial class DocumentNumber
    {
        public static IQueryable<DocumentNumber> GetAll()
        {
            return CurrentDataContext.CurrentContext.DocumentNumbers.OrderByDescending(x => x.CreatedDate);
        }

        public static DocumentNumber GetByDocumentType(string _docType, string _companyCode)
        {
            int year = DateTime.Now.Year;
            return CurrentDataContext.CurrentContext.DocumentNumbers.FirstOrDefault(x => x.DocumentType == _docType && x.CompanyCode == _companyCode && x.Year == year);
        }
    }
}
