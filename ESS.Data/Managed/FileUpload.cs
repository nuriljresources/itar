﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ESS.Data
{
    public partial class FileUpload
    {
        public static IQueryable<FileUpload> GetAll()
        {
            return CurrentDataContext.CurrentContext.FileUploads.OrderByDescending(x => x.UploadDate);
        }

        public static FileUpload GetById(Guid fileId)
        {
            return CurrentDataContext.CurrentContext.FileUploads.FirstOrDefault(x => x.ID == fileId);
        }

        public static IQueryable<FileUpload> GetByDocumentNo(String documentNo)
        {
            return CurrentDataContext.CurrentContext.FileUploads.Where(x => x.DocumentNo == documentNo).OrderByDescending(x => x.UploadDate);
        }
    }
}
