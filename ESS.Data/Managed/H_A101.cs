﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ESS.Data
{
    public partial class H_A101
    {
        public static IQueryable<H_A101> GetAll()
        {
            return CurrentDataContext.CurrentContext.H_A101.Where(x => x.Active =="1").OrderByDescending(x => x.NIKSITE);
        }

        public static IQueryable<H_A101> GetAllByCompany(string KdSite)
        {
            return CurrentDataContext.CurrentContext.H_A101.Where(x => x.Active == "1" && x.KdSite == KdSite).OrderByDescending(x => x.NIKSITE);
        }

        public static H_A101 GetByNIK(string NIK)
        {
            return CurrentDataContext.CurrentContext.H_A101.FirstOrDefault(x => x. NIKSITE == NIK && x.Active == "1");
        }
    }
}
