﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ESS.Data
{
    public partial class H_A120
    {

        public static IQueryable<H_A120> GetAll()
        {
            return CurrentDataContext.CurrentContext.H_A120.OrderByDescending(x => x.KdCompany);
        }

        public static H_A120 GetByKdCompany(string KdSite)
        {
            return CurrentDataContext.CurrentContext.H_A120.FirstOrDefault(x => x.KdSite == KdSite);
        }

    }
}
