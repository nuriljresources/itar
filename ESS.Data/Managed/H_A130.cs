﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ESS.Data
{
    public partial class H_A130
    {

        public static IQueryable<H_A130> GetAll()
        {
            return CurrentDataContext.CurrentContext.H_A130.OrderByDescending(x => x._KdDepar);
        }

        public static H_A130 GetByKdDepar(string KdDepar)
        {
            return CurrentDataContext.CurrentContext.H_A130.FirstOrDefault(x => x.KdDepar == KdDepar);
        }

        public static IQueryable<H_A130> GetBySite(string kdsite)
        {
            return CurrentDataContext.CurrentContext.H_A130.Where(x => x.kdsite == kdsite);
        }

    }
}
