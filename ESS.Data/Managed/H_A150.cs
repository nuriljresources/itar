﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ESS.Data
{
    public partial class H_A150
    {

        public static IQueryable<H_A150> GetAll()
        {
            return CurrentDataContext.CurrentContext.H_A150.OrderByDescending(x => x._KdDepar);
        }

        public static H_A150 GetByKdJabat(string KdJabat)
        {
            return CurrentDataContext.CurrentContext.H_A150.FirstOrDefault(x => x.KdJabat == KdJabat);
        }

    }
}
