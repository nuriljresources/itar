﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ESS.Data
{
    public partial class RequestAccess
    {
        public static IQueryable<RequestAccess> GetAll()
        {
            return CurrentDataContext.CurrentContext.RequestAccesses.Where(x => !x.IsDeleted).OrderByDescending(x => x.CreatedDate);
        }

        public static RequestAccess GetByID(Guid Id)
        {
            return CurrentDataContext.CurrentContext.RequestAccesses.FirstOrDefault(x => !x.IsDeleted && x.ID == Id);
        }

        public static RequestAccess GetByRequestNo(string _requestNo)
        {
            return CurrentDataContext.CurrentContext.RequestAccesses.FirstOrDefault(x => !x.IsDeleted && x.RequestAccessNo == _requestNo);
        }
    }
}
