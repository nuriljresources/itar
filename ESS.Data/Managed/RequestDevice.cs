﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ESS.Data
{
    public partial class RequestDevice
    {
        public static IQueryable<RequestDevice> GetAll()
        {
            return CurrentDataContext.CurrentContext.RequestDevices.Where(x => !x.IsDeleted).OrderByDescending(x => x.CreatedDate);
        }

        public static RequestDevice GetByID(Guid Id)
        {
            return CurrentDataContext.CurrentContext.RequestDevices.FirstOrDefault(x => !x.IsDeleted && x.ID == Id);
        }

        public static RequestDevice GetByRequestNo(string _requestNo)
        {
            return CurrentDataContext.CurrentContext.RequestDevices.FirstOrDefault(x => !x.IsDeleted && x.RequestDeviceNo == _requestNo);
        }
    }
}
