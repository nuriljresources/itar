﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ESS.Data
{
    public partial class UserAccess
    {
        public static UserAccess GetByNIKSite(string NIKSite)
        {
            return CurrentDataContext.CurrentContext.UserAccesses.FirstOrDefault(x => x.NIKSite == NIKSite);
        }
    }
}
