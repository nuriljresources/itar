﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ESS.Data
{
    public partial class V_ITADR_APPROVAL_VP
    {
        public static IQueryable<V_ITADR_APPROVAL_VP> GetAll()
        {
            return CurrentDataContext.CurrentContext.V_ITADR_APPROVAL_VP.OrderByDescending(x => x.nama);
        }

        public static IQueryable<V_ITADR_APPROVAL_VP> SearchByName(string name)
        {
            return CurrentDataContext.CurrentContext.V_ITADR_APPROVAL_VP.OrderByDescending(x => x.nama);
        }

        public static V_ITADR_APPROVAL_VP GetByNIK(string nik)
        {
            return CurrentDataContext.CurrentContext.V_ITADR_APPROVAL_VP.FirstOrDefault(x => x.nik == nik);
        }
    }
}
