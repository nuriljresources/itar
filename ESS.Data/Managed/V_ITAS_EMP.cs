﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ESS.Data
{
    public partial class V_ITAS_EMP
    {
        public static IQueryable<V_ITAS_EMP> GetAll()
        {
            return CurrentDataContext.CurrentContext.V_ITAS_EMP.Where(x => x.Active == "1").OrderByDescending(x => x.Nama);
        }

        public static IQueryable<V_ITAS_EMP> SearchByName(string name)
        {
            return CurrentDataContext.CurrentContext.V_ITAS_EMP.Where(x => x.Active == "1" && (x.Nama == "name" || x.Nama.Contains(name) || x.NIKSITE.Contains(name))).OrderBy(x => x.Nama);
        }

        public static V_ITAS_EMP GetByNIKSite(string nikSite)
        {
            return CurrentDataContext.CurrentContext.V_ITAS_EMP.FirstOrDefault(x => x.Active == "1" && x.NIKSITE == nikSite);
        }

        public static V_ITAS_EMP GetByNiksiteFunctDiv(string kdfunct, string kddivisi, string kdlevel)
        {
            return CurrentDataContext.CurrentContext.V_ITAS_EMP.FirstOrDefault(x => x.Active == "1" && x.kdFunct == kdfunct && x.kdDivisi == kddivisi && x.KdLevel.Contains(kdlevel));
        }

        public static IQueryable<V_ITAS_EMP> SearchByNameDC(string name)
        {
            return CurrentDataContext.CurrentContext.V_ITAS_EMP.Where(x => x.Active == "1" && x.nmLevel != "Non Staff" && (x.Nama == "name" || x.Nama.Contains(name) || x.NIKSITE.Contains(name))).OrderBy(x => x.Nama);
        }
    }
}
