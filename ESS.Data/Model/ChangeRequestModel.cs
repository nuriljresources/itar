﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESS.Common;

namespace ESS.Data.Model
{
    public class ChangeRequestModel
    {
        public ChangeRequestModel()
        {
            StatusWF = REQUEST_STATUS.NEW;
            CreatedDateStr = DateTime.Now.ToString(Constant.FORMAT_DATE_JRESOURCES);
            CreatedBy = new UserModel();
            LogHistories = new List<LogHistoryModel>();
            FileAttachments = new List<FileUploadModel>();

            UserApproval = new UserApprovalChangeRequestModel();
        }

        public Guid ID { get; set; }
        public string ChangeRequestNo { get; set; }
        public string ApplicationCode { get; set; }
        public string ApplicationName { get; set; }
        public string ModuleCode { get; set; }
        public string ModuleName { get; set; }
        public string ProgramManager { get; set; }
        public UserModel RequestUser { get; set; }
        public DateTime DateIssued { get; set; }
        public string DateIssuedStr { get; set; }        

        public DateTime? DueDate { get; set; }
        public string DueDateStr { get; set; }

        public string Reason { get; set; }
        public bool IsChangeMasterData { get; set; }
        public bool IsNewReport { get; set; }
        public bool IsChangeUpdateReport { get; set; }
        public bool IsNewInterfaceProgram { get; set; }
        public bool IsAuthorisation { get; set; }
        public bool IsApplication { get; set; }
        public string DescriptionChange { get; set; }

        public string DevelopmentDescription { get; set; }
        public string LevelDevelopment { get; set; }

        public DateTime? DateStart { get; set; }
        public string DateStartStr { get; set; }

        public DateTime? DateFinishPlanning { get; set; }
        public string DateFinishPlanningStr { get; set; }       
        
        
        public string StatusWF { get; set; }
        public string kdsite { get; set; }

        public UserModel CreatedBy { get; set; }
        public UserModel UpdatedBy { get; set; }

        public UserModel UserLastAction { get; set; }
        
        public DateTime CreatedDate { get; set; }
        public string CreatedDateStr { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedDateStr { get; set; }

        public DateTime DateFinish { get; set; }
        public string DateFinishStr { get; set; }

        public bool IsFinish { get; set; }

        public List<LogHistoryModel> LogHistories { get; set; }
        public List<FileUploadModel> FileAttachments { get; set; }

        public UserApprovalChangeRequestModel UserApproval { get; set; }
    }

}
