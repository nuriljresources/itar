﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ESS.Data.Model
{
    public class ChangeRequestSearchModel : SearchModel
    {
        public ChangeRequestSearchModel()
        {
            ListData = new List<ChangeRequestModel>();
        }
        public string ChangeRequestNo { get; set; }
        public string Notes { get; set; }
        public string FullName { get; set; }
        public string NIKSite { get; set; }
        public string Site { get; set; }
        public string Status { get; set; }

        public List<ChangeRequestModel> ListData { get; set; }
    }

}
