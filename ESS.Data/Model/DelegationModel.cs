﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ESS.Data.Model
{
    public class DelegationModel
    {
        public DelegationModel()
        {
            ListDelegationTo = new List<UserModel>();
            ListDelegationCC = new List<UserModel>();
            DelegationDetails = new List<DelegationDetailModel>();
        }

        public Guid ID { get; set; }
        public UserModel UserCreated { get; set; }
        public string DelegationNo { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
        public string DepartmentCode { get; set; }
        public string DepartmentName { get; set; }
        public string DelegationTo { get; set; }
        public string DelegationCC { get; set; }

        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public string DateFromStr { get; set; }
        public string DateToStr { get; set; }

        public string Reason { get; set; }
        public string Task { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedDateStr { get; set; }

        public List<UserModel> ListDelegationTo { get; set; }
        public List<UserModel> ListDelegationCC { get; set; }
        public List<DelegationDetailModel> DelegationDetails { get; set; }
    }

    public class DelegationDetailModel
    {
        public DelegationDetailModel()
        {
            UserDelegation = new UserModel();
        }

        public Guid ID { get; set; }
        public string ApplicationName { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
        public string DepartmentCode { get; set; }
        public string DepartmentName { get; set; }
        public string NIKSite { get; set; }
        public string Fullname { get; set; }
        public string Position { get; set; }
        public string Task { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public string DateFromStr { get; set; }
        public string DateToStr { get; set; }

        public UserModel UserDelegation { get; set; }
    }
}
