﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ESS.Data.Model
{
    public class DelegationSearchModel : SearchModel
    {
        public DelegationSearchModel()
        {
            ListData = new List<DelegationModel>();
        }

        public List<DelegationModel> ListData { get; set; }
    }
}
