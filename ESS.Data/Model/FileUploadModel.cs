﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ESS.Data.Model
{
    public class FileUploadModel
    {
        public Guid ID { get; set; }
        public String DocumentNo { get; set; }
        public String PathFile { get; set; }
        public String FileName { get; set; }
        public String LinkDownloadFile { get; set; }
        public String FullPathFile
        {
            get
            {
                if (!String.IsNullOrEmpty(this.PathFile) && !String.IsNullOrEmpty(this.PathFile))
                {
                    return Path.Combine(this.PathFile, this.FileName);
                }
                else
                {
                    return String.Empty;
                }
            }
        }

        public String UploadDate { get; set; }
        public String UploadBy { get; set; }


        //fileUpload.ID = id;
        //    fileUpload. = documentNo;
        //    fileUpload. = pathFile;
        //    fileUpload. = fileName;
        //    fileUpload. = uploadDate;
        //    fileUpload. = uploadBy;
    }
}
