﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ESS.Data.Model
{
    public class LogHistoryModel
    {
        public DateTime? ActionTime { get; set; }
        public string RequestAccessNo { get; set; }
        public string NIKSite { get; set; }
        public string Nama { get; set; }
        public string Jabatan { get; set; }
        public string Role { get; set; }
        public string Action { get; set; }
        public bool IsApprove { get; set; }
        public string Remarks { get; set; }
    }
}
