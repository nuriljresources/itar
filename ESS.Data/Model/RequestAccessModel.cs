﻿using ESS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ESS.Data.Model
{
    public class RequestAccessModel
    {
        public RequestAccessModel()
        {
            RequestStatus = REQUEST_STATUS.NEW;
            StatusWorkFlow = REQUEST_STATUS.NEW;
            RequestDateStr = DateTime.Now.ToString(Constant.FORMAT_DATE_JRESOURCES);
            RequestUser = new UserModel();
            FileAttachments = new List<FileUploadModel>();
        }

        public Guid ID { get; set; }
        public string RequestAccessNo { get; set; }
        public string RequestStatus { get; set; }
        public string StatusWorkFlow { get; set; }
        public DateTime RequestDate { get; set; }
        public string RequestDateStr { get; set; }
        public DateTime? AppointmentDate { get; set; }
        public string AppointmentDateStr { get; set; }
        public string CompanyCode { get; set; }
        public UserModel RequestUser { get; set; }
        public UserModel AppointmentUser { get; set; }
        public bool IsEmailAddresAccess { get; set; }
        public bool IsUSBPortAccess { get; set; }
        public bool IsAccpacAccess { get; set; }
        public bool IsVPNAccess { get; set; }
        public bool IsEmailAddresToExtAccess { get; set; }
        public bool IsInternetAccess { get; set; }
        public bool IsSharepointAccess { get; set; }
        public bool IsEmailDomainRegister { get; set; }
        public bool IsMSR { get; set; }
        public bool IsHRMS { get; set; }
        public bool IsJOLIS { get; set; }
        public bool IsCEA { get; set; }
        public bool IsNonEmployee { get; set; }
        public bool IsOther { get; set; }

        public bool IsACCFIN { get; set; }
        public bool IsACCACC { get; set; }
        public bool IsACCPRC { get; set; }
        public bool IsACCWHD { get; set; }
        public bool IsACCCMS { get; set; }

        public bool IsJMINE { get; set; }
        public bool IsJPLT { get; set; }
        public bool IsJGEO { get; set; }
        public bool IsJEHS { get; set; }

        public bool IsJMAINTENANCE { get; set; }
        public bool IsJSCM { get; set; }
        public bool IsJCAFFAIR { get; set; }
        public bool IsJMEMO { get; set; }

        public bool IsDocumentCenter { get; set; }

        public string Notes { get; set; }

        public UserModel Manager { get; set; }
        public UserModel VP { get; set; }
        public UserModel ManagerIT { get; set; }
        public UserModel VPIT { get; set; }

        public UserModel ITAdmin { get; set; }
        public UserModel ITSPV { get; set; }

        public UserModel UserLastAction { get; set; }

        public UserModel CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public String CreatedDateStr { get; set; }

        public UserModel UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public String UpdatedDateStr { get; set; }

        public bool IsFinish { get; set; }
        public DateTime FinishDate { get; set; }

        public List<LogHistoryModel> LogHistories { get; set; }
        public List<FileUploadModel> FileAttachments { get; set; }

    }
}
