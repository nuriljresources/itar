﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ESS.Data.Model
{
    public class RequestAccessSearchModel : SearchModel
    {
        public RequestAccessSearchModel()
        {
            ListData = new List<RequestAccessModel>();
        }
        public string RequestAccessNo { get; set; }
        public string Notes { get; set; }
        public string FullName { get; set; }
        public string NIKSite { get; set; }
        public string Site { get; set; }
        public string Status { get; set; }

        public List<RequestAccessModel> ListData { get; set; }
    }
}
