﻿using ESS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ESS.Data.Model
{
    public class RequestDeviceModel
    {
        public RequestDeviceModel()
        {
            RequestStatus = REQUEST_STATUS.NEW;
            StatusWorkFlow = REQUEST_STATUS.NEW;
            RequestDateStr = DateTime.Now.ToString(Constant.FORMAT_DATE_JRESOURCES);
            RequestUser = new UserModel();
            LogHistories = new List<LogHistoryModel>();
            FileAttachments = new List<FileUploadModel>();
            RequestDeviceItems = new List<RequestDeviceItemModel>();
        }

        public Guid ID { get; set; }
        public string RequestDeviceNo { get; set; }
        public string RequestStatus { get; set; }
        public string StatusWorkFlow { get; set; }
        public DateTime RequestDate { get; set; }
        public string RequestDateStr { get; set; }
        public string CompanyCode { get; set; }
        public UserModel RequestUser { get; set; }

        public string Notes { get; set; }

        public UserModel Manager { get; set; }
        public UserModel VP { get; set; }
        public UserModel ManagerIT { get; set; }
        public UserModel VPIT { get; set; }

        public UserModel ITAdmin { get; set; }
        public UserModel ITSPV { get; set; }
        public UserModel UserLastAction { get; set; }

        public UserModel CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public String CreatedDateStr { get; set; }

        public UserModel UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public String UpdatedDateStr { get; set; }

        public bool IsFinish { get; set; }
        public DateTime FinishDate { get; set; }

        public List<RequestDeviceItemModel> RequestDeviceItems { get; set; }
        public List<LogHistoryModel> LogHistories { get; set; }
        public List<FileUploadModel> FileAttachments { get; set; }

    }

    public class RequestDeviceItemModel
    {
        public RequestDeviceItemModel()
        {
            ItemStatus = "NEW";
        }

        public Guid ID { get; set; }
        public Guid RequestDeviceId { get; set; }
        public String ItemName { get; set; }
        public String Note { get; set; }
        public String NoteAdmin { get; set; }
        public int Qty { get; set; }
        public string ItemStatus { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }


}
