﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ESS.Data.Model
{
    public class RequestDeviceSearchModel : SearchModel
    {
        public RequestDeviceSearchModel()
        {
            ListData = new List<RequestDeviceModel>();
        }
        public string RequestDeviceNo { get; set; }
        public string Notes { get; set; }
        public string FullName { get; set; }
        public string NIKSite { get; set; }
        public string Site { get; set; }
        public string Status { get; set; }

        public List<RequestDeviceModel> ListData { get; set; }
    }
}
