using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESS.Common;

namespace ESS.Data.Model
{
    public class SearchModel
    {
        public SearchModel()
        {
            CurrentPage = 1;
            initSet(false);

            ListDataGeneral = new List<object>();
        }

        public SearchModel(bool IsSetDate)
        {
            initSet(IsSetDate);
        }

        private void initSet(bool IsSetDate)
        {
            if (IsSetDate)
            {
                DateTime dtTo = DateTime.Now;
                DateTime dtFrom = DateHelper.DateFromMinimun(new DateTime(dtTo.Year, dtTo.Month, 1));
                DateTo = dtTo;
                DateToStr = dtTo.ToString(Constant.FORMAT_DATE_JRESOURCES);
                DateFrom = dtFrom;
                DateFromStr = dtFrom.ToString(Constant.FORMAT_DATE_JRESOURCES);
            }
        }

        public string[] ROLE_ACCESS { get; set; }
        public string[] MSR_STATUS { get; set; }

        public string SearchCriteria { get; set; }

        public string DateFromStr { get; set; }
        public DateTime? DateFrom { get; set; }
        public string DateToStr { get; set; }
        public DateTime? DateTo { get; set; }

        public string IsDeletedString { get; set; }
        public bool? IsDeleted
        {
            get
            {
                if (string.IsNullOrEmpty(this.IsDeletedString))
                {
                    if (this.IsDeletedString == "1")
                        return true;
                }

                return false;
            }
        }

        public string MoreCriteria { get; set; }

        // Untuk Order By
        // Additional
        public string OrderBy { get; set; }
        public bool IsOrderByDesc { get; set; }

        public int TotalItems { get; set; }
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public int TotalPages { get; set; }
        public int StartPage { get; set; }
        public int EndPage { get; set; }

        // Additonal Untuk Approva Cancel
        public bool IsPendingCancel { get; set; }

        /// <summary>
        /// Untuk Search Requisition
        /// </summary>
        public bool IsRequisition { get; set; }

        public void SetPager(int totalItems)
        {
            // calculate total, start and end pages
            var totalPages = (int)Math.Ceiling((decimal)totalItems / (decimal)Common.SiteSettings.PAGE_SIZE_PAGING);
            var startPage = CurrentPage - 5;
            var endPage = CurrentPage + 4;
            if (startPage <= 0)
            {
                endPage -= (startPage - 1);
                startPage = 1;
            }
            if (endPage > totalPages)
            {
                endPage = totalPages;
                if (endPage > 10)
                {
                    startPage = endPage - 9;
                }
            }

            TotalItems = totalItems;
            PageSize = Common.SiteSettings.PAGE_SIZE_PAGING;
            TotalPages = totalPages;
            StartPage = startPage;
            EndPage = endPage;
        }

        public void SetDateRange()
        {
            if (!string.IsNullOrEmpty(DateFromStr))
            {
                var dateConvert = Common.CommonFunction.DateTimeESS(DateFromStr);
                if (dateConvert != DateTime.MinValue)
                {
                    DateFrom = Common.DateHelper.DateFromMinimun(dateConvert);
                }
            }

            if (!string.IsNullOrEmpty(DateToStr))
            {
                var dateConvert = Common.CommonFunction.DateTimeESS(DateToStr);
                if (dateConvert != DateTime.MinValue)
                {
                    DateTo = Common.DateHelper.DateToMax(dateConvert);
                }
            }
        }
        public bool IsSearchFilter { get; set; }

        public List<Object> ListDataGeneral { get; set; }
    }

}
