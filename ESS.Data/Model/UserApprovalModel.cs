﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ESS.Data.Model
{
    public class UserApprovalModel
    {
        public UserModel Manager { get; set; }
        public UserModel VP { get; set; }
        public UserModel ManagerIT { get; set; }
        public UserModel VPIT { get; set; }

        public UserModel ITAdmin { get; set; }
        public UserModel ITSPV { get; set; }
    }

    public class UserApprovalChangeRequestModel
    {
        /// <summary>
        /// Approval Level
        /// </summary>
        public UserModel Manager { get; set; }
        public int ManagerCheck { get; set; }
        public string ManagerCheckDate { get; set; }

        public UserModel GeneralManager { get; set; }
        public int GeneralManagerCheck { get; set; }
        public string GeneralManagerCheckDate { get; set; }

        public UserModel OwnerApp { get; set; }
        public int OwnerAppCheck { get; set; }
        public string OwnerAppCheckDate { get; set; }

        public UserModel GMHO { get; set; }
        public int GMHOCheck { get; set; }
        public string GMHOCheckDate { get; set; }

        public UserModel VPHO { get; set; }
        public int VPHOCheck { get; set; }
        public string VPHOCheckDate { get; set; }

        public UserModel ITApp { get; set; }
        public int ITAppCheck { get; set; }
        public string ITAppCheckDate { get; set; }

        public UserModel ManagerIT { get; set; }
        public int ManagerITCheck { get; set; }
        public string ManagerITCheckDate { get; set; }

        public UserModel VPIT { get; set; }
        public int VPITCheck { get; set; }
        public string VPITCheckDate { get; set; }

        public UserModel ITAdmin { get; set; }
        public UserModel ITSPV { get; set; }
    }
}
