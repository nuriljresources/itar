using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace ESS.Data.Model
{
    public class UserLoginModel
    {
        [Required]
        public string Username { get; set; }
        public string Password { get; set; }

        [Required]
        public string CompanyCode { get; set; }
        public bool IsRemember { get; set; }
    }
}
