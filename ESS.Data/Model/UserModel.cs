﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using ESS.Common;
using Omu.ValueInjecter;

namespace ESS.Data.Model
{
    public class UserModel
    {
        public UserModel()
        {
            
        }

        public Guid ID { get; set; }
        [Required]
        public string Username { get; set; }
        public string UserIdAccpac { get; set; }
        public string UserIdMSR { get; set; }
        public string EmployeeId { get; set; }

        public string NIK { get; set; }

        [Required]
        public string NIKSite { get; set; }

        [Required]
        public string FullName { get; set; }


        // Additional
        public string RoleAccess { get; set; }
        public Guid CompanyId { get; set; }
        public string SiteCode { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }

        public Guid DepartmentId { get; set; }
        public string DepartmentCode { get; set; }
        public string DepartmentName { get; set; }

        public string DivisionCode { get; set; }
        public string DivisionName { get; set; }
        public string Jabatan { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public UserModel Superior { get; set; }

        public string Posisition { get; set; }

        [Required]
        public Guid RoleId { get; set; }
        public string RoleName { get; set; }

        public string kdFunct { get; set; }
        public string kdDivisi { get; set; }

        public string kdLevel { get; set; }

        /// <summary>
        /// Properties It is really just for a helper as set Role Access
        /// </summary>
        public List<string> ListRoleDepartment { get; set; }
        /// <summary>
        /// Flag If This User Administrator
        /// </summary>
        public Boolean IsAdministrator { get; set; }
        public string COOKIES_STRING
        {
            get
            {
                return string.Format("{0}#{1}#{2}#{3}", ID, Username, FullName, RoleId);
            }
        }


        /// <summary>
        /// Additional For Approval
        /// </summary>
        public bool IsApproved { get; set; }
        public DateTime ApprovedDate { get; set; }
        public String ApprovedDateStr { get; set; }

        #region function Helper
        /// <summary>
        /// Function Set Customer To Cookies
        /// </summary>
        /// <param name="strCookies"></param>
        public void SetFromCookies(string strCookies)
        {
            string[] fields = strCookies.Split('#');
            Guid Id = Guid.Empty;
            if (!string.IsNullOrEmpty(fields[0]))
            {
                Guid.TryParse(fields[0], out Id);
                this.ID = Id;
            }


            if (!string.IsNullOrEmpty(fields[1]))
            {
                this.Username = fields[1];
            }

            if (!string.IsNullOrEmpty(fields[2]))
            {
                this.FullName = fields[2];
            }

            if (!string.IsNullOrEmpty(fields[3]))
            {
                Id = Guid.Empty;
                Guid.TryParse(fields[3], out Id);
                this.RoleId = Id;
            }

        }
        #endregion
    }

    /// <summary>
    /// Class Ini Dipanggil saat sudah melakukan login Saja.
    /// Karena kalau tidak akan mengembalikan nilai null
    ///// </summary>
    //public class CurrentUser
    //{
    //    public static UserModel Model
    //    {
    //        get
    //        {
    //            var userCookies = CookieManager.Get(COOKIES_NAME.USER_MODEL);
    //            var user = new UserModel();
    //            user.SetFromCookies(userCookies);
    //            return user;
    //        }
    //    }

    //    public static Guid USERID
    //    {
    //        get
    //        {
    //            var user = UserLogin.GetByUsername(USERNAME);
    //            return user.ID;
    //        }
    //    }

    //    public static string NIK
    //    {
    //        get
    //        {
    //            var NIK = CookieManager.Get(COOKIES_NAME.USER_NIK);
    //            return string.IsNullOrEmpty(NIK) ? string.Empty : NIK;
    //        }
    //    }

    //    public static string COMPANY
    //    {
    //        get
    //        {
    //            var USER_COMPANY_CODE = CookieManager.Get(COOKIES_NAME.USER_COMPANY_CODE);
    //            return string.IsNullOrEmpty(USER_COMPANY_CODE) ? string.Empty : USER_COMPANY_CODE;
    //        }
    //    }

    //    public static string COMPANY_NAME
    //    {
    //        get
    //        {
    //            var USER_COMPANY_CODE = CookieManager.Get(COOKIES_NAME.USER_COMPANY_NAME);
    //            return string.IsNullOrEmpty(USER_COMPANY_CODE) ? string.Empty : USER_COMPANY_CODE;
    //        }
    //    }

    //    public static string NIKSITE
    //    {
    //        get
    //        {
    //            var NIKSITE = CookieManager.Get(COOKIES_NAME.USER_NIKSITE);
    //            return string.IsNullOrEmpty(NIKSITE) ? string.Empty : NIKSITE;
    //        }
    //    }

    //    public static string USERNAME
    //    {
    //        get
    //        {
    //            var USER_NAME = CookieManager.Get(COOKIES_NAME.USER_NAME);
    //            return string.IsNullOrEmpty(USER_NAME) ? string.Empty : USER_NAME;
    //        }
    //    }

    //    public static string FULLNAME
    //    {
    //        get
    //        {
    //            var USER_NAME = CookieManager.Get(COOKIES_NAME.FULL_NAME);
    //            return string.IsNullOrEmpty(USER_NAME) ? string.Empty : USER_NAME;
    //        }
    //    }


    //}
}