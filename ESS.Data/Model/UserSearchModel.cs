﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ESS.Data.Model
{
    public class UserSearchModel : SearchModel
    {
        public UserSearchModel()
        {
            ListData = new List<UserModel>();
        }

        public string NIKSITE { get; set; }
        public string Name { get; set; }
        public bool IsAllSite { get; set; }

        public List<UserModel> ListData { get; set; }
        public List<suggestion> suggestions { get; set; }
    }

    public class suggestion
    {
        public String value { get; set; }
        public Object data { get; set; }
    }
}
