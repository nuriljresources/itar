﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using ESS.BusinessLogic;
using ESS.Common;
using ESS.Data;
using ESS.Data.Model;

using Omu.ValueInjecter;
using PagedList;
using System.IO;

namespace ESS.Web.Controllers
{
    [ESSAuthorization]
    public partial class ChangeRequestController : Controller
    {
        public UserModel CurrentUser { get; set; }

        public ChangeRequestController()
        {
            List<V_ITAS_EMP> listSite = V_ITAS_EMP.GetAll()
                .Where(x => x.KdSite != "" && x.KdSite != null)
                .GroupBy(p => p.KdSite).Select(x => x.FirstOrDefault()).ToList();

            List<SelectListItem> ListSite = new List<SelectListItem>();
            ListSite.Add(new SelectListItem() { Value = "", Text = "Select Site" });
            foreach (var item in listSite)
            {
                ListSite.Add(new SelectListItem() { Value = item.KdSite, Text = item.nmSite });
            }

            ViewBag.ListSite = ListSite;

            List<SelectListItem> ListStatus = new List<SelectListItem>();
            ListStatus.Add(new SelectListItem() { Value = "-", Text = "Select Status" });
            ListStatus.Add(new SelectListItem() { Value = REQUEST_STATUS.NEW, Text = "NEW" });
            ListStatus.Add(new SelectListItem() { Value = REQUEST_STATUS.INITIAL, Text = "INITIAL" });
            ListStatus.Add(new SelectListItem() { Value = REQUEST_STATUS.IN_PROGRESS, Text = "IN PROGRES" });
            ListStatus.Add(new SelectListItem() { Value = REQUEST_STATUS.REVISION, Text = "REVISION" });
            ListStatus.Add(new SelectListItem() { Value = REQUEST_STATUS.COMPLETE, Text = "COMPLETE" });
            ViewBag.ListStatus = ListStatus;

            CurrentUser = UserLogic.GetCurrentUser();
        }

        public void ListItemApplication(string _applicationCode, string _moduleCode)
        {
            var listApplications = ApplicationList.GetAll().ToList();

            var GroupApplication = listApplications
                .GroupBy(u => u.ApplicationCode)
                .Select(grp => grp.ToList())
                .ToList();

            Dictionary<string, List<SelectListItem>> dicGroupApp = new Dictionary<string, List<SelectListItem>>();
            List<SelectListItem> _listApps = new List<SelectListItem>();
            List<SelectListItem> _listModule = new List<SelectListItem>();

            _listApps.Add(new SelectListItem() { Value = string.Empty, Text = "Select Application", Selected = true });
            _listModule.Add(new SelectListItem() { Value = string.Empty, Text = "Select Module", Selected = true });

            foreach (var groupApp in GroupApplication)
            {
                var _application = groupApp.FirstOrDefault();
                _listApps.Add(new SelectListItem()
                {
                    Value = _application.ApplicationCode,
                    Text = _application.ApplicationName,
                    Selected = (_application.ApplicationCode == _applicationCode)
                });

                if (!string.IsNullOrEmpty(_applicationCode) && _application.ApplicationCode == _applicationCode)
                {
                    foreach (var itemModule in groupApp)
                    {
                        _listModule.Add(new SelectListItem()
                        {
                            Value = itemModule.ModuleCode,
                            Text = itemModule.ModuleName,
                            Selected = (itemModule.ModuleCode == _moduleCode)
                        });
                    }
                }

                List<SelectListItem> _listGroupModule = new List<SelectListItem>();
                if (groupApp.Count > 1)
                {
                    _listGroupModule.Add(new SelectListItem() { Value = string.Empty, Text = "Select Module", Selected = true });
                }

                foreach (var itemModule in groupApp)
                {
                    _listGroupModule.Add(new SelectListItem()
                    {
                        Value = itemModule.ModuleCode,
                        Text = itemModule.ModuleName,
                        Selected = ( (groupApp.Count == 1) || (itemModule.ModuleCode == _moduleCode) )
                    });
                }

                dicGroupApp.Add(_application.ApplicationCode, _listGroupModule);
            }

            ViewBag.GroupApplication = dicGroupApp;
            ViewBag.ListApplication = _listApps;
            ViewBag.ListModule = _listModule;
        }

        public void BindApproval(string _changeRequestNo)
        {
            var crApprovalUser = UserLogic.GetUserApprovalChangeRequest(_changeRequestNo);
            ViewBag.ApprovalUser = crApprovalUser;
        }

        public virtual ActionResult Index(
            string ChangeRequestNo = "",
            string Notes = "",
            int CurrentPage = 1,
            string FullName = "",
            string NIKSite = "",
            string Site = "",
            string Status = "")
        {
            ChangeRequestSearchModel result = new ChangeRequestSearchModel();
            result.ChangeRequestNo = ChangeRequestNo;
            result.Notes = Notes;
            result.CurrentPage = CurrentPage;
            result.FullName = FullName;
            result.NIKSite = NIKSite;
            result.Site = Site;
            result.Status = Status;

            var datas = ChangeRequest.GetAll();

            if (CurrentUser.RoleAccess == ROLE.IT)
            {
                datas = datas.Where(x => x.kdsite == CurrentUser.SiteCode);
            }

            if (CurrentUser.RoleAccess == ROLE.USERS)
            {
                datas = datas.Where(x => (x.NIKSite == CurrentUser.NIKSite || x.CreatedBy == CurrentUser.NIKSite));
            }

            if (!string.IsNullOrEmpty(result.ChangeRequestNo))
            {
                datas = datas.Where(x => x.ChangeRequestNo.Contains(result.ChangeRequestNo.Trim()));
            }

            if (!string.IsNullOrEmpty(result.Notes))
            {
                datas = datas.Where(x => x.Reason.Contains(result.Notes) || x.DescriptionChange.Contains(result.Notes));
            }

            if (!string.IsNullOrEmpty(result.FullName))
            {
                var userList = V_ITAS_EMP.GetAll().Where(x => x.Nama.Contains(result.FullName));
                if (CurrentUser.RoleAccess != ROLE.ADMINISTRATOR)
                {
                    userList = userList.Where(x => x.KdSite == CurrentUser.SiteCode);
                }

                var userListNIKSite = userList.Select(x => x.NIKSITE).ToList();
                datas = datas.Where(x => userListNIKSite.Contains(x.NIKSite));
            }

            if (!string.IsNullOrEmpty(result.NIKSite))
            {
                datas = datas.Where(x => x.NIKSite.Contains(result.NIKSite));
            }

            if (!string.IsNullOrEmpty(result.Site))
            {
                datas = datas.Where(x => x.kdsite == result.Site);
            }

            if (result.Status != "-")
            {
                datas = datas.Where(x => x.StatusWF == result.Status);
            }

            result.SetPager(datas.Count());

            datas.ToPagedList(result.CurrentPage, SiteSettings.PAGE_SIZE_PAGING);

            List<ChangeRequest> listData = datas.OrderByDescending(x => x.CreatedDate)
                .Skip((result.CurrentPage - 1) * Common.SiteSettings.PAGE_SIZE_PAGING)
                .Take(Common.SiteSettings.PAGE_SIZE_PAGING).ToList();

            foreach (var item in listData)
            {
                ChangeRequestModel newItem = new ChangeRequestModel();
                newItem.ID = item.ID;
                newItem.ChangeRequestNo = item.ChangeRequestNo;
                newItem.Reason = item.Reason;
                newItem.RequestUser = UserLogic.GetByNIK(item.NIKSite);
                newItem.CreatedDate = item.CreatedDate;
                newItem.StatusWF = item.StatusWF;

                if (!String.IsNullOrEmpty(item.NxtActionr) && item.NxtActionr != "-")
                {
                    newItem.UserLastAction = UserLogic.GetByNIK(item.NxtActionr);
                }

                if (item.DateFinish.HasValue)
                {
                    newItem.IsFinish = true;
                    newItem.DateFinish = item.DateFinish.Value;
                }

                result.ListData.Add(newItem);
            }

            ViewData.Model = result;
            return View();
        }

        public virtual ActionResult Detail(Guid Id)
        {
            var model = ChangeRequestLogic.GetById(Id);

            if (Id != Guid.Empty && model.ID == Guid.Empty)
            {
                this.AddNotification("Request Access Not Found", NotificationType.ERROR);
                return RedirectToAction(MVC.ChangeRequest.Index());
            }

            ChangeRequestLogic.SetHistory(model);
            model.FileAttachments = FileUploadLogic.GetListFile(model.ChangeRequestNo);

            ListItemApplication(model.ApplicationCode, model.ModuleCode);
            BindApproval(model.ChangeRequestNo);
            ViewData.Model = model;
            return View();
        }

        [HttpPost]
        public virtual ActionResult Detail(ChangeRequestModel Model)
        {
            ResponseModel result = new ResponseModel();

            #region VALIDATION
            if (!string.IsNullOrEmpty(Model.RequestUser.NIKSite))
            {
                Model.RequestUser = UserLogic.GetByNIK(Model.RequestUser.NIKSite);
                if (!(Model.RequestUser != null && !String.IsNullOrEmpty(Model.RequestUser.FullName)))
                {
                    result.AddErrorList("NIK : " + Model.RequestUser.NIKSite + "not found in database Employee");
                }
                else
                {
                    if (!string.IsNullOrEmpty(Model.ChangeRequestNo))
                    {
                        Model.UserApproval = UserLogic.GetUserApprovalChangeRequest(Model.ChangeRequestNo);
                    }
                }
            }
            else
            {
                result.AddErrorList("Request User Must Be Select");
            }

            if (string.IsNullOrEmpty(Model.ApplicationCode))
            {
                result.AddErrorList("Application want to request must be select");
            }

            if (string.IsNullOrEmpty(Model.ModuleCode))
            {
                result.AddErrorList("Module Application want to request must be select");
            }

            if (string.IsNullOrEmpty(Model.Reason))
            {
                result.AddErrorList("Reason cannot be empty");
            }

            if (string.IsNullOrEmpty(Model.DescriptionChange))
            {
                result.AddErrorList("Description Change cannot be empty");
            }

            if (!(Model.IsChangeMasterData || Model.IsNewReport || Model.IsChangeUpdateReport || Model.IsNewInterfaceProgram || Model.IsAuthorisation || Model.IsApplication))
            {
                result.AddErrorList("Should at least select one access");
            }

            #endregion

            
            #region PROSES DATABASE
            if (result.IsSuccess)
            {
                var appItem = ApplicationList.GetModuleByModuleCode(Model.ModuleCode);
                var CR_APPROVAL = CurrentDataContext.CurrentContext.SP_GET_APPROVAL_CHANGEREQUEST(Model.RequestUser.NIKSite, Model.ModuleCode).FirstOrDefault();

                if (Model.ID == Guid.Empty)
                {
                    using (var transaction = new TransactionScope())
                    {
                        ChangeRequest newRequest = new ChangeRequest();

                        newRequest.ID = Guid.NewGuid();
                        newRequest.ChangeRequestNo = DocumentNumberLogic.GetDocumentNo(DOCUMENT_TYPE.ITCR);
                        newRequest.ApplicationCode = Model.ApplicationCode;
                        newRequest.ModuleCode = !string.IsNullOrEmpty(Model.ModuleCode) ? Model.ModuleCode : string.Empty;
                        newRequest.ProgramManager = string.Empty; //Model.ProgramManager;
                        newRequest.DateIssued = DateTime.Now;
                        newRequest.Reason = Model.Reason;
                        newRequest.StatusWF = REQUEST_STATUS.NEW;
                        newRequest.NIK = Model.RequestUser.NIK;
                        newRequest.NIKSite = Model.RequestUser.NIKSite;

                        newRequest.IsChangeMasterData = Model.IsChangeMasterData;
                        newRequest.IsNewReport = Model.IsNewReport;
                        newRequest.IsChangeUpdateReport = Model.IsChangeUpdateReport;
                        newRequest.IsNewInterfaceProgram = Model.IsNewInterfaceProgram;
                        newRequest.IsAuthorisation = Model.IsAuthorisation;
                        newRequest.IsApplication = Model.IsApplication;

                        newRequest.DescriptionChange = Model.DescriptionChange;
                        newRequest.LevelDevelopment = "Medium";//Model.LevelDevelopment;

                        newRequest.DevelopmentDescription = !string.IsNullOrEmpty(Model.DevelopmentDescription) ? Model.DevelopmentDescription : string.Empty;

                        newRequest.MGR = CR_APPROVAL.Manager;
                        newRequest.GMSITE = (Model.RequestUser.SiteCode != "JKT" ? CR_APPROVAL.GMSITE : string.Empty);
                        newRequest.GMHO = CR_APPROVAL.GMHO;
                        newRequest.VPHO = (Model.IsChangeMasterData ? CR_APPROVAL.VPHO :string.Empty);

                        newRequest.MGRIT = CR_APPROVAL.MGRIT;
                        newRequest.VPIT = CR_APPROVAL.VPIT;

                        newRequest.OWNAPP = appItem.NIKSite;
                        newRequest.ITAPP = appItem.NIKSiteAuthApproval;

                        
                        newRequest.CreatedDate = DateTime.Now;
                        newRequest.UpdatedBy = Model.RequestUser.NIKSite;
                        newRequest.UpdatedDate = DateTime.Now;

                        newRequest.kdsite = Model.RequestUser.SiteCode;
                        newRequest.CreatedBy = CurrentUser.NIKSite;

                        newRequest.Save<ChangeRequest>();
                        transaction.Complete();
                        result.SetSuccess("Request Access No : " + newRequest.ChangeRequestNo + " has been saved");

                        this.AddNotification(result);
                        return RedirectToAction(MVC.ChangeRequest.Detail(newRequest.ID));
                    }

                }
                else
                {
                    using (var transaction = new TransactionScope())
                    {
                        ChangeRequest updateRequest = ChangeRequest.GetByID(Model.ID);

                        updateRequest.ApplicationCode = Model.ApplicationCode;
                        updateRequest.ModuleCode = Model.ModuleCode;
                        //updateRequest.ProgramManager = Model.ProgramManager;
                        //updateRequest.DateIssued = Model.DateIssued;
                        updateRequest.Reason = Model.Reason;
                        updateRequest.IsChangeMasterData = Model.IsChangeMasterData;
                        updateRequest.IsNewReport = Model.IsNewReport;
                        updateRequest.IsChangeUpdateReport = Model.IsChangeUpdateReport;
                        updateRequest.IsNewInterfaceProgram = Model.IsNewInterfaceProgram;
                        updateRequest.IsAuthorisation = Model.IsAuthorisation;
                        updateRequest.IsApplication = Model.IsApplication;
                        updateRequest.NIK = Model.RequestUser.NIK;
                        updateRequest.NIKSite = Model.RequestUser.NIKSite;

                        updateRequest.DescriptionChange = Model.DescriptionChange;
                        //updateRequest.LevelDevelopment = Model.LevelDevelopment;

                        updateRequest.DevelopmentDescription = !string.IsNullOrEmpty(Model.DevelopmentDescription) ? Model.DevelopmentDescription : string.Empty;

                        updateRequest.MGR = CR_APPROVAL.Manager;
                        updateRequest.GMSITE = CR_APPROVAL.GMSITE;
                        updateRequest.GMHO = CR_APPROVAL.GMHO;
                        updateRequest.VPHO = (Model.IsChangeMasterData ? CR_APPROVAL.VPHO : string.Empty);
                        updateRequest.MGRIT = CR_APPROVAL.MGRIT;
                        updateRequest.VPIT = CR_APPROVAL.VPIT;

                        updateRequest.OWNAPP = appItem.NIKSite;
                        updateRequest.ITAPP = appItem.NIKSiteAuthApproval;


                        updateRequest.UpdatedBy = CurrentUser.NIKSite;
                        updateRequest.UpdatedDate = DateTime.Now;

                        updateRequest.Save<RequestAccess>();
                        transaction.Complete();
                        

                        result.SetSuccess("Request Access No : " + updateRequest.ChangeRequestNo + " has been update");

                        this.AddNotification(result);
                        return RedirectToAction(MVC.ChangeRequest.Detail(updateRequest.ID));
                    }
                }

                
            }
            #endregion

            ChangeRequestLogic.SetHistory(Model);
            Model.FileAttachments = FileUploadLogic.GetListFile(Model.ChangeRequestNo);
            this.AddNotification(result);

            ListItemApplication(Model.ApplicationCode, Model.ModuleCode);
            BindApproval(Model.ChangeRequestNo);
            ViewData.Model = Model;

            return View();
        }

        [HttpGet]
        public virtual ActionResult SubmitRequestAccess(Guid Id)
        {
            ChangeRequest request = ChangeRequest.GetByID(Id);
            if (request != null && request.ID != Guid.Empty)
            {
                if (request.StatusWF == REQUEST_STATUS.NEW || request.StatusWF == REQUEST_STATUS.REVISION)
                {
                    var CR_APPROVAL = CurrentDataContext.CurrentContext.SP_GET_APPROVAL_CHANGEREQUEST(request.NIKSite, request.ModuleCode).FirstOrDefault();
                    request.MGR = CR_APPROVAL.Manager;
                    request.GMSITE = CR_APPROVAL.GMSITE;
                    request.GMHO = CR_APPROVAL.GMHO;
                    request.VPHO = (request.IsChangeMasterData ? CR_APPROVAL.VPHO : string.Empty);
                    request.MGRIT = CR_APPROVAL.MGRIT;
                    request.VPIT = CR_APPROVAL.VPIT;

                    request.StatusWF = REQUEST_STATUS.INITIAL;
                    request.Save<RequestAccess>();
                    this.AddNotification("Request Access No : " + request.ChangeRequestNo + " Success To Submit", NotificationType.SUCCESS);
                }
                else
                {
                    this.AddNotification("Request Access No : " + request.ChangeRequestNo + " has been submit", NotificationType.ERROR);
                }
            }
            else
            {
                this.AddNotification("Request Access not found", NotificationType.ERROR);
            }


            return RedirectToAction(MVC.ChangeRequest.Detail(Id));
        }

        [HttpGet]
        public virtual ActionResult FinishChangeRequest(Guid Id)
        {
            ChangeRequest request = ChangeRequest.GetByID(Id);
            if (request != null && request.ID != Guid.Empty)
            {
                if (UserLogic.GetCurrentAccess == ROLE.ADMINISTRATOR)
                {
                    request.DateFinish = DateTime.Now;
                    request.Save<RequestAccess>();
                    this.AddNotification("Request Access No : " + request.ChangeRequestNo + " Success To Finish", NotificationType.SUCCESS);
                }
                else
                {
                    this.AddNotification("You don't have access to finish", NotificationType.ERROR);
                }
            }
            else
            {
                this.AddNotification("Request Access not found", NotificationType.ERROR);
            }


            return RedirectToAction(MVC.ChangeRequest.Index("", "", 1, "", "", "", "-"));
        }
    }
}
