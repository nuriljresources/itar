﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using ESS.BusinessLogic;
using ESS.Common;
using ESS.Data;
using ESS.Data.Model;
using Omu.ValueInjecter;


namespace ESS.Web.Controllers
{
    public partial class DelegationController : Controller
    {
        // GET: Delegation
        public virtual ActionResult Index()
        {
            DelegationSearchModel model = new DelegationSearchModel();

            ViewData.Model = model;
            return View();
        }


        public virtual JsonResult GetList(DelegationSearchModel model)
        {
            var delegations = Delegation.GetAll();
            delegations = delegations.Where(x => x.CreatedBy == UserLogic.GetCurrentNIKSite);

            if (!string.IsNullOrEmpty(model.SearchCriteria))
            {
                delegations = delegations.Where(x => x.DelegationNo.Contains(model.SearchCriteria));
            }

            model.SetPager(delegations.Count());
            List<Delegation> listData = delegations.OrderByDescending(x => x.CreatedDate)
                .Skip((model.CurrentPage - 1) * Common.SiteSettings.PAGE_SIZE_PAGING)
                .Take(Common.SiteSettings.PAGE_SIZE_PAGING).ToList();

            foreach (var item in listData)
            {
                var itemDel = new DelegationModel();
                itemDel.InjectFrom(item);

                itemDel.DateFromStr = item.DateFrom.ToString(Constant.FORMAT_DATE_JRESOURCES);
                itemDel.DateToStr = item.DateTo.ToString(Constant.FORMAT_DATE_JRESOURCES);

                model.ListData.Add(itemDel);
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual ActionResult Entry()
        {
            ViewData.Model = new DelegationModel();
            UserModel currentUser = UserLogic.GetCurrentUser();

            ViewBag.IsHaveAccessAccpac = (!string.IsNullOrEmpty(currentUser.UserIdAccpac) ? true : false);
            ViewBag.IsHaveAccessMSR = (!string.IsNullOrEmpty(currentUser.UserIdMSR) ? true : false);

            return View();
        }

        [HttpGet]
        public virtual ActionResult DelegationView(Guid DelegationId)
        {
            DelegationModel model = DelegationLogic.GetByDelegationId(DelegationId);
            ViewData.Model = model;

            return View("Entry");
        }

        [HttpPost]
        public virtual JsonResult Save(DelegationModel model)
        {
            DelegationLogic logic = new DelegationLogic();
            var response = logic.Save(model);

            return Json(response, JsonRequestBehavior.AllowGet);
        }


    }
}