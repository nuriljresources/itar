﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

using ESS.Data;
using ESS.Common;
using ESS.BusinessLogic;

namespace ESS.Web.Controllers
{
    [ESSAuthorization]
    public partial class HomeController : Controller
    {
        public virtual ActionResult Index()
        {
            var nik = CookieManager.Get(COOKIES_NAME.USER_NIK);
            var user = UserLogic.GetByNIK(nik);
            ViewBag.NIK= nik;

            return View();
        }


    }
}