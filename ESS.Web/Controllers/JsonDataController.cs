﻿using ESS.BusinessLogic;
using ESS.Common;
using ESS.Data;
using ESS.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Omu.ValueInjecter;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace ESS.Web.Controllers
{
    public partial class JsonDataController : Controller
    {
        [HttpGet]
        public virtual JsonResult SearchUser()
        {
            String query = string.Empty;

            if (Request.QueryString != null && !String.IsNullOrEmpty(Request.QueryString["query"]))
            {
                query = Request.QueryString["query"];
            }

            UserSearchModel result = new UserSearchModel();
            result.suggestions = new List<suggestion>();

            var resultData = V_ITAS_EMP.SearchByName(query).Take(10);
            if(resultData != null && resultData.Count() > 0)
            {
                foreach (var item in resultData)
                {
                    result.suggestions.Add(new suggestion() { value = String.Format("{0} ({1} - {2})", item.Nama, item.nmJabat, item.NmCompany), data = item });
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual JsonResult SearchUserDC()
        {
            String query = string.Empty;

            if (Request.QueryString != null && !String.IsNullOrEmpty(Request.QueryString["query"]))
            {
                query = Request.QueryString["query"];
            }

            UserSearchModel result = new UserSearchModel();
            result.suggestions = new List<suggestion>();

            var resultData = V_ITAS_EMP.SearchByNameDC(query).Take(10);
            if (resultData != null && resultData.Count() > 0)
            {
                foreach (var item in resultData)
                {
                    result.suggestions.Add(new suggestion() { value = String.Format("{0} ({1} - {2})", item.Nama, item.nmJabat, item.NmCompany), data = item });
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual JsonResult GetListApproval(string _NIKSite)
        {
            var result = UserLogic.GetUserApproval(_NIKSite);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual JsonResult GetListApprovalDoccenter(string _NIKSite)
        {
            var result = UserLogic.GetUserApprovalAccessDocCenter(_NIKSite);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public virtual JsonResult SaveUploadedFile()
        {
            StringBuilder sbMessage = new StringBuilder();

            string _documentNo = string.Empty;
            if (Request.Form != null && Request.Form["DocumentNo"] != null && !String.IsNullOrEmpty(Request.Form["DocumentNo"].ToString()))
            {
                _documentNo = Request.Form["DocumentNo"].ToString();
            }

            if (!String.IsNullOrEmpty(_documentNo))
            {
                foreach (string fileName in Request.Files)
                {
                    try
                    {
                        HttpPostedFileBase file = Request.Files[fileName];
                        if (file != null && file.ContentLength > 0)
                        {
                            
                            string fileNameOnly = Path.GetFileNameWithoutExtension(file.FileName);
                            string fileExtension = Path.GetExtension(file.FileName);

                            string newFileName = Regex.Replace(fileNameOnly, "[^a-zA-Z0-9]", "_");
                            if (newFileName.Length > 40) newFileName = newFileName.Substring(0, 40);
                            newFileName = newFileName + "_" + Guid.NewGuid().ToString().Split('-')[0] + fileExtension;

                            string pathFile = Path.Combine("UploadFile", _documentNo.Replace('/', '_'));
                            string pathString = Path.Combine(Server.MapPath(@"\"), pathFile);

                            bool isExists = Directory.Exists(pathString);

                            if (!isExists)
                                Directory.CreateDirectory(pathString);

                            var path = string.Format("{0}\\{1}", pathString, newFileName);
                            file.SaveAs(path);

                            FileUpload itemFile = FileUpload.GetByDocumentNo(_documentNo).FirstOrDefault(x => x.FileName == file.FileName);

                            if (itemFile != null && itemFile.ID != Guid.Empty)
                            {
                                itemFile.UploadDate = DateTime.Now;
                                itemFile.UploadBy = UserLogic.GetCurrentNIKSite;
                            }
                            else
                            {
                                itemFile = new FileUpload();
                                itemFile.ID = Guid.NewGuid();
                                itemFile.PathFile = pathFile;
                                itemFile.FileName = newFileName;
                                itemFile.DocumentNo = _documentNo;
                                itemFile.UploadDate = DateTime.Now;
                                itemFile.UploadBy = UserLogic.GetCurrentNIKSite;
                            }

                            itemFile.Save<FileUpload>();
                            sbMessage.AppendFormat("File {0} success to upload", itemFile.FileName);
                        }
                    }
                    catch (Exception ex)
                    {
                        sbMessage.AppendFormat("File {0} failed to upload", fileName);
                    }


                }


            }

            return Json(sbMessage.ToString(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual JsonResult GetListFile(String documentNo)
        {
            List<FileUploadModel> list = FileUploadLogic.GetListFile(documentNo);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual JsonResult DeleteFile(Guid FileId)
        {
            ResponseModel result = new ResponseModel();

            try
            {
                FileUpload fileUpload = FileUpload.GetById(FileId);
                if (fileUpload != null && fileUpload.ID != Guid.Empty)
                {
                    string pathString = Path.Combine(Server.MapPath(@"\"), fileUpload.PathFile, fileUpload.FileName);
                    bool IsExists = System.IO.File.Exists(pathString);
                    if (IsExists)
                    {
                        System.IO.File.Delete(pathString);
                        string fileName = fileUpload.FileName;
                        fileUpload.Delete<FileUpload>();
                        result.SetSuccess("File [" + fileName + "] success to deleted");
                    }
                    else
                    {
                        result.SetError("File not found in drive");
                    }
                }
                else
                {
                    result.SetError("File not found");
                }
            } catch(Exception ex)
            {
                result.SetError(ex.Message);
            }



            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}
