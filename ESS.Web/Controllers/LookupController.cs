﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using ESS.Data;
using ESS.Data.Model;
using ESS.BusinessLogic;

namespace ESS.Web.Controllers
{
    public partial class LookupController : Controller
    {
        public virtual PartialViewResult Employee()
        {
            return PartialView();
        }

        [HttpPost]
        public virtual JsonResult GetEmployee(UserSearchModel model)
        {
            model.ListData = new List<UserModel>();
            var currentUser = UserLogic.GetCurrentUser();

            var users = V_ITAS_EMP.GetAll();

            if(!model.IsAllSite)
            {
                users = users.Where(x => x.KdSite == currentUser.CompanyCode);
            }

            if (!string.IsNullOrEmpty(model.Name))
            {
                users = users.Where(x => x.Nama.Contains(model.Name) || x.NIKSITE.Contains(model.Name)).OrderByDescending(x => x.Nama);
            }

            model.SetPager(users.Count());
            List<V_ITAS_EMP> listData = users.OrderByDescending(x => x.Nama)
                .Skip((model.CurrentPage - 1) * Common.SiteSettings.PAGE_SIZE_PAGING)
                .Take(Common.SiteSettings.PAGE_SIZE_PAGING).ToList();

            foreach (var item in listData)
            {
                var _detail = new UserModel();
                _detail.FullName = item.Nama;
                _detail.NIKSite = item.NIKSITE;
                _detail.CompanyCode = item.KdSite;
                _detail.CompanyName = item.nmSite;
                _detail.DepartmentCode = item.KdDepar;
                _detail.DepartmentName = item.nmDepar;

                model.ListData.Add(_detail);
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public virtual PartialViewResult TaskDelegation()
        {
            return PartialView();
        }
    }
}