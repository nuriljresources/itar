﻿using ESS.BusinessLogic;
using ESS.Common;
using ESS.Data;
using ESS.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;

using Omu.ValueInjecter;
using PagedList;
using System.IO;

namespace ESS.Web.Controllers
{
    [ESSAuthorization]
    public partial class RequestController : Controller
    {
        public UserModel CurrentUser { get; set; }
        public RequestController()
        {
            List<V_ITAS_EMP> listSite = V_ITAS_EMP.GetAll()
                .Where(x => x.KdSite != "" && x.KdSite != null)
                .GroupBy(p => p.KdSite).Select(x => x.FirstOrDefault()).ToList();

            List<SelectListItem> ListSite = new List<SelectListItem>();
            ListSite.Add(new SelectListItem() { Value = "", Text = "Select Site" });
            foreach (var item in listSite)
            {
                ListSite.Add(new SelectListItem() { Value = item.KdSite, Text = item.nmSite });
            }

            ViewBag.ListSite = ListSite;

            List<SelectListItem> ListStatus = new List<SelectListItem>();
            ListStatus.Add(new SelectListItem() { Value = "-", Text = "Select Status" });
            ListStatus.Add(new SelectListItem() { Value = REQUEST_STATUS.NEW, Text = "NEW" });
            ListStatus.Add(new SelectListItem() { Value = REQUEST_STATUS.INITIAL, Text = "INITIAL" });
            ListStatus.Add(new SelectListItem() { Value = REQUEST_STATUS.IN_PROGRESS, Text = "IN PROGRES" });
            ListStatus.Add(new SelectListItem() { Value = REQUEST_STATUS.REVISION, Text = "REVISION" });
            ListStatus.Add(new SelectListItem() { Value = REQUEST_STATUS.COMPLETE, Text = "COMPLETE" });
            ViewBag.ListStatus = ListStatus;

            CurrentUser = UserLogic.GetCurrentUser();
        }

        #region ACCESS
        [HttpGet]
        public virtual ActionResult Access(
            string RequestAccessNo = "",
            string Notes = "",
            int CurrentPage = 1,
            string FullName = "",
            string NIKSite = "",
            string Site = "",
            string Status = "")
        {
            RequestAccessSearchModel result = new RequestAccessSearchModel();
            result.RequestAccessNo = RequestAccessNo;
            result.Notes = Notes;
            result.CurrentPage = CurrentPage;
            result.FullName = FullName;
            result.NIKSite = NIKSite;
            result.Site = Site;
            result.Status = Status;

            var datas = RequestAccess.GetAll();

            if (CurrentUser.RoleAccess == ROLE.IT)
            {
                datas = datas.Where(x => x.kdsite == CurrentUser.SiteCode);
            }

            if (CurrentUser.RoleAccess == ROLE.USERS)
            {
                datas = datas.Where(x => (x.NIKSite == CurrentUser.NIKSite || x.CreatedBy == CurrentUser.NIKSite));
            }

            if (CurrentUser.RoleAccess == ROLE.DocCenter)
            {
                datas = datas.Where(x => (x.IsDocumentCenter == true || x.CreatedBy == CurrentUser.NIKSite));
            }

            if (!string.IsNullOrEmpty(result.RequestAccessNo))
            {
                datas = datas.Where(x => x.RequestAccessNo.Contains(result.RequestAccessNo.Trim()));
            }

            if (!string.IsNullOrEmpty(result.Notes))
            {
                datas = datas.Where(x => x.RequestDesc.Contains(result.Notes));
            }

            if (!string.IsNullOrEmpty(result.FullName))
            {
                datas = datas.Where(x => x.FullName.Contains(result.FullName));
            }

            if (!string.IsNullOrEmpty(result.NIKSite))
            {
                datas = datas.Where(x => x.NIKSite.Contains(result.NIKSite));
            }

            if (!string.IsNullOrEmpty(result.Site))
            {
                datas = datas.Where(x => x.kdsite == result.Site);
            }

            if (result.Status != "-")
            {
                datas = datas.Where(x => x.StatusWorkflow == result.Status);
            }

            result.SetPager(datas.Count());

            datas.ToPagedList(result.CurrentPage, SiteSettings.PAGE_SIZE_PAGING);

            List<RequestAccess> listData = datas.OrderByDescending(x => x.CreatedDate)
                .Skip((result.CurrentPage - 1) * Common.SiteSettings.PAGE_SIZE_PAGING)
                .Take(Common.SiteSettings.PAGE_SIZE_PAGING).ToList();

            foreach (var item in listData)
            {
                RequestAccessModel newItem = new RequestAccessModel();
                newItem.ID = item.ID;
                newItem.RequestAccessNo = item.RequestAccessNo;
                newItem.Notes = item.RequestDesc;
                newItem.RequestUser = UserLogic.GetByNIK(item.NIKSite);
                newItem.RequestDate = item.RequestDate;
                newItem.StatusWorkFlow = item.StatusWorkflow;
                newItem.IsDocumentCenter = item.IsDocumentCenter;

                if (!String.IsNullOrEmpty(item.NextAction) && item.NextAction != "-")
                {
                    newItem.UserLastAction = UserLogic.GetByNIK(item.NextAction);
                }

                if (item.FinishDate.HasValue)
                {
                    newItem.IsFinish = true;
                    newItem.FinishDate = item.FinishDate.Value;
                }
                
                result.ListData.Add(newItem);
            }

            ViewData.Model = result;
            return View();
        }

        [HttpGet]
        public virtual ActionResult AccessDetail(Guid Id)
        {
            var model = RequestAccessLogic.GetById(Id);

            if (Id != Guid.Empty && model.ID == Guid.Empty)
            {
                this.AddNotification("Request Access Not Found", NotificationType.ERROR);
                return RedirectToAction(MVC.Request.Access());
            }

            RequestAccessLogic.SetHistory(model);
            model.FileAttachments = FileUploadLogic.GetListFile(model.RequestAccessNo);
            ViewData.Model = model;
            return View();
        }

        [HttpPost]
        public virtual ActionResult AccessDetail(RequestAccessModel Model)
        {
            ResponseModel result = new ResponseModel();

            #region VALIDATION
            if (!string.IsNullOrEmpty(Model.RequestUser.NIKSite))
            {
                Model.RequestUser = UserLogic.GetByNIK(Model.RequestUser.NIKSite);
                Model.AppointmentUser = UserLogic.GetByNIK(Model.AppointmentUser.NIKSite);
                if (!(Model.RequestUser != null && !String.IsNullOrEmpty(Model.RequestUser.FullName)))
                {
                    result.AddErrorList("NIK : " + Model.RequestUser.NIKSite + "not found in database Employee");
                }
                else
                {
                    if (Model.IsDocumentCenter) {
                        var userApproval = UserLogic.GetUserApprovalAccessDocCenter(Model.RequestUser.NIKSite);
                        Model.Manager = userApproval.Manager;
                        Model.VP = userApproval.VP;
                        Model.ManagerIT = userApproval.ManagerIT;
                        Model.VPIT = userApproval.VPIT;
                        Model.ITAdmin = userApproval.ITAdmin;
                        Model.ITSPV = userApproval.ITSPV;

                        //var userinfo = UserLogic.GetByNIK(Model.RequestUser.NIKSite);

                        //if (!userinfo.kdLevel.Contains("4") && !userinfo.kdLevel.Contains("5"))
                        //{
                        //    result.AddErrorList("User Info must be General Manager or VP");
                        //}

                        if (Model.Manager.kdFunct != Model.AppointmentUser.kdFunct)
                        {
                            result.AddErrorList("User Info Function not the same as the appointed user");
                        }
                    }
                    else
                    {
                        var userApproval = UserLogic.GetUserApproval(Model.RequestUser.NIKSite);
                        Model.Manager = userApproval.Manager;
                        Model.VP = userApproval.VP;
                        Model.ManagerIT = userApproval.ManagerIT;
                        Model.VPIT = userApproval.VPIT;
                        Model.ITAdmin = userApproval.ITAdmin;
                        Model.ITSPV = userApproval.ITSPV;
                    }
                    if (!(Model.Manager != null && !String.IsNullOrEmpty(Model.Manager.FullName)))
                    {
                        result.AddErrorList("User dont have manager");
                    }

                    if (!(Model.VP != null && !String.IsNullOrEmpty(Model.VP.FullName)))
                    {
                        result.AddErrorList("User dont have VP");
                    }

                }
            }
            else
            {
                result.AddErrorList("Request User Must Be Select");
            }

            if (!( Model.IsEmailAddresAccess || Model.IsUSBPortAccess || Model.IsAccpacAccess || Model.IsVPNAccess || 
                Model.IsEmailAddresToExtAccess || Model.IsInternetAccess || Model.IsSharepointAccess || Model.IsEmailDomainRegister || 
                Model.IsMSR || Model.IsHRMS || Model.IsJOLIS || Model.IsCEA || Model.IsNonEmployee || Model.IsACCFIN || Model.IsACCACC ||
                Model.IsACCPRC || Model.IsACCWHD || Model.IsACCCMS || Model.IsJMINE || Model.IsJPLT || Model.IsJGEO || Model.IsJEHS || Model.IsDocumentCenter || Model.IsJMEMO || Model.IsOther))
            {
                result.AddErrorList("Should at least select one access");
            } else
            {
                if (Model.IsDocumentCenter)
                {
                    if (string.IsNullOrEmpty(Model.AppointmentUser.NIKSite))
                    {
                        result.AddErrorList("Appointment User Must Be Select");
                    }

                    if (string.IsNullOrEmpty(Model.AppointmentDateStr))
                    {
                        result.AddErrorList("Appointment Date Must Be Select");
                    } else
                    {
                        DateTime AppointmentDate = DateTime.MinValue;
                        if(DateTime.TryParse(Model.AppointmentDateStr, out AppointmentDate))
                        {
                            Model.AppointmentDate = AppointmentDate;
                        } else
                        {
                            result.AddErrorList("Appointment Date Error convert");
                        }
                    }
                }
            }
            #endregion

            #region PROSES DATABASE
            if (result.IsSuccess)
            {
                if (Model.ID == Guid.Empty)
                {
                    using (var transaction = new TransactionScope())
                    {
                        RequestAccess newRequest = new RequestAccess();
                        newRequest.ID = Guid.NewGuid();
                        newRequest.RequestAccessNo = DocumentNumberLogic.GetDocumentNo(DOCUMENT_TYPE.ITAR);
                        newRequest.RequestStatus = REQUEST_STATUS.NEW;
                        newRequest.RequestDate = DateTime.Now;
                        newRequest.CompanyCode = UserLogic.GetCurrentCompanyCode;
                        newRequest.NIK = Model.RequestUser.NIK;
                        newRequest.NIKSite = Model.RequestUser.NIKSite;
                        newRequest.FullName = Model.RequestUser.FullName;
                        newRequest.DepartmentName = Model.RequestUser.DepartmentName;
                        newRequest.Email = Model.RequestUser.Email;
                        if (Model.IsDocumentCenter)
                        {
                            newRequest.AppointmentDate = Model.AppointmentDate;
                            newRequest.AppointmentNIKSite = Model.AppointmentUser.NIKSite;
                        } else
                        {
                            newRequest.AppointmentDate = null;
                            newRequest.AppointmentNIKSite = null;
                        }

                        newRequest.ManagerByName = Model.Manager.FullName;
                        newRequest.ManagerByNIKSite = Model.Manager.NIKSite;
                        newRequest.VPRequesterName = Model.VP.FullName;
                        newRequest.VPRequesterNIKSite = Model.VP.NIKSite;

                        newRequest.ITManagerName = Model.ManagerIT.FullName;
                        newRequest.ITManagerNIKSite = Model.ManagerIT.NIKSite;
                        newRequest.ITVPName = Model.VPIT.FullName;
                        newRequest.ITVPNIKSite = Model.VPIT.NIKSite;
                        newRequest.ITADMIN = Model.ITAdmin.NIKSite;
                        newRequest.ITSPV = Model.ITSPV.NIKSite;

                        newRequest.RequestDesc = !String.IsNullOrEmpty(Model.Notes) ? Model.Notes : String.Empty;
                        newRequest.IsEmail = Model.IsEmailAddresAccess;
                        newRequest.IsUSBAccess = Model.IsUSBPortAccess;
                        newRequest.IsAccpacAccess = Model.IsAccpacAccess;
                        newRequest.IsVPNAccess = Model.IsVPNAccess;
                        newRequest.IsEmailExtended = Model.IsEmailAddresToExtAccess;
                        newRequest.IsInternetAccess = Model.IsInternetAccess;
                        newRequest.IsSharepointAccess = Model.IsSharepointAccess;
                        newRequest.IsEmailDomain = Model.IsEmailDomainRegister;
                        newRequest.IsMSRAccess = Model.IsMSR;
                        newRequest.IsHRMS = Model.IsHRMS;
                        newRequest.IsJOLIS = Model.IsJOLIS;
                        newRequest.IsCEA = Model.IsCEA;
                        newRequest.IsNonEmployee = Model.IsNonEmployee;

                        newRequest.IsACCFIN = Model.IsACCFIN;
                        newRequest.IsACCACC = Model.IsACCACC;
                        newRequest.IsACCPRC = Model.IsACCPRC;
                        newRequest.IsACCWHD = Model.IsACCWHD;
                        newRequest.IsACCCMS = Model.IsACCCMS;
                        newRequest.IsJMINE = Model.IsJMINE;
                        newRequest.IsJPLT = Model.IsJPLT;
                        newRequest.IsJGEO = Model.IsJGEO;
                        newRequest.IsJEHS = Model.IsJEHS;

                        newRequest.IsJMAINTENANCE = Model.IsJMAINTENANCE;
                        newRequest.IsJSCM = Model.IsJSCM;
                        newRequest.IsJCAFFAIR = Model.IsJCAFFAIR;
                        newRequest.IsJMEMO = Model.IsJMEMO;
                        
                        newRequest.IsDocumentCenter = Model.IsDocumentCenter;
                        newRequest.IsOther = Model.IsOther;

                        newRequest.kdsite = Model.RequestUser.SiteCode;
                        newRequest.CreatedBy = CurrentUser.NIKSite;

                        newRequest.Save<RequestAccess>();
                        transaction.Complete();
                        result.SetSuccess("Request Access No : " + newRequest.RequestAccessNo + " has been saved");


                        this.AddNotification(result);
                        return RedirectToAction(MVC.Request.AccessDetail(newRequest.ID));
                    }

                }
                else
                {
                    using (var transaction = new TransactionScope())
                    {
                        RequestAccess updateRequest = RequestAccess.GetByID(Model.ID);

                        updateRequest.NIK = Model.RequestUser.NIK;
                        updateRequest.NIKSite = Model.RequestUser.NIKSite;
                        updateRequest.FullName = Model.RequestUser.FullName;
                        updateRequest.DepartmentName = Model.RequestUser.DepartmentName;
                        updateRequest.Email = Model.RequestUser.Email;
                        if (Model.IsDocumentCenter)
                        {
                            updateRequest.AppointmentDate = Model.AppointmentDate;
                            updateRequest.AppointmentNIKSite = Model.AppointmentUser.NIKSite;
                        }
                        else
                        {
                            updateRequest.AppointmentDate = null;
                            updateRequest.AppointmentNIKSite = null;
                        }

                        updateRequest.ManagerByName = Model.Manager.FullName;
                        updateRequest.ManagerByNIKSite = Model.Manager.NIKSite;
                        updateRequest.VPRequesterName = Model.VP.FullName;
                        updateRequest.VPRequesterNIKSite = Model.VP.NIKSite;

                        updateRequest.ITManagerName = Model.ManagerIT.FullName;
                        updateRequest.ITManagerNIKSite = Model.ManagerIT.NIKSite;
                        updateRequest.ITVPName = Model.VPIT.FullName;
                        updateRequest.ITVPNIKSite = Model.VPIT.NIKSite;
                        updateRequest.ITADMIN = Model.ITAdmin.NIKSite;
                        updateRequest.ITSPV = Model.ITSPV.NIKSite;

                        updateRequest.RequestDesc = !String.IsNullOrEmpty(Model.Notes) ? Model.Notes : String.Empty;
                        updateRequest.IsEmail = Model.IsEmailAddresAccess;
                        updateRequest.IsUSBAccess = Model.IsUSBPortAccess;
                        updateRequest.IsAccpacAccess = Model.IsAccpacAccess;
                        updateRequest.IsVPNAccess = Model.IsVPNAccess;
                        updateRequest.IsEmailExtended = Model.IsEmailAddresToExtAccess;
                        updateRequest.IsInternetAccess = Model.IsInternetAccess;
                        updateRequest.IsSharepointAccess = Model.IsSharepointAccess;
                        updateRequest.IsEmailDomain = Model.IsEmailDomainRegister;
                        updateRequest.IsMSRAccess = Model.IsMSR;
                        updateRequest.IsHRMS = Model.IsHRMS;
                        updateRequest.IsJOLIS = Model.IsJOLIS;
                        updateRequest.IsCEA = Model.IsCEA;
                        updateRequest.IsNonEmployee = Model.IsNonEmployee;

                        updateRequest.IsACCFIN = Model.IsACCFIN;
                        updateRequest.IsACCACC = Model.IsACCACC;
                        updateRequest.IsACCPRC = Model.IsACCPRC;
                        updateRequest.IsACCWHD = Model.IsACCWHD;
                        updateRequest.IsACCCMS = Model.IsACCCMS;
                        updateRequest.IsJMINE = Model.IsJMINE;
                        updateRequest.IsJPLT = Model.IsJPLT;
                        updateRequest.IsJGEO = Model.IsJGEO;
                        updateRequest.IsJEHS = Model.IsJEHS;

                        updateRequest.IsJMAINTENANCE = Model.IsJMAINTENANCE;
                        updateRequest.IsJSCM = Model.IsJSCM;
                        updateRequest.IsJCAFFAIR = Model.IsJCAFFAIR;
                        updateRequest.IsJMEMO = Model.IsJMEMO;

                        updateRequest.IsDocumentCenter = Model.IsDocumentCenter;

                        updateRequest.IsOther = Model.IsOther;
                        updateRequest.kdsite = Model.RequestUser.SiteCode;
                        updateRequest.UpdatedBy = CurrentUser.NIKSite;

                        updateRequest.Save<RequestAccess>();
                        transaction.Complete();
                        result.SetSuccess("Request Access No : " + updateRequest.RequestAccessNo + " has been update");

                        this.AddNotification(result);
                        return RedirectToAction(MVC.Request.AccessDetail(updateRequest.ID));
                    }
                }
            }
            #endregion

            RequestAccessLogic.SetHistory(Model);
            Model.FileAttachments = FileUploadLogic.GetListFile(Model.RequestAccessNo);
            this.AddNotification(result);
            ViewData.Model = Model;

            return View();
        }

        [HttpGet]
        public virtual ActionResult SubmitRequestAccess(Guid Id)
        {
            RequestAccess request = RequestAccess.GetByID(Id);
            if (request != null && request.ID != Guid.Empty)
            {
                if (request.StatusWorkflow == REQUEST_STATUS.NEW || request.StatusWorkflow == REQUEST_STATUS.REVISION)
                {
                    request.StatusWorkflow = REQUEST_STATUS.INITIAL;
                    request.Save<RequestAccess>();
                    this.AddNotification("Request Access No : " + request.RequestAccessNo + " Success To Submit", NotificationType.SUCCESS);
                }
                else
                {
                    this.AddNotification("Request Access No : " + request.RequestAccessNo + " has been submit", NotificationType.ERROR);
                }
            }
            else
            {
                this.AddNotification("Request Access not found", NotificationType.ERROR);
            }


            return RedirectToAction(MVC.Request.AccessDetail(Id));
        }

        [HttpGet]
        public virtual ActionResult FinishRequestAccess(Guid Id)
        {
            RequestAccess request = RequestAccess.GetByID(Id);
            if (request != null && request.ID != Guid.Empty)
            {
                if (UserLogic.GetCurrentAccess == ROLE.ADMINISTRATOR)
                {
                    request.FinishDate = DateTime.Now;
                    request.Save<RequestAccess>();
                    this.AddNotification("Request Access No : " + request.RequestAccessNo + " Success To Finish", NotificationType.SUCCESS);
                }
                else
                {
                    this.AddNotification("You don't have access to finish", NotificationType.ERROR);
                }
            }
            else
            {
                this.AddNotification("Request Access not found", NotificationType.ERROR);
            }


            return RedirectToAction(MVC.Request.Access("", "", 1, "", "", "", "-"));
        }
        #endregion

        #region DEVICE
        [HttpGet]
        public virtual ActionResult Device(
            string RequestDeviceNo = "",
            string Notes = "",
            int CurrentPage = 1,
            string FullName = "",
            string NIKSite = "",
            string Site = "",
            string Status = "")
        {
            RequestDeviceSearchModel result = new RequestDeviceSearchModel();
            result.RequestDeviceNo = RequestDeviceNo;
            result.Notes = Notes;
            result.CurrentPage = CurrentPage;
            result.FullName = FullName;
            result.NIKSite = NIKSite;
            result.Site = Site;
            result.Status = Status;

            var datas = RequestDevice.GetAll();

            if (CurrentUser.RoleAccess == ROLE.IT)
            {
                datas = datas.Where(x => x.kdsite == CurrentUser.SiteCode);
            }

            if (CurrentUser.RoleAccess == ROLE.USERS)
            {
                datas = datas.Where(x => (x.NIKSite == CurrentUser.NIKSite || x.CreatedBy == CurrentUser.NIKSite));
            }

            if (!string.IsNullOrEmpty(result.RequestDeviceNo))
            {
                datas = datas.Where(x => x.RequestDeviceNo.Contains(result.RequestDeviceNo.Trim()));
            }

            if (!string.IsNullOrEmpty(result.Notes))
            {
                datas = datas.Where(x => x.RequestDesc.Contains(result.Notes));
            }

            if (!string.IsNullOrEmpty(result.FullName))
            {
                datas = datas.Where(x => x.FullName.Contains(result.FullName));
            }

            if (!string.IsNullOrEmpty(result.NIKSite))
            {
                datas = datas.Where(x => x.NIKSite.Contains(result.NIKSite));
            }

            if (!string.IsNullOrEmpty(result.Site))
            {
                datas = datas.Where(x => x.kdsite == result.Site);
            }

            if (result.Status != "-")
            {
                datas = datas.Where(x => x.StatusWorkflow == result.Status);
            }

            result.SetPager(datas.Count());

            datas.ToPagedList(result.CurrentPage, SiteSettings.PAGE_SIZE_PAGING);

            List<RequestDevice> listData = datas.OrderByDescending(x => x.CreatedDate)
                .Skip((result.CurrentPage - 1) * Common.SiteSettings.PAGE_SIZE_PAGING)
                .Take(Common.SiteSettings.PAGE_SIZE_PAGING).ToList();

            foreach (var item in listData)
            {
                RequestDeviceModel newItem = new RequestDeviceModel();
                newItem.ID = item.ID;
                newItem.RequestDeviceNo = item.RequestDeviceNo;
                newItem.Notes = item.RequestDesc;
                newItem.RequestUser = UserLogic.GetByNIK(item.NIKSite);
                newItem.RequestDate = item.RequestDate;
                newItem.StatusWorkFlow = item.StatusWorkflow;

                if (!String.IsNullOrEmpty(item.NextAction) && item.NextAction != "-")
                {
                    newItem.UserLastAction = UserLogic.GetByNIK(item.NextAction);
                }

                if (item.FinishDate.HasValue)
                {
                    newItem.IsFinish = true;
                    newItem.FinishDate = item.FinishDate.Value;
                }

                result.ListData.Add(newItem);
            }


            ViewData.Model = result;
            return View();
        }

        [HttpGet]
        public virtual ActionResult DeviceDetail(Guid Id)
        {
            var model = RequestDeviceLogic.GetById(Id);

            if (Id != Guid.Empty && model.ID == Guid.Empty)
            {
                this.AddNotification("Request Device Not Found", NotificationType.ERROR);
                return RedirectToAction(MVC.Request.Access());
            }

            RequestDeviceLogic.SetHistory(model);
            model.FileAttachments = FileUploadLogic.GetListFile(model.RequestDeviceNo);

            ViewData.Model = model;
            return View();
        }

        [HttpPost]
        public virtual ActionResult DeviceDetail(RequestDeviceModel Model)
        {
            ResponseModel result = new ResponseModel();

            try
            {
                #region VALIDATION
                if (!string.IsNullOrEmpty(Model.RequestUser.NIKSite))
                {
                    Model.RequestUser = UserLogic.GetByNIK(Model.RequestUser.NIKSite);
                    if (!(Model.RequestUser != null && !String.IsNullOrEmpty(Model.RequestUser.FullName)))
                    {
                        result.AddErrorList("NIK : " + Model.RequestUser.NIKSite + "not found in database Employee");
                    }
                    else
                    {
                        var userApproval = UserLogic.GetUserApproval(Model.RequestUser.NIKSite);
                        Model.Manager = userApproval.Manager;
                        Model.VP = userApproval.VP;
                        Model.ManagerIT = userApproval.ManagerIT;
                        Model.VPIT = userApproval.VPIT;
                        Model.ITAdmin = userApproval.ITAdmin;
                        Model.ITSPV = userApproval.ITSPV;

                        if (!(Model.Manager != null && !String.IsNullOrEmpty(Model.Manager.FullName)))
                        {
                            result.AddErrorList("User dont have manager");
                        }

                        if (!(Model.VP != null && !String.IsNullOrEmpty(Model.VP.FullName)))
                        {
                            result.AddErrorList("User dont have VP");
                        }
                    }
                }
                else
                {
                    result.AddErrorList("Request User Must Be Select");
                }

                var requestDeviceItemsChecks = Model.RequestDeviceItems.Where(x => !x.IsDeleted).ToList();
                if (!(requestDeviceItemsChecks != null && requestDeviceItemsChecks.Count > 0))
                {
                    result.AddErrorList("Must have one item request device");
                }
                #endregion

                if (result.IsSuccess)
                {
                    if (Model.ID == Guid.Empty)
                    {
                        #region INSERT
                        using (var transaction = new TransactionScope())
                        {
                            RequestDevice newRequest = new RequestDevice();
                            newRequest.ID = Guid.NewGuid();
                            newRequest.RequestDeviceNo = DocumentNumberLogic.GetDocumentNo(DOCUMENT_TYPE.ITDR);
                            newRequest.RequestStatus = REQUEST_STATUS.NEW;
                            newRequest.RequestDate = DateTime.Now;
                            newRequest.CompanyCode = UserLogic.GetCurrentCompanyCode;
                            newRequest.NIK = Model.RequestUser.NIK;
                            newRequest.NIKSite = Model.RequestUser.NIKSite;
                            newRequest.FullName = Model.RequestUser.FullName;
                            newRequest.DepartmentName = Model.RequestUser.DepartmentName;
                            newRequest.Email = Model.RequestUser.Email;

                            newRequest.ManagerByName = Model.Manager.FullName;
                            newRequest.ManagerByNIKSite = Model.Manager.NIKSite;
                            newRequest.VPRequesterName = Model.VP.FullName;
                            newRequest.VPRequesterNIKSite = Model.VP.NIKSite;

                            newRequest.ITManagerName = Model.ManagerIT.FullName;
                            newRequest.ITManagerNIKSite = Model.ManagerIT.NIKSite;
                            newRequest.ITVPName = Model.VPIT.FullName;
                            newRequest.ITVPNIKSite = Model.VPIT.NIKSite;
                            newRequest.ITADMIN = Model.ITAdmin.NIKSite;
                            newRequest.ITSPV = Model.ITSPV.NIKSite;

                            newRequest.RequestDesc = !String.IsNullOrEmpty(Model.Notes) ? Model.Notes : String.Empty;
                            newRequest.kdsite = Model.RequestUser.SiteCode;

                            newRequest.CreatedBy = CurrentUser.NIKSite;

                            foreach (var item in Model.RequestDeviceItems)
                            {
                                RequestDeviceItem newItem = new RequestDeviceItem();
                                newItem.ID = Guid.NewGuid();
                                newItem.RequestDeviceId = newRequest.ID;
                                newItem.ItemName = item.ItemName;
                                newItem.Note = !String.IsNullOrEmpty(item.Note) ? item.Note : String.Empty;
                                newItem.NoteAdmin = !String.IsNullOrEmpty(item.NoteAdmin) ? item.NoteAdmin : String.Empty;
                                newItem.Qty = item.Qty;
                                newItem.ItemStatus = item.ItemStatus;
                                newItem.CreatedDate = DateTime.Now;
                                newItem.UpdatedDate = DateTime.Now;

                                newRequest.RequestDeviceItems.Add(newItem);
                            }

                            newRequest.Save<RequestDevice>();
                            transaction.Complete();
                            result.SetSuccess("Request Access No : " + newRequest.RequestDeviceNo + " has been saved");

                            this.AddNotification(result);
                            return RedirectToAction(MVC.Request.DeviceDetail(newRequest.ID));
                        }
                        #endregion
                    }
                    else
                    {
                        #region UPDATE
                        using (var transaction = new TransactionScope())
                        {
                            RequestDevice updateRequest = RequestDevice.GetByID(Model.ID);
                            updateRequest.NIK = Model.RequestUser.NIK;
                            updateRequest.NIKSite = Model.RequestUser.NIKSite;
                            updateRequest.FullName = Model.RequestUser.FullName;
                            updateRequest.DepartmentName = Model.RequestUser.DepartmentName;
                            updateRequest.Email = Model.RequestUser.Email;

                            updateRequest.ManagerByName = Model.Manager.FullName;
                            updateRequest.ManagerByNIKSite = Model.Manager.NIKSite;
                            updateRequest.VPRequesterName = Model.VP.FullName;
                            updateRequest.VPRequesterNIKSite = Model.VP.NIKSite;

                            updateRequest.ITManagerName = Model.ManagerIT.FullName;
                            updateRequest.ITManagerNIKSite = Model.ManagerIT.NIKSite;
                            updateRequest.ITVPName = Model.VPIT.FullName;
                            updateRequest.ITVPNIKSite = Model.VPIT.NIKSite;

                            updateRequest.RequestDesc = !String.IsNullOrEmpty(Model.Notes) ? Model.Notes : String.Empty;
                            updateRequest.kdsite = Model.RequestUser.SiteCode;
                            updateRequest.UpdatedBy = CurrentUser.NIKSite;

                            var rdIds = updateRequest.RequestDeviceItems.Where(x => x.IsDeleted == false).Select(x => x.ID).ToList();
                            foreach (var itemToDeleted in updateRequest.RequestDeviceItems)
                            {
                                var checkRDItem = Model.RequestDeviceItems.FirstOrDefault(x => x.ID == itemToDeleted.ID);
                                if ( !(checkRDItem != null && checkRDItem.ID != Guid.Empty) )
                                {
                                    itemToDeleted.IsDeleted = true;
                                    itemToDeleted.Save<RequestDeviceItem>();
                                }
                            }

                            foreach (var item in Model.RequestDeviceItems)
                            {
                                var itemUpdateDevice = updateRequest.RequestDeviceItems.FirstOrDefault(x => x.ID == item.ID);

                                if (itemUpdateDevice != null && itemUpdateDevice.ID != Guid.Empty)
                                {
                                    itemUpdateDevice.IsDeleted = item.IsDeleted;
                                    itemUpdateDevice.ItemName = item.ItemName;
                                    itemUpdateDevice.Qty = item.Qty;
                                    itemUpdateDevice.Note = !String.IsNullOrEmpty(item.Note) ? item.Note : String.Empty;
                                    itemUpdateDevice.NoteAdmin = !String.IsNullOrEmpty(item.NoteAdmin) ? item.NoteAdmin : String.Empty;
                                    itemUpdateDevice.ItemStatus = item.ItemStatus;
                                    itemUpdateDevice.UpdatedDate = DateTime.Now;
                                    itemUpdateDevice.UpdateSave<RequestDeviceItem>();
                                }
                                else
                                {
                                    if (!item.IsDeleted)
                                    {
                                        RequestDeviceItem newItem = new RequestDeviceItem();
                                        newItem.ID = Guid.NewGuid();
                                        newItem.RequestDeviceId = updateRequest.ID;
                                        newItem.ItemName = item.ItemName;
                                        newItem.Note = !String.IsNullOrEmpty(item.Note) ? item.Note : String.Empty;
                                        newItem.NoteAdmin = !String.IsNullOrEmpty(item.NoteAdmin) ? item.NoteAdmin : String.Empty;
                                        newItem.Qty = item.Qty;
                                        newItem.ItemStatus = item.ItemStatus;
                                        newItem.CreatedDate = DateTime.Now;
                                        newItem.UpdatedDate = DateTime.Now;

                                        newItem.InsertSave<RequestDeviceItem>();
                                    }
                                }
                            }

                            updateRequest.UpdateSave<RequestDevice>();
                            transaction.Complete();
                            result.SetSuccess("Request Access No : " + updateRequest.RequestDeviceNo + " has been updated");

                            this.AddNotification(result);
                            return RedirectToAction(MVC.Request.DeviceDetail(updateRequest.ID));
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                result.SetError("Error When Save " + ex.Message);
            }

            RequestDeviceLogic.SetHistory(Model);
            Model.FileAttachments = FileUploadLogic.GetListFile(Model.RequestDeviceNo);
            this.AddNotification(result);
            ViewData.Model = Model;
            return View();
        }


        [HttpGet]
        public virtual ActionResult SubmitRequestDevice(Guid Id)
        {
            RequestDevice request = RequestDevice.GetByID(Id);
            if (request != null && request.ID != Guid.Empty)
            {
                if (request.StatusWorkflow == REQUEST_STATUS.NEW || request.StatusWorkflow == REQUEST_STATUS.REVISION)
                {
                    request.StatusWorkflow = REQUEST_STATUS.INITIAL;
                    request.Save<RequestAccess>();
                    this.AddNotification("Request Device No : " + request.RequestDeviceNo + " Success To Submit", NotificationType.SUCCESS);
                }
                else
                {
                    this.AddNotification("Request Device No : " + request.RequestDeviceNo + " has been submit", NotificationType.ERROR);
                }
            }
            else
            {
                this.AddNotification("Request Device not found", NotificationType.ERROR);
            }


            return RedirectToAction(MVC.Request.Device("","",1,"","","","-"));
        }

        [HttpGet]
        public virtual ActionResult FinishRequestDevice(Guid Id)
        {
            RequestDevice request = RequestDevice.GetByID(Id);
            if (request != null && request.ID != Guid.Empty)
            {
                if (UserLogic.GetCurrentAccess == ROLE.ADMINISTRATOR)
                {
                    request.FinishDate = DateTime.Now;
                    request.Save<RequestDevice>();
                    this.AddNotification("Request Device No : " + request.RequestDeviceNo + " Success To Finish", NotificationType.SUCCESS);
                }
                else
                {
                    this.AddNotification("You don't have access to finish", NotificationType.ERROR);
                }
            }
            else
            {
                this.AddNotification("Request Device not found", NotificationType.ERROR);
            }


            return RedirectToAction(MVC.Request.Device("", "", 1, "", "", "", "-"));
        }
        #endregion
    }
}
