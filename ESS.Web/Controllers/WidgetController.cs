﻿using ESS.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ESS.Web.Controllers
{
    public partial class WidgetController : Controller
    {
        // GET: Widget
        public virtual PartialViewResult TopBar()
        {
            return PartialView();
        }

        public virtual PartialViewResult Sidebar()
        {
            return PartialView();
        }

        public virtual PartialViewResult MainMenu()
        {
            var models = new List<MenuModel>();

            models.AddRange(
                new List<MenuModel>() {
                    new MenuModel()
                    {
                        NameMenu = "Dashboard",
                        Url = Url.Action(MVC.Home.Index()),
                        IsHeading = true
                    },

                    new MenuModel()
                    {
                        NameMenu = "Information",
                        Url = @Url.Action(MVC.Home.Index()),
                        ControllerName = "Home",
                        ActionName = "Index",

                        IconType = "icon-w",
                        Icon = "os-icon os-icon-layout"
                    },
                });


            models.AddRange(
                new List<MenuModel>() {
                    new MenuModel()
                    {
                        NameMenu = "IT Service",
                        IsHeading = true
                    },

                    new MenuModel()
                    {
                        NameMenu = "Request Access",
                        Url = @Url.Action(MVC.Request.Access()),
                        ControllerName = "Request",
                        ActionName = "Access|AccessDetail",

                        IconType = "icon-w",
                        Icon = "ti-unlock",

                        ChildsMenus = new List<MenuModel>(){

                            new MenuModel(){
                                NameMenu = "Entry Request",
                                Url = @Url.Action(MVC.Request.AccessDetail(Guid.Empty)),
                                ControllerName = "Request",
                                ActionName = "AccessDetail",
                                Icon = "fa fa-pencil-square-o"
                            },

                            new MenuModel(){
                                NameMenu = "List Request",
                                Url = @Url.Action(MVC.Request.Access("","",1,"","","","-")),
                                ControllerName = "Request",
                                ActionName = "Access",
                                Icon = "fa fa-pencil-square-o"
                            },

                        }
                    },

                    new MenuModel()
                    {
                        NameMenu = "Request Device",
                        Url = @Url.Action(MVC.Request.Device()),
                        ControllerName = "Request",
                        ActionName = "Device|DeviceDetail",

                        IconType = "icon-w",
                        Icon = "ti-desktop",

                        ChildsMenus = new List<MenuModel>(){

                            new MenuModel(){
                                NameMenu = "Entry Request",
                                Url = @Url.Action(MVC.Request.DeviceDetail(Guid.Empty)),
                                ControllerName = "Request",
                                ActionName = "DeviceDetail",
                            },

                            new MenuModel(){
                                NameMenu = "List Request",
                                Url = @Url.Action(MVC.Request.Device("","",1,"","","","-")),
                                ControllerName = "Request",
                                ActionName = "Device",
                            },

                        }
                    },

                    new MenuModel()
                    {
                        NameMenu = "Change Request",
                        Url = @Url.Action(MVC.ChangeRequest.Index()),
                        ControllerName = "ChangeRequest",
                        ActionName = "Index|Detail",

                        IconType = "icon-w",
                        Icon = "ti-write",

                        ChildsMenus = new List<MenuModel>(){

                            new MenuModel(){
                                NameMenu = "Entry Change Request",
                                Url = @Url.Action(MVC.ChangeRequest.Detail(Guid.Empty)),
                                ControllerName = "ChangeRequest",
                                ActionName = "Detail",
                            },

                            new MenuModel(){
                                NameMenu = "List Change Request",
                                Url = @Url.Action(MVC.ChangeRequest.Index("","",1,"","","","-")),
                                ControllerName = "ChangeRequest",
                                ActionName = "Index",
                            },

                        }
                    },


                });


            ViewData.Model = models;
            return PartialView();
        }

        public virtual PartialViewResult MobileMenu()
        {
            return PartialView();
        }
    }
}