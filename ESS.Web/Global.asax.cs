﻿using ESS.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace ESS.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class HttpDataContextDataStore : IDataRepositoryStore
    {
        public object this[string key]
        {
            get { return HttpContext.Current.Items[key]; }
            set { HttpContext.Current.Items[key] = value; }
        }
    }

    public class HttpDataContextDataITS : IDataRepositoryITS
    {
        public object this[string key]
        {
            get { return HttpContext.Current.Items[key]; }
            set { HttpContext.Current.Items[key] = value; }
        }
    }

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            DataRepositoryStore.CurrentDataStore = new HttpDataContextDataStore();
            DataRepositoryITS.CurrentDataStore = new HttpDataContextDataITS();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}