﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using ESS.Common;
using System.Web.Routing;
using ESS.Data.Model;
using ESS.BusinessLogic;
using System.Text;
using ESS.Common;

namespace ESS.Web
{
    public class ESSAuthorizationAttribute : AuthorizeAttribute
    {
        private AuthorizationContext _currentAuthContext;
        public string ROLE_CODE { get; set; }
        public bool IS_AJAX { get; set; }

        ///// <summary>
        ///// Called when a process requests authorization.
        ///// </summary>
        ///// <param name="filterContext">The filter context, which encapsulates information for using <see cref="T:System.Web.Mvc.AuthorizeAttribute" />.</param>
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            _currentAuthContext = filterContext;
            var userModel = UserLogic.GetCurrentUser();

            if (userModel != null && !string.IsNullOrEmpty(userModel.NIKSite))
            {
                if (!string.IsNullOrEmpty(ROLE_CODE))
                {
                    if(userModel.RoleAccess != ROLE.ADMINISTRATOR)
                    {
                        if(userModel.RoleAccess != Roles)
                        {
                            if (IS_AJAX)
                            {
                                RedirectUnauthorisePageAjax();
                            }
                            else
                            {
                                RedirectToUnauthorisePage();
                            }
                        }
                    }

                }
            }
            else
            {
                if (IS_AJAX)
                {
                    RedirectUnauthorisePageAjax();
                }
                else
                {
                    RedirectToLoginPage();
                }
            }

        }

        #region Private Methods
        private class Http401Result : ActionResult
        {
            public override void ExecuteResult(ControllerContext context)
            {
                context.HttpContext.Response.ContentType = "application/json";
                ResponseModel response = new ResponseModel();
                response.SetError("401 Unauthorized");

                byte[] data = Encoding.UTF8.GetBytes(Newtonsoft.Json.JsonConvert.SerializeObject(response));
                context.HttpContext.Response.StatusCode = 401;
                //context.HttpContext.Response.Write("User login timeout, login again");
                context.HttpContext.Response.BinaryWrite(data);
                context.HttpContext.Response.End();
            }
        }

        /// <summary>
        /// Redirects to unauthorise page.
        /// </summary>
        private void RedirectToUnauthorisePage()
        {
            var redirectResult = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Error", action = "AccessDenied" }));
            _currentAuthContext.Result = redirectResult;
        }

        /// <summary>
        /// Redirects to login page.
        /// </summary>
        private void RedirectToLoginPage()
        {
            //var returnUrl = _currentAuthContext.HttpContext.Request.Url.AbsolutePath;
            //var redirectResult = new RedirectToRouteResult(new RouteValueDictionary(new { Areas = "", controller = "User", action = "Login", returnUrl = returnUrl }));

            var returnUrl = _currentAuthContext.HttpContext.Request.Url.AbsolutePath;
            string returnURL = HttpContext.Current.Request.RawUrl;
            var redirectResult = new RedirectToRouteResult(new RouteValueDictionary
                       {
                           { "action", "Login" },
                           { "controller", "User" },
                           { "Area", String.Empty },
                           { "ReturnUrl", returnURL }
                       });

            _currentAuthContext.Result = redirectResult;
        }

        private void RedirectUnauthorisePageAjax()
        {
            _currentAuthContext.Result = new Http401Result();
        }



        #endregion

    }
}