﻿using ESS.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ESS.Common;
using System.Web.Mvc;
using System.Text;

namespace ESS.Web
{
    public class FunctionWebHelpers
    {
        public static List<MenuModel> GetAllMenu()
        {
            var menus = new List<MenuModel>();

            return menus;
        }

        public static string GetErrorModelState(ModelStateDictionary ModelState)
        {
            var sbMessage = new StringBuilder();

            foreach (ModelState modelState in ModelState.Values)
            {
                foreach (ModelError error in modelState.Errors)
                {
                    sbMessage.AppendLine(error.ErrorMessage);
                }
            }

            return sbMessage.ToString();
        }

        public static SearchModel GetQueryString(System.Collections.Specialized.NameValueCollection Request)
        {
            var search = new Data.Model.SearchModel();

            if (Request != null)
            {
                search.SearchCriteria = string.IsNullOrEmpty(Request["SearchCriteria"]) ? string.Empty : Request["SearchCriteria"];
                search.MoreCriteria = string.IsNullOrEmpty(Request["MoreCriteria"]) ? string.Empty : Request["MoreCriteria"];

                if (!string.IsNullOrEmpty(Request["DateFrom"]))
                {
                    DateTime dateFrom = DateTime.Now;
                    if (DateTime.TryParse(Request["DateFrom"], out dateFrom))
                    {
                        search.DateFrom = DateHelper.DateFromMinimun(dateFrom);
                    }

                    if (!search.DateFrom.HasValue)
                    {
                        dateFrom = CommonFunction.DateTimeESS(Request["DateFrom"].ToString());
                        if (dateFrom != DateTime.MinValue)
                        {
                            search.DateFrom = dateFrom;
                        }
                    }
                }

                if (!string.IsNullOrEmpty(Request["DateTo"]))
                {
                    DateTime dateTo = DateTime.Now;
                    if (DateTime.TryParse(Request["DateTo"], out dateTo))
                    {
                        search.DateTo = DateHelper.DateToMax(dateTo);
                    }

                    if (!search.DateTo.HasValue)
                    {
                        dateTo = CommonFunction.DateTimeESS(Request["DateTo"].ToString());
                        if (dateTo != DateTime.MinValue)
                        {
                            search.DateTo = dateTo;
                        }
                    }
                }

                if (!string.IsNullOrEmpty(Request["CurrentPage"]))
                {
                    int currentPage = 0;
                    int.TryParse(Request["CurrentPage"].ToString(), out currentPage);
                    search.CurrentPage = currentPage;
                }

                search.OrderBy = string.IsNullOrEmpty(Request["grid-column"]) ? string.Empty : Request["grid-column"];
                search.IsOrderByDesc = string.IsNullOrEmpty(Request["grid-dir"]) ? false : (Request["grid-dir"] == "1" ? true : false);
            }

            return search;
        }

        public static List<MenuModel> SetCurrentMenu(List<MenuModel> models)
        {
            // SET CURRENT URL
            var routeValues = HttpContext.Current.Request.RequestContext.RouteData.Values;
            string controllerName = "-";
            string actionName = "-";

            var roleCookies = CookieManager.Get(COOKIES_NAME.ROLE);
            UserModel usermodel = new UserModel();


            if (routeValues.ContainsKey("controller"))
            {
                controllerName = (string)routeValues["controller"];
            }

            if (routeValues.ContainsKey("action"))
            {
                actionName = (string)routeValues["action"];
            }

            foreach (var model in models)
            {
                var IsCurrentParent = (model.ControllerName == controllerName && model.ActionName == actionName);

                if (model.ChildsMenus != null && model.ChildsMenus.Count > 0)
                {
                    foreach (var itemChild in model.ChildsMenus)
                    {
                        var actionNames = itemChild.ActionName.Split('|').ToList();

                        var IsCurrentChild = (itemChild.ControllerName == controllerName && actionNames.Contains(actionName));
                        if (IsCurrentChild)
                        {
                            IsCurrentParent = true;
                        }

                        itemChild.IsCurrent = IsCurrentChild;
                    }
                }

                model.IsCurrent = IsCurrentParent;
            }

            return models;
        }

        public class BreadCrumbClass
        {
            public string Url { get; set; }
            public string Label { get; set; }
        }

        public static List<BreadCrumbClass> BreadCrumbs(BreadCrumbClass url1, BreadCrumbClass url2 = null, BreadCrumbClass url3 = null)
        {
            var breadList = new List<BreadCrumbClass>();
            if (url1 != null)
            {
                breadList.Add(url1);
            }

            if (url2 != null)
            {
                breadList.Add(url2);
            }

            if (url3 != null)
            {
                breadList.Add(url3);
            }

            return breadList;
        }

        public static string ActionNo(string label)
        {
            return string.Format("<div class=\"success\"></div><span style=\"margin-left: 5px;\">{0}</span>", label);
        }

        public static string ActionHref(string url, string label)
        {
            return string.Format("<a href=\"{0}\">{1}</a>", url, label);
        }

        public static string ActionView(string url)
        {
            return string.Format("<a href=\"{0}\" class=\"btn default btn-xs\"><i class=\"fa fa-search-plus\"></i>View</a>", url);
        }

        public static string ActionEdit(string url)
        {
            return string.Format("<a href=\"{0}\" class=\"btn default btn-xs purple\"><i class=\"fa fa-edit\"></i>Edit</a>", url);
        }

        public static string ActionDelete(string url)
        {
            return string.Format("<a href=\"{0}\" class=\"btn default btn-xs black\"><i class=\"fa fa-trash-o\"></i>Delete</a>", url);
        }

        public static string ActionViewModal(string url, string label)
        {
            return string.Format("<div class=\"success\"></div><a href=\"{0}\" data-target=\"#ajax\" data-toggle=\"modal\">{1}</a>", url, label);
        }

        public static string ActionViewModal(string url)
        {
            return string.Format("<a href=\"{0}\" class=\"btn green btn-xs\" data-target=\"#ajax\" data-toggle=\"modal\"><i class=\"fa fa-check-square-o\"></i> Action</a>", url);
        }


    }
}