﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ESS.BusinessLogic;
using ESS.Common;
using ESS.Data;
using ESS.Data.Model;
using Microsoft.Reporting.WebForms;

namespace ESS.Web
{
    public partial class PrintForm : System.Web.UI.Page
    {


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                String FormType = string.Empty;
                if (Request.QueryString["FormType"] != null && !string.IsNullOrEmpty(Request.QueryString["FormType"]))
                {
                    FormType = Request.QueryString["FormType"];
                }

                Guid DocumentId = Guid.Empty;
                if (Request.QueryString["id"] != null && !string.IsNullOrEmpty(Request.QueryString["id"]))
                {
                    string id = Request.QueryString["id"];
                    Guid.TryParse(id, out DocumentId);
                }

                switch (FormType)
                {
                    case "ITAR":
                        ITAR(DocumentId);
                        break;
                    case "ITDR":
                        ITDR(DocumentId);
                        break;
                    case "ITCR":
                        ITCR(DocumentId);
                        break;
                    case "DC":
                        ITARDC(DocumentId);
                        break;
                }



            }


        }

        public void ITAR(Guid DocumentId)
        {
            RequestAccess model = RequestAccess.GetByID(DocumentId);
            RptViewer.LocalReport.ReportPath = "Reports/ITARPrint.rdlc";
            RptViewer.LocalReport.DataSources.Clear();

            if (model.CreatedDate.Year < 2020)
            {
                RptViewer.LocalReport.ReportPath = "Reports/OLD/ITARPrint.rdlc";
            }            

            ESSDataSet.DSITARDataTable TableHeader = new ESSDataSet.DSITARDataTable();

            if (model != null)
            {

                var UserCreated = UserLogic.GetByNIK(model.NIKSite);

                var userApproval = UserLogic.GetUserApproval(model.NIKSite);
                string Manager = string.Empty;
                string ManagerTitle = string.Empty;
                string VP = string.Empty;
                string VPTitle = string.Empty;
                string ManagerIT = userApproval.ManagerIT.FullName;
                string ManagerITTitle = userApproval.ManagerIT.Jabatan;
                string VPIT = userApproval.VPIT.FullName;
                string VPITTitle = userApproval.VPIT.Jabatan;

                if (userApproval.Manager != null && !String.IsNullOrEmpty(userApproval.Manager.FullName))
                {
                    Manager = userApproval.Manager.FullName;
                    ManagerTitle = (!string.IsNullOrEmpty(userApproval.Manager.Jabatan) ? userApproval.Manager.Jabatan : string.Empty);
                }

                if (userApproval.VP != null && !String.IsNullOrEmpty(userApproval.VP.FullName))
                {
                    VP = userApproval.VP.FullName;
                    VPTitle = (!string.IsNullOrEmpty(userApproval.VP.Jabatan) ? userApproval.VP.Jabatan : string.Empty);
                }

                string requestorCheck = string.Empty;
                string dtRequestor = string.Empty;
                string dtManager = string.Empty;
                string dtVP = string.Empty;
                string dtManagerIT = string.Empty;
                string dtVPIT = string.Empty;

                var logs = CurrentDataContext.CurrentContext.RequestAccess_logWF.Where(x => x.transid == model.RequestAccessNo).ToList();
                foreach (var item in logs)
                {
                    if (item.StatusWF == "INT" && item.datex.HasValue)
                    {
                        requestorCheck = "1";
                        dtRequestor = item.datex.Value.ESSDate();
                    }

                    if (item.datex.HasValue && (item .picAction == 1 || item .picAction == 2))
                    {
                        if (item.picUserRole == "MGR")
                        {
                            dtManager = item.datex.Value.ESSDate();
                        }

                        if (item.picUserRole == "VP")
                        {
                            dtVP = item.datex.Value.ESSDate();
                        }

                        if (item.picUserRole == "MGRIT")
                        {
                            dtManagerIT = item.datex.Value.ESSDate();
                        }

                        if (item.picUserRole == "VPIT")
                        {
                            dtVPIT = item.datex.Value.ESSDate();
                        }
                    }
                }

                TableHeader.AddDSITARRow(
                    model.RequestAccessNo,
                    UserCreated.CompanyName,
                    UserCreated.DepartmentName,
                    model.RequestDate.ToString(Constant.FORMAT_DATE_JRESOURCES),
                    UserCreated.FullName,
                    UserCreated.NIKSite,
                    UserCreated.Email,
                    (model.IsEmail ? "X":""),
                    (model.IsUSBAccess ? "X" : ""),
                    (model.IsAccpacAccess ? "X" : ""),
                    (model.IsVPNAccess ? "X" : ""),
                    (model.IsEmailExtended ? "X" : ""),
                    (model.IsInternetAccess ? "X" : ""),
                    (model.IsSharepointAccess ? "X" : ""),
                    (model.IsEmailDomain ? "X" : ""),
                    (model.IsMSRAccess ? "X" : ""),
                    (model.IsOther ? "X" : ""),
                    model.RequestDesc,
                    Manager,
                    ManagerTitle,
                    VP,
                    VPTitle,
                    ManagerIT,
                    ManagerITTitle,
                    VPIT,
                    VPITTitle,
                    model.fsup_nik.ToString(),
                    model.fapprove_nik.ToString(),
                    model.freview_nik.ToString(),
                    model.fproceed_nik.ToString(),

                    requestorCheck,
                    dtRequestor,
                    dtManager,
                    dtVP,
                    dtManagerIT,
                    dtVPIT
                );

                RptViewer.LocalReport.DataSources.Add(new ReportDataSource("DSITAR", TableHeader as object));
                
                RptViewer.DataBind();
                RptViewer.LocalReport.Refresh();
            }
        }

        public void ITARDC(Guid DocumentId)
        {
            RequestAccess model = RequestAccess.GetByID(DocumentId);
            RptViewer.LocalReport.ReportPath = "Reports/DCPrint.rdlc";
            RptViewer.LocalReport.DataSources.Clear();

            ESSDataSet.DSITARDCDataTable TableHeader = new ESSDataSet.DSITARDCDataTable();

            if (model != null)
            {

                var UserCreated = UserLogic.GetByNIK(model.NIKSite);

                var userApproval = UserLogic.GetUserApprovalAccessDocCenter(model.NIKSite);
                var suser = V_ITAS_EMP.GetByNIKSite(model.NIKSite);
                var suserappoint = V_ITAS_EMP.GetByNIKSite(model.AppointmentNIKSite);

                string Manager = string.Empty;
                string ManagerTitle = string.Empty;
                string VP = string.Empty;
                string VPTitle = string.Empty;
                string ManagerIT = userApproval.ManagerIT.FullName;
                string ManagerITTitle = userApproval.ManagerIT.Jabatan;
                string VPIT = userApproval.VPIT.FullName;
                string VPITTitle = userApproval.VPIT.Jabatan;
                string UserTitle = suser.nmJabat;
                string UserNameAppoint = suserappoint.Nama;
                string userTitleAppoint = suserappoint.nmJabat;
                string AppointDate = model.AppointmentDate.ToString();


                if (userApproval.Manager != null && !String.IsNullOrEmpty(userApproval.Manager.FullName))
                {
                    Manager = userApproval.Manager.FullName;
                    ManagerTitle = (!string.IsNullOrEmpty(userApproval.Manager.Jabatan) ? userApproval.Manager.Jabatan : string.Empty);
                }

                if (userApproval.VP != null && !String.IsNullOrEmpty(userApproval.VP.FullName))
                {
                    VP = userApproval.ITAdmin.FullName;
                    VPTitle = (!string.IsNullOrEmpty(userApproval.ITAdmin.Jabatan) ? userApproval.ITAdmin.Jabatan : string.Empty);
                }

                string requestorCheck = string.Empty;
                string dtRequestor = string.Empty;
                string dtManager = string.Empty;
                string dtVP = string.Empty;
                string dtManagerIT = string.Empty;
                string dtVPIT = string.Empty;
                string ManagerCheck = string.Empty;
                string VPCheck = string.Empty;
                string ManagerITCheck = string.Empty;
                string VPITCheck = string.Empty;

                var logs = CurrentDataContext.CurrentContext.RequestAccess_logWF.Where(x => x.transid == model.RequestAccessNo).ToList();
                foreach (var item in logs)
                {
                    if (item.StatusWF == "INT" && item.datex.HasValue)
                    {
                        requestorCheck = "1";
                        dtRequestor = item.datex.Value.ESSDate();
                    }

                    if (item.datex.HasValue && (item.picAction == 1 || item.picAction == 2))
                    {
                        if (item.picUserRole == "MGR")
                        {
                            dtManager = item.datex.Value.ESSDate();
                            ManagerCheck = item.picAction.ToString();
                        }

                        if (item.picUserRole == "VP")
                        {
                            dtVP = item.datex.Value.ESSDate();
                            VPCheck = item.picAction.ToString();
                        }

                        if (item.picUserRole == "MGRIT")
                        {
                            dtManagerIT = item.datex.Value.ESSDate();
                            ManagerITCheck = item.picAction.ToString();
                        }

                        if (item.picUserRole == "VPIT")
                        {
                            dtVPIT = item.datex.Value.ESSDate();
                            VPITCheck = item.picAction.ToString();
                        }
                    }
                }

                TableHeader.AddDSITARDCRow(
                    model.RequestAccessNo,
                    UserCreated.CompanyCode,
                    UserCreated.DepartmentName,
                    model.RequestDate.ToString(Constant.FORMAT_DATE_JRESOURCES),
                    UserCreated.FullName,
                    UserCreated.NIKSite,
                    UserCreated.Email,
                    (model.IsEmail ? "X" : ""),
                    (model.IsUSBAccess ? "X" : ""),
                    (model.IsAccpacAccess ? "X" : ""),
                    (model.IsVPNAccess ? "X" : ""),
                    (model.IsEmailExtended ? "X" : ""),
                    (model.IsInternetAccess ? "X" : ""),
                    (model.IsSharepointAccess ? "X" : ""),
                    (model.IsEmailDomain ? "X" : ""),
                    (model.IsMSRAccess ? "X" : ""),
                    (model.IsOther ? "X" : ""),
                    model.RequestDesc,
                    Manager,
                    ManagerTitle,
                    VP,
                    VPTitle,
                    ManagerIT,
                    ManagerITTitle,
                    VPIT,
                    VPITTitle,
                    ManagerCheck,
                    VPCheck,
                    ManagerITCheck,
                    VPITCheck,

                    requestorCheck,
                    dtRequestor,
                    dtManager,
                    dtVP,
                    dtManagerIT,
                    dtVPIT,
                    UserTitle,
                    UserNameAppoint,
                    userTitleAppoint,
                    AppointDate
                );

                RptViewer.LocalReport.DataSources.Add(new ReportDataSource("DSITARDC", TableHeader as object));

                RptViewer.DataBind();
                RptViewer.LocalReport.Refresh();
            }
        }

        public void ITDR(Guid DocumentId)
        {
            RequestDevice model = RequestDevice.GetByID(DocumentId);
            RptViewer.LocalReport.ReportPath = "Reports/ITDRPrint.rdlc";
            RptViewer.LocalReport.DataSources.Clear();

            if (model.CreatedDate.Year < 2020)
            {
                RptViewer.LocalReport.ReportPath = "Reports/OLD/ITDRPrint.rdlc";
            }

            ESSDataSet.DSITDRDataTable TableHeader = new ESSDataSet.DSITDRDataTable();
            ESSDataSet.DSITDRDetailDataTable TableDetail = new ESSDataSet.DSITDRDetailDataTable();

            if (model != null)
            {

                var UserCreated = UserLogic.GetByNIK(model.NIKSite);

                var userApproval = UserLogic.GetUserApproval(model.NIKSite);
                string Manager = string.Empty;
                string ManagerTitle = string.Empty;
                string VP = string.Empty;
                string VPTitle = string.Empty;
                string ManagerIT = userApproval.ManagerIT.FullName;
                string ManagerITTitle = userApproval.ManagerIT.Jabatan;
                string VPIT = userApproval.VPIT.FullName;
                string VPITTitle = userApproval.VPIT.Jabatan;

                if (userApproval.Manager != null && !String.IsNullOrEmpty(userApproval.Manager.FullName))
                {
                    Manager = userApproval.Manager.FullName;
                    ManagerTitle = userApproval.Manager.Jabatan;
                }

                if (userApproval.VP != null && !String.IsNullOrEmpty(userApproval.VP.FullName))
                {
                    VP = userApproval.VP.FullName;
                    VPTitle = userApproval.VP.Jabatan;
                }

                string requestorCheck = string.Empty;
                string dtRequestor = string.Empty;
                string dtManager = string.Empty;
                string dtVP = string.Empty;
                string dtManagerIT = string.Empty;
                string dtVPIT = string.Empty;

                var logs = CurrentDataContext.CurrentContext.RequestDevice_logWF.Where(x => x.transid == model.RequestDeviceNo).ToList();
                foreach (var item in logs)
                {
                    if (item.StatusWF == "INT" && item.datex.HasValue)
                    {
                        requestorCheck = "1";
                        dtRequestor = item.datex.Value.ESSDate();
                    }

                    if (item.datex.HasValue && (item.picAction == 1 || item.picAction == 2))
                    {
                        if (item.picUserRole == "MGR")
                        {
                            dtManager = item.datex.Value.ESSDate();
                        }

                        if (item.picUserRole == "VP")
                        {
                            dtVP = item.datex.Value.ESSDate();
                        }

                        if (item.picUserRole == "MGRIT")
                        {
                            dtManagerIT = item.datex.Value.ESSDate();
                        }

                        if (item.picUserRole == "VPIT")
                        {
                            dtVPIT = item.datex.Value.ESSDate();
                        }
                    }
                }

                TableHeader.AddDSITDRRow(
                    model.RequestDeviceNo,
                    UserCreated.CompanyName,
                    UserCreated.DepartmentName,
                    model.RequestDate.ToString(Constant.FORMAT_DATE_JRESOURCES),
                    model.RequestDesc,
                    Manager,
                    ManagerTitle,
                    VP,
                    VPTitle,
                    ManagerIT,
                    ManagerITTitle,
                    VPIT,
                    VPITTitle,
                    UserCreated.FullName,
                    UserCreated.NIKSite,
                    UserCreated.Email,
                    model.fsup_nik.ToString(),
                    model.fapprove_nik.ToString(),
                    model.freview_nik.ToString(),
                    model.fproceed_nik.ToString(),
                    requestorCheck,
                    dtRequestor,
                    dtManager,
                    dtVP,
                    dtManagerIT,
                    dtVPIT
                );

                int i = 1;
                foreach (var item in model.RequestDeviceItems)
                {
                    TableDetail.AddDSITDRDetailRow(i.ToString(),
                        item.ItemName,
                        item.Qty.ToString(),
                        item.ItemStatus,
                        item.NoteAdmin);
                    i++;
                }

                RptViewer.LocalReport.DataSources.Add(new ReportDataSource("DSITDR", TableHeader as object));
                RptViewer.LocalReport.DataSources.Add(new ReportDataSource("DSITDRDetail", TableDetail as object));

                RptViewer.DataBind();
                RptViewer.LocalReport.Refresh();
            }
        }

        public void ITCR(Guid DocumentId)
        {
            ChangeRequestModel model = ChangeRequestLogic.GetById(DocumentId);
            ESSDataSet.DSITCRDataTable TableHeader = new ESSDataSet.DSITCRDataTable();
            RptViewer.LocalReport.ReportPath = "Reports/ITCRPrint.rdlc";
            RptViewer.LocalReport.DataSources.Clear();

            if (model.CreatedDate.Year < 2020)
            {
                RptViewer.LocalReport.ReportPath = "Reports/OLD/ITCRPrint.rdlc";
            }

            if (model != null)
            {
                var logs = CurrentDataContext.CurrentContext.ChangeRequest_logWF.Where(x => x.transid == model.ChangeRequestNo).ToList();

                string requestorCheck = string.Empty;
                string dtRequestor = string.Empty;
                

                #region Log 
                foreach (var item in logs)
                {
                    if (item.StatusWF == "INT" && item.datex.HasValue)
                    {
                        requestorCheck = "1";
                        dtRequestor = item.datex.Value.ESSDate();
                    }

                    if (item.datex.HasValue && (item.picAction == 1 || item.picAction == 2))
                    {
                        switch (item.picUserRole)
                        {
                            case "MGR":
                                model.UserApproval.ManagerCheck = item.picAction.Value;
                                model.UserApproval.ManagerCheckDate = item.datex.Value.ESSDate();
                                break;

                            case "GMSITE":
                                model.UserApproval.GeneralManagerCheck = item.picAction.Value;
                                model.UserApproval.GeneralManagerCheckDate = item.datex.Value.ESSDate();
                                break;

                            case "OWNAPP":
                                model.UserApproval.OwnerAppCheck = item.picAction.Value;
                                model.UserApproval.OwnerAppCheckDate = item.datex.Value.ESSDate();
                                break;

                            case "GMHO":
                                model.UserApproval.GMHOCheck = item.picAction.Value;
                                model.UserApproval.GMHOCheckDate = item.datex.Value.ESSDate();
                                break;

                            case "VPHO":
                                model.UserApproval.VPHOCheck = item.picAction.Value;
                                model.UserApproval.VPHOCheckDate = item.datex.Value.ESSDate();
                                break;

                            case "ITAPP":
                                model.UserApproval.ITAppCheck = item.picAction.Value;
                                model.UserApproval.ITAppCheckDate = item.datex.Value.ESSDate();
                                break;

                            case "MGRIT":
                                model.UserApproval.ManagerITCheck = item.picAction.Value;
                                model.UserApproval.ManagerITCheckDate = item.datex.Value.ESSDate();
                                break;

                            case "VPIT":
                                model.UserApproval.VPITCheck = item.picAction.Value;
                                model.UserApproval.VPITCheckDate = item.datex.Value.ESSDate();
                                break;
                        }
                    }
                }
                #endregion

                TableHeader.AddDSITCRRow(
                    model.ChangeRequestNo,
                    model.ApplicationName,
                    model.ModuleName,
                    model.ProgramManager,
                    model.CreatedBy.FullName,
                    model.CreatedBy.NIKSite,
                    model.CreatedBy.Email,
                    model.CreatedDateStr,
                    model.DueDateStr,
                    model.Reason,
                    model.DescriptionChange,
                    model.DevelopmentDescription,
                    model.LevelDevelopment,
                    model.DateStartStr,
                    model.DateFinishPlanningStr,
                    model.DateFinishStr,

                    (model.UserApproval.Manager.FullName ?? "- - - - -"),
                    (model.UserApproval.Manager.Jabatan ?? "- - - - -"),
                    (model.UserApproval.GeneralManager.FullName ?? "- - - - -"),
                    (model.UserApproval.GeneralManager.Jabatan ?? "- - - - -"),
                    (model.UserApproval.OwnerApp.FullName ?? "- - - - -"),
                    (model.UserApproval.OwnerApp.Jabatan ?? "- - - - -"),
                    (model.UserApproval.GMHO.FullName ?? "- - - - -"),
                    (model.UserApproval.GMHO.Jabatan ?? "- - - - -"),
                    (model.UserApproval.VPHO.FullName ?? "- - - - -"),
                    (model.UserApproval.VPHO.Jabatan ?? "- - - - -"),
                    (model.UserApproval.ManagerIT.FullName ?? "- - - - -"),
                    (model.UserApproval.ManagerIT.Jabatan ?? "- - - - -"),
                    (model.UserApproval.VPIT.FullName ?? "- - - - -"),
                    (model.UserApproval.VPIT.Jabatan ?? "- - - - -"),
                    (model.UserApproval.ITApp.FullName ?? "- - - - -"),
                    (model.UserApproval.ITApp.Jabatan ?? "- - - - -"),

                    model.UserApproval.GeneralManager.ToString(),
                    model.UserApproval.OwnerAppCheck.ToString(),
                    model.UserApproval.GMHOCheck.ToString(),
                    model.UserApproval.VPHOCheck.ToString(),
                    model.UserApproval.ManagerITCheck.ToString(),
                    model.UserApproval.VPITCheck.ToString(),
                    model.UserApproval.ITAppCheck.ToString(),
                    requestorCheck,

                    dtRequestor,

                    (model.IsChangeMasterData ? "X" : ""),
                    (model.IsNewReport ? "X" : ""),
                    (model.IsChangeUpdateReport ? "X" : ""),
                    (model.IsNewInterfaceProgram ? "X" : ""),
                    (model.IsAuthorisation ? "X" : ""),
                    (model.IsApplication ? "X" : ""),

                    model.UserApproval.GeneralManagerCheckDate,
                    model.UserApproval.OwnerAppCheckDate,
                    model.UserApproval.GMHOCheckDate,
                    model.UserApproval.VPHOCheckDate,
                    model.UserApproval.ManagerITCheckDate,
                    model.UserApproval.VPITCheckDate,
                    model.UserApproval.ITAppCheckDate,
                    
                    model.UserApproval.ManagerCheck.ToString(),
                    model.UserApproval.ManagerCheckDate);
                    

                RptViewer.LocalReport.DataSources.Add(new ReportDataSource("DSITCR", TableHeader as object));

                RptViewer.DataBind();
                RptViewer.LocalReport.Refresh();
            }

        }



    }
}