// <auto-generated />
// This file was generated by a T4 template.
// Don't change it directly as your change would get overwritten.  Instead, make changes
// to the .tt file (i.e. the T4 template) and save it to regenerate this file.

// Make sure the compiler doesn't complain about missing Xml comments and CLS compliance
// 0108: suppress "Foo hides inherited member Foo. Use the new keyword if hiding was intended." when a controller and its abstract parent are both processed
// 0114: suppress "Foo.BarController.Baz()' hides inherited member 'Qux.BarController.Baz()'. To make the current member override that implementation, add the override keyword. Otherwise add the new keyword." when an action (with an argument) overrides an action in a parent controller
#pragma warning disable 1591, 3008, 3009, 0108, 0114
#region T4MVC

using System;
using System.Diagnostics;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Mvc.Html;
using System.Web.Routing;
using T4MVC;
namespace ESS.Web.Controllers
{
    public partial class RequestController
    {
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RequestController(Dummy d) { }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(ActionResult result)
        {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoute(callInfo.RouteValueDictionary);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(Task<ActionResult> taskResult)
        {
            return RedirectToAction(taskResult.Result);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToActionPermanent(ActionResult result)
        {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoutePermanent(callInfo.RouteValueDictionary);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToActionPermanent(Task<ActionResult> taskResult)
        {
            return RedirectToActionPermanent(taskResult.Result);
        }

        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult AccessDetail()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.AccessDetail);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult SubmitRequestAccess()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.SubmitRequestAccess);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult FinishRequestAccess()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.FinishRequestAccess);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult DeviceDetail()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.DeviceDetail);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult SubmitRequestDevice()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.SubmitRequestDevice);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult FinishRequestDevice()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.FinishRequestDevice);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public RequestController Actions { get { return MVC.Request; } }
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Area = "";
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Name = "Request";
        [GeneratedCode("T4MVC", "2.0")]
        public const string NameConst = "Request";
        [GeneratedCode("T4MVC", "2.0")]
        static readonly ActionNamesClass s_actions = new ActionNamesClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionNamesClass ActionNames { get { return s_actions; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNamesClass
        {
            public readonly string Access = "Access";
            public readonly string AccessDetail = "AccessDetail";
            public readonly string SubmitRequestAccess = "SubmitRequestAccess";
            public readonly string FinishRequestAccess = "FinishRequestAccess";
            public readonly string Device = "Device";
            public readonly string DeviceDetail = "DeviceDetail";
            public readonly string SubmitRequestDevice = "SubmitRequestDevice";
            public readonly string FinishRequestDevice = "FinishRequestDevice";
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNameConstants
        {
            public const string Access = "Access";
            public const string AccessDetail = "AccessDetail";
            public const string SubmitRequestAccess = "SubmitRequestAccess";
            public const string FinishRequestAccess = "FinishRequestAccess";
            public const string Device = "Device";
            public const string DeviceDetail = "DeviceDetail";
            public const string SubmitRequestDevice = "SubmitRequestDevice";
            public const string FinishRequestDevice = "FinishRequestDevice";
        }


        static readonly ActionParamsClass_Access s_params_Access = new ActionParamsClass_Access();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_Access AccessParams { get { return s_params_Access; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_Access
        {
            public readonly string RequestAccessNo = "RequestAccessNo";
            public readonly string Notes = "Notes";
            public readonly string CurrentPage = "CurrentPage";
            public readonly string FullName = "FullName";
            public readonly string NIKSite = "NIKSite";
            public readonly string Site = "Site";
            public readonly string Status = "Status";
        }
        static readonly ActionParamsClass_AccessDetail s_params_AccessDetail = new ActionParamsClass_AccessDetail();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_AccessDetail AccessDetailParams { get { return s_params_AccessDetail; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_AccessDetail
        {
            public readonly string Id = "Id";
            public readonly string Model = "Model";
        }
        static readonly ActionParamsClass_SubmitRequestAccess s_params_SubmitRequestAccess = new ActionParamsClass_SubmitRequestAccess();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_SubmitRequestAccess SubmitRequestAccessParams { get { return s_params_SubmitRequestAccess; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_SubmitRequestAccess
        {
            public readonly string Id = "Id";
        }
        static readonly ActionParamsClass_FinishRequestAccess s_params_FinishRequestAccess = new ActionParamsClass_FinishRequestAccess();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_FinishRequestAccess FinishRequestAccessParams { get { return s_params_FinishRequestAccess; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_FinishRequestAccess
        {
            public readonly string Id = "Id";
        }
        static readonly ActionParamsClass_Device s_params_Device = new ActionParamsClass_Device();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_Device DeviceParams { get { return s_params_Device; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_Device
        {
            public readonly string RequestDeviceNo = "RequestDeviceNo";
            public readonly string Notes = "Notes";
            public readonly string CurrentPage = "CurrentPage";
            public readonly string FullName = "FullName";
            public readonly string NIKSite = "NIKSite";
            public readonly string Site = "Site";
            public readonly string Status = "Status";
        }
        static readonly ActionParamsClass_DeviceDetail s_params_DeviceDetail = new ActionParamsClass_DeviceDetail();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_DeviceDetail DeviceDetailParams { get { return s_params_DeviceDetail; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_DeviceDetail
        {
            public readonly string Id = "Id";
            public readonly string Model = "Model";
        }
        static readonly ActionParamsClass_SubmitRequestDevice s_params_SubmitRequestDevice = new ActionParamsClass_SubmitRequestDevice();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_SubmitRequestDevice SubmitRequestDeviceParams { get { return s_params_SubmitRequestDevice; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_SubmitRequestDevice
        {
            public readonly string Id = "Id";
        }
        static readonly ActionParamsClass_FinishRequestDevice s_params_FinishRequestDevice = new ActionParamsClass_FinishRequestDevice();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_FinishRequestDevice FinishRequestDeviceParams { get { return s_params_FinishRequestDevice; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_FinishRequestDevice
        {
            public readonly string Id = "Id";
        }
        static readonly ViewsClass s_views = new ViewsClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ViewsClass Views { get { return s_views; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ViewsClass
        {
            static readonly _ViewNamesClass s_ViewNames = new _ViewNamesClass();
            public _ViewNamesClass ViewNames { get { return s_ViewNames; } }
            public class _ViewNamesClass
            {
                public readonly string Access___Backup = "Access - Backup";
                public readonly string Access = "Access";
                public readonly string AccessDetail = "AccessDetail";
                public readonly string Device = "Device";
                public readonly string DeviceDetail = "DeviceDetail";
            }
            public readonly string Access___Backup = "~/Views/Request/Access - Backup.cshtml";
            public readonly string Access = "~/Views/Request/Access.cshtml";
            public readonly string AccessDetail = "~/Views/Request/AccessDetail.cshtml";
            public readonly string Device = "~/Views/Request/Device.cshtml";
            public readonly string DeviceDetail = "~/Views/Request/DeviceDetail.cshtml";
        }
    }

    [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
    public partial class T4MVC_RequestController : ESS.Web.Controllers.RequestController
    {
        public T4MVC_RequestController() : base(Dummy.Instance) { }

        [NonAction]
        partial void AccessOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string RequestAccessNo, string Notes, int CurrentPage, string FullName, string NIKSite, string Site, string Status);

        [NonAction]
        public override System.Web.Mvc.ActionResult Access(string RequestAccessNo, string Notes, int CurrentPage, string FullName, string NIKSite, string Site, string Status)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Access);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "RequestAccessNo", RequestAccessNo);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "Notes", Notes);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "CurrentPage", CurrentPage);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "FullName", FullName);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "NIKSite", NIKSite);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "Site", Site);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "Status", Status);
            AccessOverride(callInfo, RequestAccessNo, Notes, CurrentPage, FullName, NIKSite, Site, Status);
            return callInfo;
        }

        [NonAction]
        partial void AccessDetailOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, System.Guid Id);

        [NonAction]
        public override System.Web.Mvc.ActionResult AccessDetail(System.Guid Id)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.AccessDetail);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "Id", Id);
            AccessDetailOverride(callInfo, Id);
            return callInfo;
        }

        [NonAction]
        partial void AccessDetailOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, ESS.Data.Model.RequestAccessModel Model);

        [NonAction]
        public override System.Web.Mvc.ActionResult AccessDetail(ESS.Data.Model.RequestAccessModel Model)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.AccessDetail);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "Model", Model);
            AccessDetailOverride(callInfo, Model);
            return callInfo;
        }

        [NonAction]
        partial void SubmitRequestAccessOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, System.Guid Id);

        [NonAction]
        public override System.Web.Mvc.ActionResult SubmitRequestAccess(System.Guid Id)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.SubmitRequestAccess);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "Id", Id);
            SubmitRequestAccessOverride(callInfo, Id);
            return callInfo;
        }

        [NonAction]
        partial void FinishRequestAccessOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, System.Guid Id);

        [NonAction]
        public override System.Web.Mvc.ActionResult FinishRequestAccess(System.Guid Id)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.FinishRequestAccess);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "Id", Id);
            FinishRequestAccessOverride(callInfo, Id);
            return callInfo;
        }

        [NonAction]
        partial void DeviceOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string RequestDeviceNo, string Notes, int CurrentPage, string FullName, string NIKSite, string Site, string Status);

        [NonAction]
        public override System.Web.Mvc.ActionResult Device(string RequestDeviceNo, string Notes, int CurrentPage, string FullName, string NIKSite, string Site, string Status)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Device);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "RequestDeviceNo", RequestDeviceNo);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "Notes", Notes);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "CurrentPage", CurrentPage);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "FullName", FullName);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "NIKSite", NIKSite);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "Site", Site);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "Status", Status);
            DeviceOverride(callInfo, RequestDeviceNo, Notes, CurrentPage, FullName, NIKSite, Site, Status);
            return callInfo;
        }

        [NonAction]
        partial void DeviceDetailOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, System.Guid Id);

        [NonAction]
        public override System.Web.Mvc.ActionResult DeviceDetail(System.Guid Id)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.DeviceDetail);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "Id", Id);
            DeviceDetailOverride(callInfo, Id);
            return callInfo;
        }

        [NonAction]
        partial void DeviceDetailOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, ESS.Data.Model.RequestDeviceModel Model);

        [NonAction]
        public override System.Web.Mvc.ActionResult DeviceDetail(ESS.Data.Model.RequestDeviceModel Model)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.DeviceDetail);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "Model", Model);
            DeviceDetailOverride(callInfo, Model);
            return callInfo;
        }

        [NonAction]
        partial void SubmitRequestDeviceOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, System.Guid Id);

        [NonAction]
        public override System.Web.Mvc.ActionResult SubmitRequestDevice(System.Guid Id)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.SubmitRequestDevice);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "Id", Id);
            SubmitRequestDeviceOverride(callInfo, Id);
            return callInfo;
        }

        [NonAction]
        partial void FinishRequestDeviceOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, System.Guid Id);

        [NonAction]
        public override System.Web.Mvc.ActionResult FinishRequestDevice(System.Guid Id)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.FinishRequestDevice);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "Id", Id);
            FinishRequestDeviceOverride(callInfo, Id);
            return callInfo;
        }

    }
}

#endregion T4MVC
#pragma warning restore 1591, 3008, 3009, 0108, 0114
